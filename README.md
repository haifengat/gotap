# gotap

#### 介绍

易盛 iTap 北斗星接口 golang 封装

#### 软件架构

软件架构说明

#### 安装教程

1. 接口下载
   [](http://www.esunny.com.cn/index.php?a=lists&catid=49)

#### 使用说明

1. C++ -> C
2. go 调用 C
   1. 生成器生成 .go 文件
   2. 复制 .h 文件到 `go` 目录下
   3. 在 go 目录下, 运行 go run main
   4. 根据提示修改 .h 文件的内容
3. 类型转换 c -> go C 函数返回值转换为 go 变量
   1. void\* -> string : 返回 地址转换为字串
      1. C.GoString((\*C.char)(c 的返回函数)
4. 类型转换 go -> c go 中的变量传递给 c 函数
   1. *TAPIINT32 -> *C.int : go 中的变量地址给 c 使用
      1. (\*C.int)(unsafe.Pointer(res))
   2. string -> char\*
      ```go
      str := C.CString(IP)
      C.SetHostAddress(t.api, str, C.ushort(port))
      defer C.free(unsafe.Pointer(str))
      ```
   3. TAPIUINT16 -> C.ushort
   4. []byte -> char*
      `(*C.char)(unsafe.Pointer(&bt[0]))`
   5. 删除多余的 amp; (由&转换造成)
5. go 测试
   1. lib 目录下
      ```sh
      g++ -shared -fPIC -Wl,-rpath . -o libtaptrade.so ../c/trade.cpp ../c/trade_fn.cpp libiTapTradeAPI.so
      ```
   2. 复制库
      `cp ../lib/lib*.so lnx`
   3. go run main.go
6. c++ 测试
   1. 修改官方 Makefile
      ```sh
      # cento 7.8 用以下方法
      g++ -o demo Demo.cpp trade.cpp SimpleEvent.cpp ./libiTapTradeAPI.so`
      ```
7. QA
   1. trade_on.go `previous declaration of ‘OnRtnCombinePosition’ was here`
      1. 检查是否 .go 文件中的定义与 .h 中的不同
         1. trade.h 文件中定义的响应, 是否增加了 spi 作为第一个参数.
         2. [弃]增加 spi 是为了支持多帐号，增加了复杂性，暂`不`支持
      2. [弃]删除 trade.h 中的响应定义中的 const
         1. cgo 不支持 const char\*
         2. const structXXX 等带 const 的类型**不**受影响
      3. trade_lnx.go 中重新定义 C 的响应函数
         1. 所有类型转换为 c 的基础类型，不使用自定义类型
      4. 重新定义所有 struct, 字段类型改为 C 原始类型
   2. 登录 Login 返回 0，没有收到 OnConnected 响应，却报错：ignal: segmentation fault
      1. Login 之前，注册 SPI 则会触发异常
         1. c 返回 spi，用 new Trade()，对 make 时报错的函数先注销掉，确保编译通过
