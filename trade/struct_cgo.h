

struct TapAPIApplicationInfo 
{
    
	char AuthCode[513]; // < 授权码
    
	char KeyOperationLogPath[300]; // < 关键操作日志路径 APIRun日志
    
	char LogLevel; // < 日志级别
    
	char APPID[30]; // < 看穿式监管使用,北斗星系统不使用该字段		
    
	int ReConnectCount; // < 重连次数		
    
	int ReConnectSeconds; // < 重连时间间隔(秒)
    
};

struct TapAPICommodity 
{
    
	char ExchangeNo[10]; // < 交易所编码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编号
    
};

struct TapAPIContract 
{
    
	struct TapAPICommodity Commodity; // < 品种
    
	char ContractNo1[10]; // < 合约代码1
    
	char StrikePrice1[10]; // < 执行价1
    
	char CallOrPutFlag1; // < 看涨看跌标示1
    
	char ContractNo2[10]; // < 合约代码2
    
	char StrikePrice2[10]; // < 执行价2
    
	char CallOrPutFlag2; // < 看涨看跌标示2
    
};

struct TapAPIExchangeInfo 
{
    
	char ExchangeNo[10]; // < 交易所编码
    
	char ExchangeName[20]; // < 交易所名称
    
};

struct TapAPIChangePasswordReq 
{
    
	char PasswordType; // <密码类型
    
	char OldPassword[20]; // < 旧密码
    
	char NewPassword[20]; // < 新密码
    
};

struct TapAPIAuthPasswordReq 
{
    
	char AccountNo[20]; // <客户账号
    
	char PasswordType; // <密码类型
    
	char Password[20]; // <账户密码
    
};

struct TapAPITradeLoginAuth 
{
    
	char UserNo[20]; // < 用户名
    
	char ISModifyPassword; // < 是否修改密码	
    
	char Password[20]; // < 密码
    
	char NewPassword[20]; // < 新密码
    
	char LoginIP[40]; // < 登录IP（使用此字段需易盛授权）
    
	char LoginMac[50]; // < 登录MAC,格式12-34-56-78-90-11（使用此字段需易盛授权）
    
	char DeviceName[50]; // < 登录设备名称（使用此字段需易盛授权）
    
};

struct TapAPITradeLoginRspInfo 
{
    
	char UserNo[20]; // < 用户编号
    
	char UserType; // < 用户类型
    
	char UserName[20]; // < 用户名
    
	char ReservedInfo[50]; // < 交易中心和后台版本号
    
	char LastLoginIP[40]; // < 上次登录IP
    
	unsigned int LastLoginProt; // < 上次登录端口
    
	char LastLoginTime[20]; // < 上次登录时间
    
	char LastLogoutTime[20]; // < 上次退出时间
    
	char TradeDate[11]; // < 当前交易日期
    
	char LastSettleTime[20]; // < 上次结算时间
    
	char StartTime[20]; // < 系统启动时间
    
	char NextSecondDate[20]; // < 下次二次认证日期
    
	char LastLoginInfo[300]; // < 登录附加信息
    
};

struct TapAPIRequestVertificateCodeRsp 
{
    
	char SecondSerialID; // < 二次认证授权码序号
    
	int Effective; // < 二次认证授权码有效期（分）。
    
};

struct TapAPISecondCertificationReq 
{
    
	char VertificateCode[10]; // <二次认证码
    
	char LoginType; // <二次认证登录类型
    
};

struct TapAPIAccQryReq 
{
    
};

struct TapAPIAccountInfo 
{
    
	char AccountNo[20]; // < 资金账号
    
	char AccountType; // < 账号类型
    
	char AccountState; // < 账号状态
    
	char AccountTradeRight; // <交易状态
    
	char CommodityGroupNo[10]; // <可交易品种组.
    
	char AccountShortName[20]; // < 账号简称
    
	char AccountEnShortName[20]; // <账号英文简称
    
};

struct TapAPINewOrder 
{
    
	char AccountNo[20]; // < 客户资金帐号，必填
    
	char ExchangeNo[10]; // < 交易所编号，必填
    
	char CommodityType; // < 品种类型，必填
    
	char CommodityNo[10]; // < 品种编码类型，必填
    
	char ContractNo[10]; // < 合约1，必填
    
	char StrikePrice[10]; // < 执行价格1，期权填写
    
	char CallOrPutFlag; // < 看张看跌1 默认N
    
	char ContractNo2[10]; // < 合约2，默认空
    
	char StrikePrice2[10]; // < 执行价格2，默认空
    
	char CallOrPutFlag2; // < 看张看跌2 默认N
    
	char OrderType; // < 委托类型 必填
    
	char OrderSource; // < 委托来源，默认程序单。
    
	char TimeInForce; // < 委托有效类型,默认当日有效
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char IsRiskOrder; // < 是否风险报单，默认非风险保单
    
	char OrderSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1,默认N
    
	char PositionEffect2; // < 开平标志2，默认N
    
	char InquiryNo[50]; // < 询价号
    
	char HedgeFlag; // < 投机保值，默认N
    
	double OrderPrice; // < 委托价格1
    
	double OrderPrice2; // < 委托价格2，做市商应价使用
    
	double StopPrice; // < 触发价格
    
	unsigned int OrderQty; // < 委托数量，必填
    
	unsigned int OrderMinQty; // < 最小成交量，默认1
    
	unsigned int MinClipSize; // < 冰山单最小随机量
    
	unsigned int MaxClipSize; // < 冰山单最大随机量
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // < 浮点参考值
    
	char RefString[50]; // < 字符串参考值
    
	char ClientID; // < 客户子账号，如果存在子账号，则自行上报子账号
    
	char TacticsType; // < 策略单类型，默认N
    
	char TriggerCondition; // < 触发条件，默认N
    
	char TriggerPriceType; // < 触发价格类型，默认N
    
	char AddOneIsValid; // < 是否T+1有效,默认T+1有效。
    
	char ClientLocationID; // < 下单人区域
    
};

struct TapAPIOrderInfo 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格1
    
	char CallOrPutFlag; // < 看张看跌1
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
	char OrderType; // < 委托类型
    
	char OrderSource; // < 委托来源
    
	char TimeInForce; // < 委托有效类型
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char IsRiskOrder; // < 是否风险报单
    
	char OrderSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1
    
	char PositionEffect2; // < 开平标志2
    
	char InquiryNo[50]; // < 询价号
    
	char HedgeFlag; // < 投机保值
    
	double OrderPrice; // < 委托价格1
    
	double OrderPrice2; // < 委托价格2，做市商应价使用
    
	double StopPrice; // < 触发价格
    
	unsigned int OrderQty; // < 委托数量
    
	unsigned int OrderMinQty; // < 最小成交量
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // < 浮点参考值
    
	char RefString[50]; // < 字符串参考值
    
	unsigned int MinClipSize; // < 冰山单最小随机量
    
	unsigned int MaxClipSize; // < 冰山单最大随机量
    
	char LicenseNo[50]; // < 软件授权号
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
	char ClientOrderNo[50]; // < 客户端本地委托编号
    
	char ClientID; // < 客户子账号
    
	char TacticsType; // < 策略单类型
    
	char TriggerCondition; // < 触发条件
    
	char TriggerPriceType; // < 触发价格类型
    
	char AddOneIsValid; // < 是否T+1有效
    
	char ClientLocalIP[40]; // < 终端本地IP
    
	char ClientMac[13]; // < 终端本地Mac地址
    
	char ClientIP[40]; // < 终端网络地址.
    
	unsigned int OrderStreamID; // < 委托流水号
    
	char UpperNo[10]; // < 上手号
    
	char UpperChannelNo[10]; // < 上手通道号
    
	char OrderLocalNo[20]; // < 本地号
    
	unsigned int UpperStreamID; // < 上手流号
    
	char OrderSystemNo[50]; // < 系统号
    
	char OrderExchangeSystemNo[50]; // < 交易所系统号 
    
	char OrderParentSystemNo[50]; // < 父单系统号
    
	char OrderInsertUserNo[20]; // < 下单人
    
	char OrderInsertTime[20]; // < 下单时间
    
	char OrderCommandUserNo[20]; // < 录单操作人
    
	char OrderUpdateUserNo[20]; // < 委托更新人
    
	char OrderUpdateTime[20]; // < 委托更新时间
    
	char OrderState; // < 委托状态
    
	double OrderMatchPrice; // < 成交价1
    
	double OrderMatchPrice2; // < 成交价2
    
	unsigned int OrderMatchQty; // < 成交量1
    
	unsigned int OrderMatchQty2; // < 成交量2
    
	unsigned int ErrorCode; // < 最后一次操作错误信息码
    
	char ErrorText[50]; // < 错误信息
    
	char IsBackInput; // < 是否为录入委托单
    
	char IsDeleted; // < 委托成交删除标
    
	char IsAddOne; // < 是否为T+1单
    
	char ClientLocationID; // < 下单人区域
    
};

struct TapAPIOrderInfoNotice 
{
    
	unsigned int SessionID; // < 会话ID
    
	unsigned int ErrorCode; // < 错误码
    
};

struct TapAPIOrderActionRsp 
{
    
	char ActionType; // < 操作类型
    
};

struct TapAPIAmendOrder 
{
    
	struct TapAPINewOrder ReqData; // < 报单请求数据
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编号
    
};

struct TapAPIOrderCancelReq 
{
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // < 浮点参考值
    
	char RefString[50]; // < 字符串参考值
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
};

struct TapAPIOrderQryReq 
{
    
	char AccountNo[20]; // < 资金账号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char OrderType; // < 委托类型
    
	char OrderSource; // < 委托来源
    
	char TimeInForce; // < 委托有效类型
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char IsRiskOrder; // < 是否风险报单
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编号
    
	char IsBackInput; // < 是否为录入委托单
    
	char IsDeleted; // < 委托成交删除标
    
	char IsAddOne; // < 是否为T+1单
    
	char OrderQryType; // < 是否只查询未完成委托
    
};

struct TapAPIOrderProcessQryReq 
{
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
};

struct TapAPIFillQryReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char MatchSource; // < 委托来源
    
	char MatchSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
	char UpperNo[10]; // < 上手号
    
	char IsDeleted; // < 委托成交删除标
    
	char IsAddOne; // < 是否为T+1单
    
};

struct TapAPIFillInfo 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char MatchSource; // < 委托来源
    
	char MatchSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
	char OrderSystemNo[50]; // < 系统号
    
	char MatchNo[20]; // < 本地成交号
    
	char UpperMatchNo[70]; // < 上手成交号
    
	char ExchangeMatchNo[70]; // < 交易所成交号
    
	char MatchDateTime[20]; // < 成交时间
    
	char UpperMatchDateTime[20]; // < 上手成交时间
    
	char UpperNo[10]; // < 上手号
    
	double MatchPrice; // < 成交价
    
	unsigned int MatchQty; // < 成交量
    
	char IsDeleted; // < 委托成交删除标
    
	char IsAddOne; // < 是否为T+1单
    
	char FeeCurrencyGroup[10]; // < 客户手续费币种组
    
	char FeeCurrency[10]; // < 客户手续费币种
    
	double FeeValue; // < 手续费
    
	char IsManualFee; // < 人工客户手续费标记
    
	double ClosePrositionPrice; // < 指定价格平仓
    
	double CloseProfit; // < 平仓盈亏
    
};

struct TapAPICloseQryReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
};

struct TapAPICloseInfo 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char CloseSide; // < 平仓一边的买入卖出
    
	unsigned int CloseQty; // < 平仓成交量
    
	double OpenPrice; // < 开仓成交价
    
	double ClosePrice; // < 平仓成交价
    
	char OpenMatchNo[20]; // < 本地成交号
    
	char OpenMatchDateTime[20]; // < 成交时间
    
	char CloseMatchNo[20]; // < 本地成交号
    
	char CloseMatchDateTime[20]; // < 成交时间
    
	unsigned int CloseStreamId; // < 平仓流号
    
	char CommodityCurrencyGroup[10]; // < 品种币种组
    
	char CommodityCurrency[10]; // < 品种币种
    
	double CloseProfit; // < 平仓盈亏
    
};

struct TapAPIPositionQryReq 
{
    
};

struct TapAPIPositionInfo 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char MatchSide; // < 买入卖出
    
	char HedgeFlag; // < 投机保值
    
	char PositionNo[70]; // < 本地持仓号，服务器编写
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
	char MatchNo[20]; // < 本地成交号
    
	char UpperNo[10]; // < 上手号
    
	double PositionPrice; // < 持仓价
    
	unsigned int PositionQty; // < 持仓量
    
	unsigned int PositionStreamId; // < 持仓流号
    
	char CommodityCurrencyGroup[10]; // < 品种币种组
    
	char CommodityCurrency[10]; // < 品种币种
    
	double CalculatePrice; // < 当前计算价格
    
	double AccountInitialMargin; // < 客户初始保证金
    
	double AccountMaintenanceMargin; // < 客户维持保证金
    
	double UpperInitialMargin; // < 上手初始保证金
    
	double UpperMaintenanceMargin; // < 上手维持保证金
    
	double PositionProfit; // < 持仓盈亏
    
	double LMEPositionProfit; // < LME持仓盈亏
    
	double OptionMarketValue; // < 期权市值
    
	char IsHistory; // < 是否为昨仓。
    
};

struct TapAPIPositionProfit 
{
    
	char PositionNo[70]; // < 本地持仓号，服务器编写
    
	unsigned int PositionStreamId; // < 持仓流号
    
	double PositionProfit; // < 持仓盈亏
    
	double LMEPositionProfit; // < LME持仓盈亏
    
	double OptionMarketValue; // < 期权市值
    
	double CalculatePrice; // < 计算价格
    
};

struct TapAPIPositionProfitNotice 
{
    
	char IsLast; // < 是否最后一包
    
};

struct TapAPIPositionSummary 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char MatchSide; // < 买入卖出
    
	double PositionPrice; // < 持仓均价。
    
	unsigned int PositionQty; // < 持仓量
    
	unsigned int HisPositionQty; // < 历史持仓量
    
};

struct TapAPIFundReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
};

struct TapAPIFundData 
{
    
	char AccountNo[20]; // < 客户资金账号
    
	char CurrencyGroupNo[10]; // < 币种组号
    
	char CurrencyNo[10]; // < 币种号(为空表示币种组基币资金)
    
	double TradeRate; // < 交易汇率
    
	char FutureAlg; // < 期货算法
    
	char OptionAlg; // < 期权算法
    
	double PreBalance; // < 上日结存
    
	double PreUnExpProfit; // < 上日未到期平盈
    
	double PreLMEPositionProfit; // < 上日LME持仓平盈
    
	double PreEquity; // < 上日权益
    
	double PreAvailable1; // < 上日可用
    
	double PreMarketEquity; // < 上日市值权益
    
	double CashInValue; // < 入金
    
	double CashOutValue; // < 出金
    
	double CashAdjustValue; // < 资金调整
    
	double CashPledged; // < 质押资金
    
	double FrozenFee; // < 冻结手续费
    
	double FrozenDeposit; // < 冻结保证金
    
	double AccountFee; // < 客户手续费包含交割手续费
    
	double SwapInValue; // < 汇入资金
    
	double SwapOutValue; // < 汇出资金
    
	double PremiumIncome; // < 权利金收取
    
	double PremiumPay; // < 权利金支付
    
	double CloseProfit; // < 平仓盈亏
    
	double FrozenFund; // < 冻结资金
    
	double UnExpProfit; // < 未到期平盈
    
	double ExpProfit; // < 到期平仓盈亏
    
	double PositionProfit; // < 不含LME持仓盈亏
    
	double LmePositionProfit; // < LME持仓盈亏
    
	double OptionMarketValue; // < 期权市值
    
	double AccountIntialMargin; // < 客户初始保证金
    
	double AccountMaintenanceMargin; // < 客户维持保证金
    
	double UpperInitalMargin; // < 上手初始保证金
    
	double UpperMaintenanceMargin; // < 上手维持保证金
    
	double Discount; // < LME贴现
    
	double Balance; // < 当日结存
    
	double Equity; // < 当日权益
    
	double Available; // < 当日可用
    
	double CanDraw; // < 可提取
    
	double MarketEquity; // < 账户市值
    
	double AuthMoney; // < 授信资金
    
};

struct TapAPICommodityInfo 
{
    
};

struct TapAPITradeContractInfo 
{
    
	char ExchangeNo[10]; // < 交易所编码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编号
    
	char ContractNo1[10]; // < 合约代码1
    
	char StrikePrice1[10]; // < 执行价1
    
	char CallOrPutFlag1; // < 看涨看跌标示1
    
	char ContractNo2[10]; // < 合约代码2
    
	char StrikePrice2[10]; // < 执行价2
    
	char CallOrPutFlag2; // < 看涨看跌标示2
    
	char ContractType; // < 合约类型
    
	char QuoteUnderlyingContract[10]; // < 行情真实合约
    
	char ContractName[70]; // < 合约名称
    
	char ContractExpDate[11]; // < 合约到期日	
    
	char LastTradeDate[11]; // < 最后交易日
    
	char FirstNoticeDate[11]; // < 首次通知日
    
	double ContractSize; // < 特殊合约层每手乘数
    
};

struct TapAPICurrencyInfo 
{
    
	char CurrencyNo[10]; // < 币种编号
    
	char CurrencyGroupNo[10]; // < 币种组编号
    
	double TradeRate; // < 交易汇率
    
	double TradeRate2; // < 交易汇率2
    
	char FutureAlg; // < 币种组独立标志
    
	char OptionAlg; // < 是否是基币
    
};

struct TapAPITradeMessageReq 
{
    
};

struct TapAPITradeMessage 
{
    
	unsigned int SerialID; // < 流号
    
	char AccountNo[20]; // < 客户资金账号
    
	char TMsgValidDateTime[20]; // < 消息有效时间
    
	char TMsgTitle[50]; // < 消息标题
    
	char TMsgContent[500]; // < 消息内容
    
	char TMsgType; // < 消息类型
    
	char TMsgLevel; // < 消息级别
    
	char IsSendBySMS; // < 是否发送短信
    
	char IsSendByEMail; // < 是否发送邮件
    
	char Sender[20]; // < 发送者
    
	char SendDateTime[20]; // < 发送时间
    
};

struct TapAPIBillQryReq 
{
    
};

struct TapAPIBillQryRsp 
{
    
	char BillText[1]; // < 变长账单内容，长度由BillLen指定
    
};

struct TapAPIHisOrderQryReq 
{
    
	char AccountNo[20]; // < 客户资金账号
    
	char BeginDate[11]; // < 开始时间 (必填)
    
	char EndDate[11]; // < 结束时间 (必填)
    
};

struct TapAPIHisOrderQryRsp 
{
    
	char Date[11]; // < 日期
    
	char AccountNo[20]; // < 客户资金账号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
	char OrderType; // < 委托类型
    
	char OrderSource; // < 委托来源
    
	char TimeInForce; // < 委托有效类型
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char IsRiskOrder; // < 是否风险报单
    
	char OrderSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志
    
	char PositionEffect2; // < 开平标志2
    
	char InquiryNo[50]; // < 询价号
    
	char HedgeFlag; // < 投机保值
    
	double OrderPrice; // < 委托价格
    
	double OrderPrice2; // < 委托价格2，做市商应价使用
    
	double StopPrice; // < 触发价格
    
	unsigned int OrderQty; // < 委托数量
    
	unsigned int OrderMinQty; // < 最小成交量
    
	unsigned int OrderCanceledQty; // < 撤单数量
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // <浮点参考型。
    
	char RefString[50]; // < 字符串参考值
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
	unsigned int OrderStreamID; // < 委托流水号
    
	char UpperNo[10]; // < 上手号
    
	char UpperChannelNo[10]; // < 上手通道编号
    
	char OrderLocalNo[20]; // < 本地号
    
	unsigned int UpperStreamID; // < 上手流号
    
	char OrderSystemNo[50]; // < 系统号
    
	char OrderExchangeSystemNo[50]; // < 交易所系统号
    
	char OrderParentSystemNo[50]; // < 父单系统号 
    
	char OrderInsertUserNo[20]; // < 下单人
    
	char OrderInsertTime[20]; // < 下单时间
    
	char OrderCommandUserNo[20]; // < 指令下达人
    
	char OrderUpdateUserNo[20]; // < 委托更新人
    
	char OrderUpdateTime[20]; // < 委托更新时间
    
	char OrderState; // < 委托状态
    
	double OrderMatchPrice; // < 成交价
    
	double OrderMatchPrice2; // < 成交价2
    
	unsigned int OrderMatchQty; // < 成交量
    
	unsigned int OrderMatchQty2; // < 成交量2
    
	unsigned int ErrorCode; // < 最后一次操作错误信息码
    
	char ErrorText[50]; // < 错误信息
    
	char IsBackInput; // < 是否为录入委托单
    
	char IsDeleted; // < 委托成交删除标记
    
	char IsAddOne; // < 是否为T+1单
    
	char AddOneIsValid; // < 是否T+1有效
    
	unsigned int MinClipSize; // < 冰山单最小随机量
    
	unsigned int MaxClipSize; // < 冰山单最大随机量
    
	char LicenseNo[50]; // < 软件授权号
    
	char TacticsType; // < 策略单类型	
    
	char TriggerCondition; // < 触发条件
    
	char TriggerPriceType; // < 触发价格类型
    
};

struct TapAPIHisMatchQryReq 
{
    
	char AccountNo[20]; // < 客户资金账号
    
	char BeginDate[11]; // < 开始日期，必填
    
	char EndDate[11]; // < 结束日期，必填
    
};

struct TapAPIHisMatchQryRsp 
{
    
	char SettleDate[11]; // < 结算日期
    
	char TradeDate[11]; // <交易日期
    
	char AccountNo[20]; // < 客户资金账号
    
	char ExchangeNo[10]; // < 市场或者交易所代码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种号
    
	char ContractNo[10]; // < 合约号
    
	char StrikePrice[10]; // < 执行价
    
	char CallOrPutFlag; // < 看涨看跌标志
    
	char MatchSource; // < 成交来源	
    
	char MatchSide; // < 买卖方向
    
	char PositionEffect; // < 开平标志
    
	char HedgeFlag; // < 投机保值
    
	double MatchPrice; // < 成交价
    
	unsigned int MatchQty; // < 成交量
    
	char OrderNo[20]; // < 委托号
    
	char MatchNo[20]; // < 成交序号
    
	unsigned int MatchStreamID; // < 成交流水号
    
	char UpperNo[10]; // < 上手号
    
	char MatchCmbNo[20]; // < 组合号
    
	char ExchangeMatchNo[70]; // < 成交编号(交易所成交号)
    
	unsigned int MatchUpperStreamID; // < 上手流水号
    
	double Turnover; // < 成交金额
    
	double PremiumIncome; // < 权利金收入
    
	double PremiumPay; // < 权利金支出
    
	double AccountFee; // < 客户手续费
    
	char AccountFeeCurrency[10]; // < 客户手续费币种
    
	char IsManualFee; // < 人工客户手续费标记
    
	double UpperFee; // < 上手手续费
    
	char UpperFeeCurrency[10]; // < 上手手续费币种
    
	char IsUpperManualFee; // < 人工上手手续费标记
    
	char MatchDateTime[20]; // < 成交时间
    
	char UpperMatchDateTime[20]; // < 上手成交时间
    
	double CloseProfit; // < 平仓盈亏
    
	double ClosePrice; // < 指定平仓价格；
    
	unsigned int CloseQty; // < 平仓量
    
	char SettleGroupNo[10]; // <结算分组
    
	char OperatorNo[20]; // < 操作员
    
	char OperateTime[20]; // < 操作时间
    
};

struct TapAPIHisOrderProcessQryReq 
{
    
};

struct TapAPIHisPositionQryReq 
{
    
	char AccountNo[20]; // < 客户资金账号
    
	char Date[11]; // < 日期
    
	char SettleFlag; // <数据类型
    
};

struct TapAPIHisPositionQryRsp 
{
    
	char SettleDate[11]; // < 结算日期
    
	char OpenDate[11]; // < 开仓日期
    
	char AccountNo[20]; // < 客户资金账号
    
	char ExchangeNo[10]; // < 市场或者交易所代码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码
    
	char ContractNo[10]; // < 合约号
    
	char StrikePrice[10]; // < 执行价
    
	char CallOrPutFlag; // < 看涨看跌标志
    
	char MatchSide; // < 买卖方向
    
	char HedgeFlag; // < 投机保值
    
	double PositionPrice; // < 持仓价格
    
	unsigned int PositionQty; // < 持仓量
    
	char OrderNo[20]; // < 
    
	char PositionNo[20]; // < 持仓编号
    
	char UpperNo[10]; // < 上手号	
    
	char CurrencyGroup[10]; // < 品种币种组
    
	char Currency[10]; // < 品种币种
    
	double PreSettlePrice; // < 上日结算价格
    
	double SettlePrice; // < 结算价格
    
	double PositionDProfit; // < 持仓盈亏(盯市)
    
	double LMEPositionProfit; // < LME持仓盈亏
    
	double OptionMarketValue; // < 期权市值
    
	double AccountInitialMargin; // < 客户初始保证金
    
	double AccountMaintenanceMargin; // < 客户维持保证金
    
	double UpperInitialMargin; // < 上手初始保证金
    
	double UpperMaintenanceMargin; // < 上手维持保证金
    
	char SettleGroupNo[10]; // < 结算分组
    
	char ServerFlag; // < 服务器标识
    
	char SuperiorAccount[20]; // < 上级账号
    
};

struct TapAPIHisDeliveryQryReq 
{
    
	char AccountNo[20]; // < 客户资金账号
    
	char BeginDate[11]; // < 开始日期（必填）
    
	char EndDate[11]; // < 结束日期（必填）
    
	char SettleFlag; // < 结算类型
    
};

struct TapAPIHisDeliveryQryRsp 
{
    
	char DeliveryDate[11]; // < 交割日期
    
	char OpenDate[11]; // < 开仓日期
    
	char AccountNo[20]; // < 客户资金账号
    
	char ExchangeNo[10]; // < 市场号或交易所代码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码
    
	char ContractNo[10]; // < 合约编码
    
	char StrikePrice[10]; // < 执行价
    
	char CallOrPutFlag; // < 看涨看跌标志
    
	char MatchSource; // < 成交来源
    
	char OpenSide; // < 开仓方向
    
	double OpenPrice; // < 开仓价格
    
	double DeliveryPrice; // < 交割价格
    
	unsigned int DeliveryQty; // < 交割量
    
	unsigned int FrozenQty; // < 冻结量
    
	char OpenNo[20]; // < 开仓成交号
    
	char UpperNo[10]; // < 上手编号
    
	char CommodityCurrencyGroupy[10]; // < 品种币种
    
	char CommodityCurrency[10]; // < 品种币种
    
	double PreSettlePrice; // < 上日结算价
    
	double DeliveryProfit; // < 交割盈亏
    
	double AccountFrozenInitialMargin; // < 客户初始冻结保证金
    
	double AccountFrozenMaintenanceMargin; // < 客户维持冻结保证金
    
	double UpperFrozenInitialMargin; // < 上手初始冻结保证金
    
	double UpperFrozenMaintenanceMargin; // < 上手维持冻结保证金
    
	char AccountFeeCurrency[10]; // < 客户手续费币种
    
	double AccountDeliveryFee; // < 客户交割手续费 
    
	char UpperFeeCurrency[10]; // < 上手手续费币种
    
	double UpperDeliveryFee; // < 上手交割手续费
    
	char DeliveryMode; // < 交割行权方式
    
	char OperatorNo[20]; // < 操作员
    
	char OperateTime[20]; // < 操作时间
    
	char SettleGourpNo[20]; // < 结算分组
    
	char FutureContractNo[10]; // < 特殊期权标的合约
    
	char OptionStrikePrice[10]; // < 期权真实执行价
    
	char SuperiorAccount[20]; // < 上级账号
    
};

struct TapAPIAccountCashAdjustQryReq 
{
    
	char BeginDate[11]; // < 必填
    
	char EndDate[11]; // < 必填
    
};

struct TapAPIAccountCashAdjustQryRsp 
{
    
	char Date[11]; // < 日期
    
	char AccountNo[20]; // < 客户资金账号
    
	char CashAdjustType; // < 资金调整类型
    
	char CurrencyGroupNo[10]; // < 币种组号
    
	char CurrencyNo[10]; // < 币种号
    
	double CashAdjustValue; // < 资金调整金额
    
	char CashAdjustRemark[100]; // < 资金调整备注
    
	char OperateTime[20]; // < 操作时间
    
	char OperatorNo[20]; // < 操作员
    
	char AccountBank[10]; // < 客户银行
    
	char BankAccount[20]; // < 客户银行账号
    
	char AccountLWFlag; // < 客户本外币标识
    
	char CompanyBank[10]; // < 公司银行
    
	char InternalBankAccount[20]; // < 公司银行账户
    
	char CompanyLWFlag; // < 公司本外币标识
    
};

struct TapAPIAccountFeeRentQryReq 
{
    
};

struct TapAPIAccountFeeRentQryRsp 
{
    
};

struct TapAPIAccountMarginRentQryReq 
{
    
};

struct TapAPIAccountMarginRentQryRsp 
{
    
};

struct TapAPIOrderQuoteMarketNotice 
{
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编号
    
	char ContractNo[10]; // < 合约
    
	char StrikePrice[10]; // < 执行价
    
	char CallOrPutFlag; // < 看涨看跌
    
	char OrderSide; // < 买卖方向
    
	unsigned int OrderQty; // < 委托量
    
};

struct TapAPIOrderMarketInsertReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char OrderType; // < 委托类型
    
	char TimeInForce; // < 有效类型
    
	char ExpireTime[20]; // < 有效期
    
	char OrderSource; // < 委托来源	
    
	char BuyPositionEffect; // < 买开平标志
    
	char SellPositionEffect; // < 卖开平标志
    
	char AddOneIsValid; // < 是否T+1有效
    
	double OrderBuyPrice; // < 买委托价
    
	double OrderSellPrice; // < 卖委托价	
    
	unsigned int OrderBuyQty; // < 买委托量
    
	unsigned int OrderSellQty; // < 卖委托量
    
	char ClientBuyOrderNo[50]; // < 本地委托编号
    
	char ClientSellOrderNo[50]; // < 本地委托编号
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // < 浮点参考值
    
	char RefString[50]; // < 字符串参考值
    
	char Remark[100]; // < 备注
    
};

struct TapAPIOrderMarketInsertRsp 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char OrderType; // < 委托类型
    
	char TimeInForce; // < 委托有效类型
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char OrderSource; // < 委托来源
    
	char BuyPositionEffect; // < 买开平标志
    
	char SellPositionEffect; // < 卖开平标志
    
	double OrderBuyPrice; // < 买委托价
    
	double OrderSellPrice; // < 卖委托价
    
	unsigned int OrderBuyQty; // < 买委托量
    
	unsigned int OrderSellQty; // < 卖委托量
    
	char ServerFlag; // < 交易服务标识
    
	char OrderBuyNo[20]; // < 买委托号
    
	char OrderSellNo[20]; // < 卖委托号
    
	char AddOneIsValid; // < 是否T+1有效
    
	char OrderMarketUserNo[20]; // < 下单人
    
	char OrderMarketTime[20]; // < 下单时间
    
	int RefInt; // < 整型参考值
    
	double RefDouble; // < 浮点参考值
    
	char RefString[50]; // < 字符串参考值
    
	char ClientBuyOrderNo[50]; // < 买本地委托编号
    
	char ClientSellOrderNo[50]; // < 卖本地委托编号
    
	unsigned int ErrorCode; // < 错误信息码
    
	char ErrorText[50]; // < 错误信息
    
	char ClientLocalIP[40]; // < 终端本地IP地址（客户端填写）
    
	char ClientMac[13]; // < 终端本地Mac地址（客户端填写）
    
	char ClientIP[40]; // < 前置记录的终端IP地址（前置填写）
    
	char Remark[100]; // < 备注
    
};

struct TapAPIOrderMarketDeleteReq 
{
    
	char OrderBuyNo[20]; // < 买委托号
    
	char OrderSellNo[20]; // < 卖委托号
    
};

struct TapAPIOrderLocalRemoveReq 
{
    
};

struct TapAPIOrderLocalRemoveRsp 
{
    
};

struct TapAPIOrderLocalInputReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格1
    
	char CallOrPutFlag; // < 看张看跌1
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
	char OrderType; // < 委托类型
    
	char OrderSource; // < 委托来源
    
	char TimeInForce; // < 委托有效类型
    
	char ExpireTime[20]; // < 有效日期(GTD情况下使用)
    
	char IsRiskOrder; // < 是否风险报单
    
	char OrderSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1
    
	char PositionEffect2; // < 开平标志2
    
	char InquiryNo[50]; // < 询价号
    
	char HedgeFlag; // < 投机保值
    
	double OrderPrice; // < 委托价格1
    
	double OrderPrice2; // < 委托价格2，做市商应价使用
    
	double StopPrice; // < 触发价格
    
	unsigned int OrderQty; // < 委托数量
    
	unsigned int OrderMinQty; // < 最小成交量
    
	char OrderSystemNo[50]; // < 系统号
    
	char OrderExchangeSystemNo[50]; // < 交易所系统号
    
	char UpperNo[10]; // < 上手号
    
	double OrderMatchPrice; // < 成交价1
    
	double OrderMatchPrice2; // < 成交价2
    
	unsigned int OrderMatchQty; // < 成交量1
    
	unsigned int OrderMatchQty2; // < 成交量2
    
	char OrderState; // < 委托状态
    
	char IsAddOne; // < 是否为T+1单
    
};

struct TapAPIOrderLocalModifyReq 
{
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
};

struct TapAPIOrderLocalTransferReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 委托编码
    
};

struct TapAPIFillLocalInputReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格
    
	char CallOrPutFlag; // < 看张看跌
    
	char MatchSide; // < 买入卖出
    
	char PositionEffect; // < 开平标志1
    
	char HedgeFlag; // < 投机保值
    
	double MatchPrice; // < 成交价
    
	unsigned int MatchQty; // < 成交量
    
	char OrderSystemNo[50]; // < 系统号
    
	char UpperMatchNo[70]; // < 上手成交号
    
	char MatchDateTime[20]; // < 成交时间
    
	char UpperMatchDateTime[20]; // < 上手成交时间
    
	char UpperNo[10]; // < 上手号
    
	char IsAddOne; // < 是否为T+1单
    
	char FeeCurrencyGroup[10]; // < 客户手续费币种组
    
	char FeeCurrency[10]; // < 客户手续费币种
    
	double FeeValue; // < 手续费
    
	char IsManualFee; // < 人工客户手续费标记
    
	double ClosePositionPrice; // < 指定价格平仓
    
};

struct TapAPIFillLocalRemoveReq 
{
    
	char ServerFlag; // < 服务器标识
    
	char MatchNo[20]; // < 本地成交号
    
};

struct TapAPITradingCalendarQryRsp 
{
    
	char CurrTradeDate[11]; // < 当前交易日
    
	char LastSettlementDate[11]; // < 上次结算日
    
	char PromptDate[11]; // < LME到期日
    
	char LastPromptDate[11]; // < 上日LME到期日
    
};

struct TapAPISpotLockQryReq 
{
    
	char AccountNo[20]; // <客户资金帐号
    
};

struct TapAPISpotLockDataRsp 
{
    
	char AccountNo[20]; // <客户资金账号
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码类型
    
	unsigned int LockQty; // <锁定量
    
	unsigned int FrozenQty; // <冻结量
    
	unsigned int CanUnLockQty; // <可解锁量
    
};

struct TapAPISubmitUserLoginInfo 
{
    
	char UserNo[20]; // < 用户名
    
	char GatherInfo[500]; // < 用户终端采集信息
    
	char ClientLoginIP[40]; // < 用户公网IP
    
	unsigned int ClientLoginPort; // < 用户公网Port
    
	char ClientLoginDateTime[20]; // < 用户登录时间
    
	char ClientAppID[30]; // < 用户AppID
    
	unsigned int AuthKeyVersion; // < 用户终端信息加密密钥版本号
    
	char AbnormalNo; // < 用户采集信息异常标识
    
};

struct TapAPISubmitUserLoginRspInfo 
{
    
	char UserNo[20]; // < 用户名
    
};

struct TapAPISpecialOrderInsertReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	char SpecialOrderType; // < 特殊业务类型
    
	char OrderSource; // < 委托来源
    
	char CombineNo[50]; // < 组合编码
    
	unsigned int OrderQty; // < 委托数量
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格1
    
	char CallOrPutFlag; // < 看张看跌1
    
	char OrderSide1; // < 买卖方向1
    
	char HedgeFlag1; // < 投机备兑1
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
};

struct TapAPISpecialOrderQryReq 
{
    
	char AccountNo[20]; // < 客户资金帐号	
    
	char OrderNo[20]; // < 特殊业务委托编号
    
};

struct TapAPISpecialOrderInfo 
{
    
	unsigned int SessionID; // < 会话ID
    
	unsigned int ErrorCode; // < 错误码
    
	char ErrorText[50]; // < 错误信息
    
	char AccountNo[20]; // < 客户资金帐号
    
	char ServerFlag; // < 服务器标识
    
	char OrderNo[20]; // < 特殊业务委托编号
    
	char ClientOrderNo[50]; // < 客户端本地委托编号
    
	char SpecialOrderType; // < 特殊业务类型
    
	char OrderSource; // < 委托来源，默认程序单。
    
	char CombineStrategy; // < 组合策略代码
    
	char CombineNo[50]; // < 组合编码
    
	unsigned int OrderQty; // < 委托数量
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格1
    
	char CallOrPutFlag; // < 看张看跌1
    
	char OrderSide1; // < 买卖方向1
    
	unsigned int CombineQty1; // < 组合数量1
    
	char HedgeFlag1; // < 投机备兑1
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
	char OrderSide2; // < 买卖方向2
    
	unsigned int CombineQty2; // < 组合数量2
    
	char HedgeFlag2; // < 投机备兑2
    
	char LicenseNo[50]; // < 软件授权号
    
	char ClientLocalIP[40]; // < 终端本地IP
    
	char ClientMac[13]; // < 终端本地Mac地址
    
	char ClientIP[40]; // < 终端网络地址.
    
	unsigned int OrderStreamID; // < 委托流水号
    
	char UpperNo[10]; // < 上手号
    
	char UpperChannelNo[10]; // < 上手通道号
    
	char OrderLocalNo[20]; // < 网关本地号
    
	char OrderSystemNo[50]; // < 系统号
    
	char OrderExchangeSystemNo[50]; // < 交易所系统号
    
	char OrderInsertUserNo[20]; // < 下单人
    
	char OrderInsertTime[20]; // < 下单时间
    
	char OrderState; // < 委托状态
    
};

struct TapAPICombinePositionQryReq 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
};

struct TapAPICombinePositionInfo 
{
    
	char AccountNo[20]; // < 客户资金帐号
    
	unsigned int PositionStreamID; // < 组合持仓流号
    
	char ServerFlag; // < 服务器标识
    
	char UpperNo[10]; // < 上手号
    
	char CombineStrategy; // < 组合策略代码
    
	char CombineNo[50]; // < 组合编码
    
	unsigned int PositionQty; // < 委托数量
    
	char ExchangeNo[10]; // < 交易所编号
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编码
    
	char ContractNo[10]; // < 合约1
    
	char StrikePrice[10]; // < 执行价格1
    
	char CallOrPutFlag; // < 看张看跌1
    
	char OrderSide1; // < 买卖方向1
    
	unsigned int CombineQty1; // < 组合数量1
    
	char HedgeFlag1; // < 投机备兑1
    
	char ContractNo2[10]; // < 合约2
    
	char StrikePrice2[10]; // < 执行价格2
    
	char CallOrPutFlag2; // < 看张看跌2
    
	char OrderSide2; // < 买卖方向2
    
	unsigned int CombineQty2; // < 组合数量2
    
	char HedgeFlag2; // < 投机备兑2
    
	char CommodityCurrencyGroup[10]; // < 品种币种组
    
	char CommodityCurrency[10]; // < 品种币种
    
	double AccountInitialMargin; // < 初始组合保证金
    
	double AccountMaintenanceMargin; // < 维持组合保证金
    
	double UpperInitialMargin; // < 上手初始组合保证金
    
	double UpperMaintenanceMargin; // < 上手维持组合保证金
    
};

struct TapAPIUserTrustDeviceQryReq 
{
    
};

struct TapAPIUserTrustDeviceQryRsp 
{
    
	char UserNo[20]; // < 登录账号
    
	char LicenseNo[50]; // < 软件授权码
    
	char Mac[50]; // < MAC
    
	char DeviceName[50]; // < 设备名称
    
	char OperatorNo[20]; // < 操作员
    
	char OperateTime[20]; // < 操作时间
    
};

struct TapAPIUserTrustDeviceAddReq 
{
    
};

struct TapAPIUserTrustDeviceDelReq 
{
    
	char LicenseNo[50]; // < 软件授权码
    
	char Mac[50]; // < MAC
    
};

struct TapAPIIPOInfoQryReq 
{
    
};

struct TapAPIIPOInfoQryRsp 
{
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
	char CurrencyGroupNo[10]; // <币种组
    
	char CurrencyNo[10]; // <币种
    
	char BeginDate[20]; // <申购开始日期
    
	char EndDate[20]; // <申购结束日期
    
	char IPODate[20]; // <IPODate
    
	char ResultDate[20]; // <中签日
    
	double IPOFee; // <IPO手续费
    
	double FinancingFee; // <融资手续费
    
	double LoanRatio; // <融资利率
    
	unsigned int FinancingDays; // <融资天数
    
	double MaxLoanRatio; // <最高融资比例
    
	double MaxLoanValue; // <最高融资金额
    
	double Price; // <认购/配售价
    
};

struct TapAPIAvailableApplyQryReq 
{
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
};

struct TapAPIAvailableApplyQryRsp 
{
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
	unsigned int StockQty; // <可申购股数
    
};

struct TapAPIAccountIPOQryReq 
{
    
};

struct TapAPIAccountIPOQryRsp 
{
    
	char EndDate[20]; // <申购结束日期
    
	char ResultDate[20]; // <中签日
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
	char ApplyType; // <申购类型
    
	unsigned int ApplyQty; // <申购数量
    
	double ApplyCash; // <申购金额
    
	double LoanRatio; // <融资比例
    
	double LoanInterest; // <融资利息
    
	double ApplyFee; // <申购手续费
    
	char ApplyStatus; // <申购状态
    
	unsigned int ResultQty; // <中签量
    
};

struct TapAPIAccountIPOAddReq 
{
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
	char ApplyType; // <申购类型
    
	unsigned int ApplyQty; // <申购数量
    
	double LoanRatio; // <融资比例
    
};

struct TapAPIAccountIPOCancelReq 
{
    
	char ExchangeNo[10]; // <市场或者交易所代码
    
	char CommodityType; // <品种类型
    
	char CommodityNo[10]; // <品种号
    
};

struct TapAPIVerifyIdentityReq 
{
    
};
