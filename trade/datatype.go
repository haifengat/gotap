package trade

import (
	"encoding/binary"
	"fmt"
	"math"
	"strings"
)
import "C"

// 长度为10的字符串
type TAPISTR_10 [11]byte

func (e TAPISTR_10) String() string {
	return toGBK(e[:])
}

// 长度为20的字符串
type TAPISTR_20 [21]byte

func (e TAPISTR_20) String() string {
	return toGBK(e[:])
}

// 长度为30的字符串
type TAPISTR_30 [31]byte

func (e TAPISTR_30) String() string {
	return toGBK(e[:])
}

// 长度为40的字符串
type TAPISTR_40 [41]byte

func (e TAPISTR_40) String() string {
	return toGBK(e[:])
}

// 长度为50的字符串
type TAPISTR_50 [51]byte

func (e TAPISTR_50) String() string {
	return toGBK(e[:])
}

// 长度为70的字符串
type TAPISTR_70 [71]byte

func (e TAPISTR_70) String() string {
	return toGBK(e[:])
}

// 长度为100的字符串
type TAPISTR_100 [101]byte

func (e TAPISTR_100) String() string {
	return toGBK(e[:])
}

type TapAPIContactContentType [201]byte

func (e TapAPIContactContentType) String() string {
	return toGBK(e[:])
}

// 长度为300的字符串
type TAPISTR_300 [301]byte

func (e TAPISTR_300) String() string {
	return toGBK(e[:])
}

// 长度为500的字符串
type TAPISTR_500 [501]byte

func (e TAPISTR_500) String() string {
	return toGBK(e[:])
}

// 长度为2000的字符串
type TAPISTR_2000 [2001]byte

func (e TAPISTR_2000) String() string {
	return toGBK(e[:])
}

// Authorization Code
type TAPIAUTHCODE [513]byte

func (e TAPIAUTHCODE) String() string {
	return toGBK(e[:])
}

// MAC地址类型.
type TAPIMACTYPE [13]byte

func (e TAPIMACTYPE) String() string {
	return toGBK(e[:])
}

// 二次认证序号
type TAPISecondSerialIDType [5]byte

func (e TAPISecondSerialIDType) String() string {
	return toGBK(e[:])
}

// 单字符定义，可用于定义其他类型
type TAPICHAR byte

// 子账户类型
type TAPIClientIDType [16]byte

func (e TAPIClientIDType) String() string {
	return toGBK(e[:])
}

// 下单人地址
type TAPIClientLocationIDType [6]byte

func (e TAPIClientLocationIDType) String() string {
	return toGBK(e[:])
}

// int 32
type TAPIINT32 [4]byte // int32

func (u TAPIINT32) String() string {
	return fmt.Sprintf("%d", u.Int32())
}
func (u TAPIINT32) Int32() int32 {
	return int32(binary.LittleEndian.Uint32(u[:]))
}
func FromCint32(value C.int) TAPIINT32 {
	var u TAPIINT32
	binary.LittleEndian.PutUint32(u[:], uint32(value))
	return u
}
func FromInt32(value int32) TAPIINT32 {
	var u TAPIINT32
	binary.LittleEndian.PutUint32(u[:], uint32(value))
	return u
}

// unsigned 32
type TAPIUINT32 [4]byte // uint32

func (u TAPIUINT32) String() string {
	return fmt.Sprintf("%d", u.Uint32())
}
func (u TAPIUINT32) Uint32() uint32 {
	return binary.LittleEndian.Uint32(u[:])
}

// 会生成新的 u, 原来的 u 仍是 \x00
//
//	func (u TAPIUINT32) FromUint32(value uint32) {
//		bs := make([]byte, 4)
//		binary.LittleEndian.PutUint32(bs, value)
//		copy(u[:], bs)
//	}
func FromCuint32(value C.uint) TAPIUINT32 {
	var u TAPIUINT32
	binary.LittleEndian.PutUint32(u[:], uint32(value))
	return u
}
func FromUint32(value uint32) TAPIUINT32 {
	var u TAPIUINT32
	binary.LittleEndian.PutUint32(u[:], value)
	return u
}

// int 64
type TAPIINT64 int64

// unsigned 64
type long uint64

// unsigned 16
type TAPIUINT16 uint16

// unsigned 8
type TAPIUINT8 uint8

// real 64
type TAPIREAL64 [8]byte //float64

func (f TAPIREAL64) String() string {
	return fmt.Sprintf("%f", f.Float64())
}
func (f TAPIREAL64) Float64() float64 {
	bits := binary.LittleEndian.Uint64(f[:])
	return math.Float64frombits(bits)
}
func FromFloat64(value float64) TAPIREAL64 {
	var bs TAPIREAL64
	binary.LittleEndian.PutUint64(bs[:], math.Float64bits(value))
	return bs
}

// 是否标示
type TAPIYNFLAG TAPICHAR

const APIYNFLAG_YES TAPIYNFLAG = 'Y' // 是
const APIYNFLAG_NO TAPIYNFLAG = 'N'  // 否
var mpTAPIYNFLAG = map[TAPIYNFLAG]string{APIYNFLAG_YES: "Y", APIYNFLAG_NO: "N"}

func (e TAPIYNFLAG) String() string {
	if s, ok := mpTAPIYNFLAG[e]; ok {
		return s[strings.LastIndex(s, "_")+1:]
	}
	return string(e) + "值未定义"
}

// 密码类型
type TAPIPasswordType TAPICHAR

const APIPASSWORD_TRADE TAPIPasswordType = 'T' // 交易密码
const APIPASSWORD_PHONE TAPIPasswordType = 'P' // 电话密码

// 时间戳类型(格式 yyyy-MM-dd hh:nn:ss.xxx)
type TAPIDTSTAMP [24]byte

func (e TAPIDTSTAMP) String() string {
	return toGBK(e[:])
}

// 日期和时间类型(格式 yyyy-MM-dd hh:nn:ss)
type TAPIDATETIME [20]byte

func (e TAPIDATETIME) String() string {
	return toGBK(e[:])
}

// 日期类型(格式 yyyy-MM-dd)
type TAPIDATE [11]byte

func (e TAPIDATE) String() string {
	return toGBK(e[:])
}

// 时间类型(格式 hh:nn:ss)
type TAPITIME [9]byte

func (e TAPITIME) String() string {
	return toGBK(e[:])
}

// 日志级别
type TAPILOGLEVEL TAPICHAR

const APILOGLEVEL_NONE TAPILOGLEVEL = 'N'    // Normal
const APILOGLEVEL_ERROR TAPILOGLEVEL = 'E'   // Error
const APILOGLEVEL_WARNING TAPILOGLEVEL = 'W' // Warning
const APILOGLEVEL_DEBUG TAPILOGLEVEL = 'D'   // Debug

// =============================================================================
type TAPIOptionType TAPICHAR

// 品种类型
type TAPICommodityType TAPICHAR

const TAPI_COMMODITY_TYPE_NONE TAPICommodityType = 'N'             // 无
const TAPI_COMMODITY_TYPE_FUTURES TAPICommodityType = 'F'          // 期货
const TAPI_COMMODITY_TYPE_OPTION TAPICommodityType = 'O'           // 期权
const TAPI_COMMODITY_TYPE_SPREAD_MONTH TAPICommodityType = 'S'     // 跨期套利
const TAPI_COMMODITY_TYPE_SPREAD_COMMODITY TAPICommodityType = 'M' // 跨品种套利
const TAPI_COMMODITY_TYPE_BUL TAPICommodityType = 'U'              // 看涨垂直套利
const TAPI_COMMODITY_TYPE_BER TAPICommodityType = 'E'              // 看跌垂直套利
const TAPI_COMMODITY_TYPE_STD TAPICommodityType = 'D'              // 跨式套利
const TAPI_COMMODITY_TYPE_STG TAPICommodityType = 'G'              // 宽跨式套利
const TAPI_COMMODITY_TYPE_PRT TAPICommodityType = 'R'              // 备兑组合
const TAPI_COMMODITY_TYPE_DIRECTFOREX TAPICommodityType = 'X'      // 外汇——直接汇率
const TAPI_COMMODITY_TYPE_INDIRECTFOREX TAPICommodityType = 'I'    // 外汇——间接汇率
const TAPI_COMMODITY_TYPE_CROSSFOREX TAPICommodityType = 'C'       // 外汇——交叉汇率
const TAPI_COMMODITY_TYPE_INDEX TAPICommodityType = 'Z'            // 指数
const TAPI_COMMODITY_TYPE_STOCK TAPICommodityType = 'T'            // 股票/基金
const TAPI_COMMODITY_TYPE_TAS TAPICommodityType = 'B'              // TAS
const TAPI_COMMODITY_TYPE_DERIVATIVE TAPICommodityType = '0'       // 港股--窝轮
const TAPI_COMMODITY_TYPE_BULLBEAR TAPICommodityType = '1'         // 港股--牛熊证
const TAPI_COMMODITY_TYPE_INLINE TAPICommodityType = '2'           // 港股--界内证
const TAPI_COMMODITY_TYPE_DBET TAPICommodityType = '3'             // 港股--债券
const TAPI_COMMODITY_TYPE_FUND TAPICommodityType = '4'             // 港股--基金

// 看涨看跌标示
type TAPICallOrPutFlagType TAPICHAR

const TAPI_CALLPUT_FLAG_CALL TAPICallOrPutFlagType = 'C' // 买权
const TAPI_CALLPUT_FLAG_PUT TAPICallOrPutFlagType = 'P'  // 卖权
const TAPI_CALLPUT_FLAG_NONE TAPICallOrPutFlagType = 'N' // 无

// 账号类型
type TAPIAccountType TAPICHAR

const TAPI_ACCOUNT_TYPE_PERSON TAPIAccountType = 'P'       // 个人客户
const TAPI_ACCOUNT_TYPE_ORGANIZATION TAPIAccountType = 'O' // 机构客户
const TAPI_ACCOUNT_TYPE_AGENT TAPIAccountType = 'A'        // 代理人
const TAPI_ACCOUNT_TYPE_HOUSE TAPIAccountType = 'H'        // Internal
const TAPI_ACCOUNT_TYPE_STOCK TAPIAccountType = 'S'        // 股票账户

// 权限编码类型
type TAPIRightIDType TAPIINT32

func (u TAPIRightIDType) String() string {
	return fmt.Sprintf("%d", u.Int32())
}
func (u TAPIRightIDType) Int32() int32 {
	return int32(binary.LittleEndian.Uint32(u[:]))
}

// 登录用户身份类型
type TAPIUserTypeType TAPIINT32

func (u TAPIUserTypeType) String() string {
	return fmt.Sprintf("%d", u.Int32())
}
func (u TAPIUserTypeType) Int32() int32 {
	return int32(binary.LittleEndian.Uint32(u[:]))
}

// 账号状态
type TAPIAccountState TAPICHAR

const TAPI_ACCOUNT_STATE_NORMAL TAPIAccountState = 'N' // 正常
const TAPI_ACCOUNT_STATE_CANCEL TAPIAccountState = 'C' // 销户
const TAPI_ACCOUNT_STATE_SLEEP TAPIAccountState = 'S'  // 休眠

// 客户交易状态类型
type TAPIAccountRightType TAPICHAR

const TAPI_ACCOUNT_TRADING_RIGHT_NORMAL TAPIAccountRightType = '0'  // 正常交易
const TAPI_ACCOUNT_TRADING_RIGHT_NOTRADE TAPIAccountRightType = '1' // 禁止交易
const TAPI_ACCOUNT_TRADING_RIGHT_CLOSE TAPIAccountRightType = '2'   // 只可平仓

// 委托类型
type TAPIOrderTypeType TAPICHAR

const TAPI_ORDER_TYPE_MARKET TAPIOrderTypeType = '1'       // 市价
const TAPI_ORDER_TYPE_LIMIT TAPIOrderTypeType = '2'        // 限价
const TAPI_ORDER_TYPE_STOP_MARKET TAPIOrderTypeType = '3'  // 市价止损
const TAPI_ORDER_TYPE_STOP_LIMIT TAPIOrderTypeType = '4'   // 限价止损
const TAPI_ORDER_TYPE_OPT_EXEC TAPIOrderTypeType = '5'     // 期权行权
const TAPI_ORDER_TYPE_OPT_ABANDON TAPIOrderTypeType = '6'  // 期权弃权
const TAPI_ORDER_TYPE_REQQUOT TAPIOrderTypeType = '7'      // 询价
const TAPI_ORDER_TYPE_RSPQUOT TAPIOrderTypeType = '8'      // 应价
const TAPI_ORDER_TYPE_ICEBERG TAPIOrderTypeType = '9'      // 冰山单
const TAPI_ORDER_TYPE_GHOST TAPIOrderTypeType = 'A'        // 影子单
const TAPI_ORDER_TYPE_HKEX_AUCTION TAPIOrderTypeType = 'B' // 港交所竞价单
const TAPI_ORDER_TYPE_SWAP TAPIOrderTypeType = 'C'         // 互换
const TAPI_ORDER_TYPE_LOCK TAPIOrderTypeType = 'D'         // 证券锁定
const TAPI_ORDER_TYPE_UNLOCK TAPIOrderTypeType = 'E'       // 证券解锁
const TAPI_ORDER_TYPE_ENHANCE TAPIOrderTypeType = 'F'      // 增强限价单
const TAPI_ORDER_TYPE_SPECIAL TAPIOrderTypeType = 'G'      // 特别限价单
const TAPI_ORDER_TYPE_LIMITAUCTION TAPIOrderTypeType = 'H' // 竞价限价单

// 委托来源
type TAPIOrderSourceType TAPICHAR

const TAPI_ORDER_SOURCE_SELF_ETRADER TAPIOrderSourceType = '1'  // 自助电子单
const TAPI_ORDER_SOURCE_PROXY_ETRADER TAPIOrderSourceType = '2' // 代理电子单
const TAPI_ORDER_SOURCE_JTRADER TAPIOrderSourceType = '3'       // 外部电子单(外部电子系统下单，本系统录入)
const TAPI_ORDER_SOURCE_MANUAL TAPIOrderSourceType = '4'        // 人工录入单(外部其他方式下单，本系统录入)
const TAPI_ORDER_SOURCE_CARRY TAPIOrderSourceType = '5'         // carry单
const TAPI_ORDER_SOURCE_PROGRAM TAPIOrderSourceType = '6'       // 程式化报单
const TAPI_ORDER_SOURCE_DELIVERY TAPIOrderSourceType = '7'      // 交割行权
const TAPI_ORDER_SOURCE_ABANDON TAPIOrderSourceType = '8'       // 期权放弃
const TAPI_ORDER_SOURCE_CHANNEL TAPIOrderSourceType = '9'       // 通道费
const TAPI_ORDER_SOURCE_TEMPORARY TAPIOrderSourceType = 'J'     // 临时成交
const TAPI_ORDER_SOURCE_PROGRAMMANUAL TAPIOrderSourceType = 'K' // 程序化手工单

// 委托有效类型
type TAPITimeInForceType TAPICHAR

const TAPI_ORDER_TIMEINFORCE_GFD TAPITimeInForceType = '0' // 当日有效
const TAPI_ORDER_TIMEINFORCE_GTC TAPITimeInForceType = '1' // 取消前有效
const TAPI_ORDER_TIMEINFORCE_GTD TAPITimeInForceType = '2' // 指定日期前有效
const TAPI_ORDER_TIMEINFORCE_FAK TAPITimeInForceType = '3' // FAK或IOC
const TAPI_ORDER_TIMEINFORCE_FOK TAPITimeInForceType = '4' // FOK

// 买卖类型
type TAPISideType TAPICHAR

const TAPI_SIDE_NONE TAPISideType = 'N' // 无
const TAPI_SIDE_BUY TAPISideType = 'B'  // 买入
const TAPI_SIDE_SELL TAPISideType = 'S' // 卖出

// 开平类型
type TAPIPositionEffectType TAPICHAR

const TAPI_PositionEffect_NONE TAPIPositionEffectType = 'N'        // 不分开平
const TAPI_PositionEffect_OPEN TAPIPositionEffectType = 'O'        // 开仓
const TAPI_PositionEffect_COVER TAPIPositionEffectType = 'C'       // 平仓
const TAPI_PositionEffect_COVER_TODAY TAPIPositionEffectType = 'T' // 平当日

// 投机保值类型
type TAPIHedgeFlagType TAPICHAR

const TAPI_HEDGEFLAG_NONE TAPIHedgeFlagType = 'N' // 无
const TAPI_HEDGEFLAG_T TAPIHedgeFlagType = 'T'    // 投机
const TAPI_HEDGEFLAG_B TAPIHedgeFlagType = 'B'    // 保值
const TAPI_HEDGEFLAG_R TAPIHedgeFlagType = 'R'    // 备兑

// ！未结束的委托类型：01234578D
type TAPIOrderStateType TAPICHAR

const TAPI_ORDER_STATE_SUBMIT TAPIOrderStateType = '0'           // 终端提交
const TAPI_ORDER_STATE_ACCEPT TAPIOrderStateType = '1'           // 已受理
const TAPI_ORDER_STATE_TRIGGERING TAPIOrderStateType = '2'       // 策略待触发
const TAPI_ORDER_STATE_EXCTRIGGERING TAPIOrderStateType = '3'    // 交易所待触发
const TAPI_ORDER_STATE_QUEUED TAPIOrderStateType = '4'           // 已排队
const TAPI_ORDER_STATE_PARTFINISHED TAPIOrderStateType = '5'     // 部分成交
const TAPI_ORDER_STATE_FINISHED TAPIOrderStateType = '6'         // 完全成交
const TAPI_ORDER_STATE_CANCELING TAPIOrderStateType = '7'        // 待撤消(排队临时状态)
const TAPI_ORDER_STATE_MODIFYING TAPIOrderStateType = '8'        // 待修改(排队临时状态)
const TAPI_ORDER_STATE_CANCELED TAPIOrderStateType = '9'         // 完全撤单
const TAPI_ORDER_STATE_LEFTDELETED TAPIOrderStateType = 'A'      // 已撤余单
const TAPI_ORDER_STATE_FAIL TAPIOrderStateType = 'B'             // 指令失败
const TAPI_ORDER_STATE_DELETED TAPIOrderStateType = 'C'          // 策略删除
const TAPI_ORDER_STATE_SUPPENDED TAPIOrderStateType = 'D'        // 已挂起
const TAPI_ORDER_STATE_DELETEDFOREXPIRE TAPIOrderStateType = 'E' // 到期删除
const TAPI_ORDER_STATE_EFFECT TAPIOrderStateType = 'F'           // 已生效——询价成功
const TAPI_ORDER_STATE_APPLY TAPIOrderStateType = 'G'            // 已申请——行权、弃权、套利等申请成功

// 计算方式
type TAPICalculateModeType TAPICHAR

const TAPI_CALCULATE_MODE_PERCENTAGE TAPICalculateModeType = '1'    // 比例
const TAPI_CALCULATE_MODE_QUOTA TAPICalculateModeType = '2'         // 定额
const TAPI_CALCULATE_MODE_CHAPERCENTAGE TAPICalculateModeType = '3' // 差值比例
const TAPI_CALCULATE_MODE_CHAQUOTA TAPICalculateModeType = '4'      // 差值定额
const TAPI_CALCULATE_MODE_DISCOUNT TAPICalculateModeType = '5'      // 折扣

// 成交来源
type TAPIMatchSourceType TAPICHAR

const TAPI_MATCH_SOURCE_ALL TAPIMatchSourceType = '0'           // 全部
const TAPI_MATCH_SOURCE_SELF_ETRADER TAPIMatchSourceType = '1'  // 自助电子单
const TAPI_MATCH_SOURCE_PROXY_ETRADER TAPIMatchSourceType = '2' // 代理电子单
const TAPI_MATCH_SOURCE_JTRADER TAPIMatchSourceType = '3'       // 外部电子单
const TAPI_MATCH_SOURCE_MANUAL TAPIMatchSourceType = '4'        // 人工录入单
const TAPI_MATCH_SOURCE_CARRY TAPIMatchSourceType = '5'         // carry单
const TAPI_MATCH_SOURCE_PROGRAM TAPIMatchSourceType = '6'       // 程式化单
const TAPI_MATCH_SOURCE_DELIVERY TAPIMatchSourceType = '7'      // 交割行权
const TAPI_MATCH_SOURCE_ABANDON TAPIMatchSourceType = '8'       // 期权放弃
const TAPI_MATCH_SOURCE_CHANNEL TAPIMatchSourceType = '9'       // 通道费
const TAPI_MATCH_SOURCE_TEMPORARY TAPIMatchSourceType = 'J'     // 临时成交
const TAPI_MATCH_SOURCE_PROGRAMMANUAL TAPIMatchSourceType = 'K' // 程序化手工单

// 开平方式
type TAPIOpenCloseModeType TAPICHAR

const TAPI_CLOSE_MODE_NONE TAPIOpenCloseModeType = 'N'       // 不区分开平
const TAPI_CLOSE_MODE_UNFINISHED TAPIOpenCloseModeType = 'U' // 平仓未了结
const TAPI_CLOSE_MODE_OPENCOVER TAPIOpenCloseModeType = 'C'  // 区分开仓和平仓
const TAPI_CLOSE_MODE_CLOSETODAY TAPIOpenCloseModeType = 'T' // 区分开仓、平仓和平今

// 币种组资金是否共享
type TAPIFutureAlgType TAPICHAR

const TAPI_FUTURES_ALG_ZHUBI TAPIFutureAlgType = 'Y'   // 独立
const TAPI_FUTURES_ALG_DINGSHI TAPIFutureAlgType = 'N' // 不独立

// 是否是基币
type TAPIOptionAlgType TAPICHAR

const TAPI_OPTION_ALG_FUTURES TAPIOptionAlgType = '1' // 是基币
const TAPI_OPTION_ALG_OPTION TAPIOptionAlgType = '2'  // 不是基币

// 本外币标识
type TAPIBankAccountLWFlagType TAPICHAR

const TAPI_LWFlag_L TAPIBankAccountLWFlagType = 'L' // 境内人民币账户
const TAPI_LWFlag_W TAPIBankAccountLWFlagType = 'W' // 客户境内外币账户

// 资金调整类型
type TAPICashAdjustTypeType TAPICHAR

const TAPI_CASHINOUT_MODE_FEEADJUST TAPICashAdjustTypeType = '0'       // 手续费调整
const TAPI_CASHINOUT_MODE_YKADJUST TAPICashAdjustTypeType = '1'        // 盈亏调整
const TAPI_CASHINOUT_MODE_PLEDGE TAPICashAdjustTypeType = '2'          // 质押资金
const TAPI_CASHINOUT_MODE_INTERESTREVENUE TAPICashAdjustTypeType = '3' // 利息收入
const TAPI_CASHINOUT_MODE_COLLECTIONCOST TAPICashAdjustTypeType = '4'  // 代扣费用
const TAPI_CASHINOUT_MODE_OTHER TAPICashAdjustTypeType = '5'           // 其它
const TAPI_CASHINOUT_MODE_COMPANY TAPICashAdjustTypeType = '6'         // 公司间拨账

// 期货保证金方式
type TAPIMarginCalculateModeType TAPICHAR

const TAPI_DEPOSITCALCULATE_MODE_FEN TAPIMarginCalculateModeType = '1' // 分笔
const TAPI_DEPOSITCALCULATE_MODE_SUO TAPIMarginCalculateModeType = '2' // 锁仓

// 期权保证金公式,据此判断该品种期权采用何种内置计算公式计算保证金
type TAPIOptionMarginCalculateModeType TAPICHAR

// 组合方向,品种两腿组合合约的买卖方向和第几腿相同
type TAPICmbDirectType TAPICHAR

const TAPI_CMB_DIRECT_FIRST TAPICmbDirectType = '1'  // 和第一腿一致
const TAPI_CMB_DIRECT_SECOND TAPICmbDirectType = '2' // 和第二腿一致

// 交割行权方式,期货和期权了结的方式
type TAPIDeliveryModeType TAPICHAR

const TAPI_DELIVERY_MODE_GOODS TAPIDeliveryModeType = 'G'   // 实物交割
const TAPI_DELIVERY_MODE_CASH TAPIDeliveryModeType = 'C'    // 现金交割
const TAPI_DELIVERY_MODE_EXECUTE TAPIDeliveryModeType = 'E' // 期权行权
const TAPI_DELIVERY_MODE_ABANDON TAPIDeliveryModeType = 'A' // 期权放弃
const TAPI_DELIVERY_MODE_HKF TAPIDeliveryModeType = 'H'     // 港交所行权

// 合约类型
type TAPIContractTypeType TAPICHAR

const TAPI_CONTRACT_TYPE_TRADEQUOTE TAPIContractTypeType = '1' // 交易行情合约
const TAPI_CONTRACT_TYPE_QUOTE TAPIContractTypeType = '2'      // 行情合约

// 策略单类型
type TAPITacticsTypeType TAPICHAR

const TAPI_TACTICS_TYPE_NONE TAPITacticsTypeType = 'N'      // 无
const TAPI_TACTICS_TYPE_READY TAPITacticsTypeType = 'M'     // 预备单(埋单)
const TAPI_TACTICS_TYPE_ATUO TAPITacticsTypeType = 'A'      // 自动单
const TAPI_TACTICS_TYPE_CONDITION TAPITacticsTypeType = 'C' // 条件单

// 订单操作类型
type TAPIORDERACT TAPICHAR

const APIORDER_INSERT TAPIORDERACT = '1'        // 报单
const APIORDER_MODIFY TAPIORDERACT = '2'        // 改单
const APIORDER_DELETE TAPIORDERACT = '3'        // 撤单
const APIORDER_SUSPEND TAPIORDERACT = '4'       // 挂起
const APIORDER_ACTIVATE TAPIORDERACT = '5'      // 激活
const APIORDER_SYSTEM_DELETE TAPIORDERACT = '6' // 删除

// 触发条件类型
type TAPITriggerConditionType TAPICHAR

const TAPI_TRIGGER_CONDITION_NONE TAPITriggerConditionType = 'N'   // 无
const TAPI_TRIGGER_CONDITION_GREAT TAPITriggerConditionType = 'G'  // 大于等于
const TAPI_TRIGGER_CONDITION_LITTLE TAPITriggerConditionType = 'L' // 小于等于

// 触发价格类型
type TAPITriggerPriceTypeType TAPICHAR

const TAPI_TRIGGER_PRICE_NONE TAPITriggerPriceTypeType = 'N' // 无
const TAPI_TRIGGER_PRICE_BUY TAPITriggerPriceTypeType = 'B'  // 买价
const TAPI_TRIGGER_PRICE_SELL TAPITriggerPriceTypeType = 'S' // 卖价
const TAPI_TRIGGER_PRICE_LAST TAPITriggerPriceTypeType = 'L' // 最新价

// 交易状态
type TAPITradingStateType TAPICHAR

const TAPI_TRADE_STATE_BID TAPITradingStateType = '1'          // 集合竞价
const TAPI_TRADE_STATE_MATCH TAPITradingStateType = '2'        // 集合竞价撮合
const TAPI_TRADE_STATE_CONTINUOUS TAPITradingStateType = '3'   // 连续交易
const TAPI_TRADE_STATE_PAUSED TAPITradingStateType = '4'       // 交易暂停
const TAPI_TRADE_STATE_CLOSE TAPITradingStateType = '5'        // 闭市
const TAPI_TRADE_STATE_DEALLAST TAPITradingStateType = '6'     // 闭市处理时间
const TAPI_TRADE_STATE_GWDISCONNECT TAPITradingStateType = '0' // 网关未连
const TAPI_TRADE_STATE_UNKNOWN TAPITradingStateType = 'N'      // 未知状态
const TAPI_TRADE_STATE_INITIALIZE TAPITradingStateType = 'I'   // 正初始化
const TAPI_TRADE_STATE_READY TAPITradingStateType = 'R'        // 准备就绪

// 忽略后台推送通知标记
type TAPINoticeIgnoreFlagType TAPIUINT32

// 委托查询类型
type TAPIOrderQryTypeType TAPICHAR

const TAPI_ORDER_QRY_TYPE_ALL TAPIOrderQryTypeType = 'A'     // 返回所有委托
const TAPI_ORDER_QRY_TYPE_UNENDED TAPIOrderQryTypeType = 'U' // 只返回未结束的委托

// 二次认证登录类型
type TAPILoginTypeType TAPICHAR

const TAPI_LOGINTYPE_NORMAL TAPILoginTypeType = 'N'    // 正常登录（信任该设备）
const TAPI_LOGINTYPE_TEMPORARY TAPILoginTypeType = 'T' // 临时登录

// 挂起委托请求结构
type TapAPIOrderDeactivateReq TapAPIOrderCancelReq

// 激活委托请求结构
type TapAPIOrderActivateReq TapAPIOrderCancelReq

// 删除委托请求结构
type TapAPIOrderDeleteReq TapAPIOrderCancelReq

// 消息接收者类型
type TAPIMsgReceiverType TAPICHAR

const TAPI_MSG_RECEIVER_ACCOUNTNO TAPIMsgReceiverType = '1'      // 单资金账号客户
const TAPI_MSG_RECEIVER_ACCOUNTGROUPNO TAPIMsgReceiverType = '2' // 资金账号分组
const TAPI_MSG_RECEIVER_ATTRIBUTE TAPIMsgReceiverType = '3'      // 符合属性的资金账号
const TAPI_MSG_RECEIVER_USERNO TAPIMsgReceiverType = '4'         // 指定登录用户

// 消息级别
type TAPIMsgLevelType TAPICHAR

const TAPI_MSG_LEVEL_NORMAL TAPIMsgLevelType = '1'    // 普通
const TAPI_MSG_LEVEL_IMPORTANT TAPIMsgLevelType = '2' // 重要
const TAPI_MSG_LEVEL_IMERGENCY TAPIMsgLevelType = '3' // 紧急

// 消息类型
type TAPIMsgTypeType TAPICHAR

const TAPI_Msg_TYPE_MANAGER TAPIMsgTypeType = '1'     // 管理
const TAPI_Msg_TYPE_RISKCONTROL TAPIMsgTypeType = '2' // 风险

// 账单类型
type TAPIBillTypeType TAPICHAR

const TAPI_BILL_DATE TAPIBillTypeType = 'D'  // 日账单
const TAPI_BILL_MONTH TAPIBillTypeType = 'M' // 月账单

// 帐单文件类型
type TAPIBillFileTypeType TAPICHAR

const TAPI_BILL_FILE_TXT TAPIBillFileTypeType = 'T' // txt格式文件
const TAPI_BILL_FILE_PDF TAPIBillFileTypeType = 'F' // pdf格式文件

// 历史委托流程查询应答数据结构
type TapAPIHisOrderProcessQryRsp TapAPIHisOrderQryRsp

// 结算类型
type TAPISettleFlagType TAPICHAR

const SettleFlag_AutoSettle TAPISettleFlagType = '0' // 自动结算

// 客户录单成交删除应答结构
type TapAPIFillLocalRemoveRsp TapAPIFillLocalRemoveReq

// 客户现货锁定量查询应答结构
type TapAPISpotLockQryRsp TapAPISpotLockQryReq

// 客户现货锁定量变更通知
type TapAPISpotLockDataNotice TapAPISpotLockDataRsp

// =============================================================================
type TapAPIAbnormalFalgType TAPICHAR

// =============================================================================
type TapAPICombineStrategyType [10]byte

// =============================================================================
type TapAPISpecialOrderTypeType TAPICHAR

// 申购方式
type TapAPIApplyTypeType TAPICHAR

// 申购状态
type TapAPIApplyStatusType TAPICHAR

// 客户IPO增加应答
type TapAPIAccountIPOAddRsp TapAPIAccountIPOQryRsp

// 客户IPO撤销应答
type TapAPIAccountIPOCancelRsp TapAPIAccountIPOQryRsp

// 客户IPO增加通知
type TapAPIAccountIPOAddNotice TapAPIAccountIPOQryRsp

// 客户IPO撤销通知
type TapAPIAccountIPOCancelNotice TapAPIAccountIPOQryRsp

// 证件类型
type TapAPICertificateTypeType string
