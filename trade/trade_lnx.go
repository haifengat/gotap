package trade

/*
#cgo CPPFLAGS: -fPIC -I./
#cgo linux LDFLAGS: -fPIC -L${SRCDIR}/../lib -Wl,-rpath ${SRCDIR}/../lib -ltaptrade -lstdc++

#include "struct_cgo.h"

void* CreateITapTradeAPINotify();
void* CreateTradeAPI(struct TapAPIApplicationInfo *appInfo, int *res);
void FreeITapTradeAPI(void* api);
void* GetAPIVersion();
void* GetErrorDescribe(int errorCode);

// ********************** 调用函数 ******************
int SetAPINotify(void* api,void  *pSpi);
int SetHostAddress(void* api,char *IP,unsigned short port);
int Login(void* api,struct TapAPITradeLoginAuth *loginAuth);
int RequestVertificateCode(void* api,unsigned int *sessionID,char * ContactInfo);
int SetVertificateCode(void* api,unsigned int *sessionID,struct TapAPISecondCertificationReq *req);
int Disconnect(void* api);
int ChangePassword(void* api,unsigned int *sessionID,struct TapAPIChangePasswordReq *req);
int AuthPassword(void* api,unsigned int *sessionID,struct TapAPIAuthPasswordReq *req);
int HaveCertainRight(void* api,int rightID);
int QryTradingDate(void* api,unsigned int *sessionID);
int SetReservedInfo(void* api,unsigned int *sessionID,char * info);
int QryAccount(void* api,unsigned int *sessionID,struct TapAPIAccQryReq *qryReq);
int QryFund(void* api,unsigned int *sessionID,struct TapAPIFundReq *qryReq);
int QryExchange(void* api,unsigned int *sessionID);
int QryCommodity(void* api,unsigned int *sessionID);
int QryContract(void* api,unsigned int *sessionID,struct TapAPICommodity *qryReq);
int InsertOrder(void* api,unsigned int *sessionID,char *ClientOrderNo,struct TapAPINewOrder *order);
int CancelOrder(void* api,unsigned int *sessionID,struct TapAPIOrderCancelReq *order);
int AmendOrder(void* api,unsigned int *sessionID,struct TapAPIAmendOrder *order);
int ActivateOrder(void* api,unsigned int * sessionID,struct TapAPIOrderCancelReq * order);
int QryOrder(void* api,unsigned int *sessionID,struct TapAPIOrderQryReq *qryReq);
int QryOrderProcess(void* api,unsigned int *sessionID,struct TapAPIOrderProcessQryReq *qryReq);
int QryFill(void* api,unsigned int *sessionID,struct TapAPIFillQryReq *qryReq);
int QryPosition(void* api,unsigned int *sessionID,struct TapAPIPositionQryReq *qryReq);
int QryPositionSummary(void* api,unsigned int *sessionID,struct TapAPIPositionQryReq *qryReq);
int QryCurrency(void* api,unsigned int *sessionID);
int QryAccountCashAdjust(void* api,unsigned int *sessionID,struct TapAPIAccountCashAdjustQryReq *qryReq);
int QryTradeMessage(void* api,unsigned int *sessionID,struct TapAPITradeMessageReq *qryReq);
int QryBill(void* api,unsigned int *sessionID,struct TapAPIBillQryReq *qryReq);
int QryHisOrder(void* api,unsigned int *sessionID,struct TapAPIHisOrderQryReq *qryReq);
int QryHisOrderProcess(void* api,unsigned int *sessionID,struct TapAPIHisOrderProcessQryReq *qryReq);
int QryHisMatch(void* api,unsigned int *sessionID,struct TapAPIHisMatchQryReq *qryReq);
int QryHisPosition(void* api,unsigned int *sessionID,struct TapAPIHisPositionQryReq *qryReq);
int QryHisDelivery(void* api,unsigned int *sessionID,struct TapAPIHisDeliveryQryReq *qryReq);
int QryAccountFeeRent(void* api,unsigned int *sessionID,struct TapAPIAccountFeeRentQryReq *qryReq);
int QryAccountMarginRent(void* api,unsigned int *sessionID,struct TapAPIAccountMarginRentQryReq *qryReq);
int InsertHKMarketOrder(void* api,unsigned int *sessionID,char *ClientBuyOrderNo,char *ClientSellOrderNo,struct TapAPIOrderMarketInsertReq *order);
int CancelHKMarketOrder(void* api,unsigned int *sessionID,struct TapAPIOrderMarketDeleteReq *order);
int OrderLocalRemove(void* api,unsigned int *sessionID,struct TapAPIOrderLocalRemoveReq *order);
int OrderLocalInput(void* api,unsigned int *sessionID,struct TapAPIOrderLocalInputReq *order);
int OrderLocalModify(void* api,unsigned int *sessionID,struct TapAPIOrderLocalModifyReq *order);
int OrderLocalTransfer(void* api,unsigned int *sessionID,struct TapAPIOrderLocalTransferReq *order);
int FillLocalInput(void* api,unsigned int *sessionID,struct TapAPIFillLocalInputReq *fill);
int FillLocalRemove(void* api,unsigned int *sessionID,struct TapAPIFillLocalRemoveReq *fill);
int QrySpotLock(void* api,unsigned int *sessionID,struct TapAPISpotLockQryReq *qryReq);
int SubmitUserLoginInfo(void* api,unsigned int *sessionID,struct TapAPISubmitUserLoginInfo *qryReq);
int InsertSpecialOrder(void* api,unsigned int *sessionID,char *clientorderno,struct TapAPISpecialOrderInsertReq *order);
int QrySpecialOrder(void* api,unsigned int *sessionID,struct TapAPISpecialOrderQryReq *qryReq);
int QryCombinePosition(void* api,unsigned int *sessionID,struct TapAPICombinePositionQryReq *qryReq);
int QryUserTrustDevice(void* api,unsigned int *sessionID,struct TapAPIUserTrustDeviceQryReq *qryReq);
int AddUserTrustDevice(void* api,unsigned int *sessionID,struct TapAPIUserTrustDeviceAddReq *qryReq);
int DelUserTrustDevice(void* api,unsigned int *sessionID,struct TapAPIUserTrustDeviceDelReq *qryReq);
int QryIPOInfo(void* api,unsigned int *sessionID,struct TapAPIIPOInfoQryReq *qryReq);
int QryIPOStockQty(void* api,unsigned int *sessionID,struct TapAPIAvailableApplyQryReq *qryReq);
int QryAccountIPO(void* api,unsigned int *sessionID,struct TapAPIAccountIPOQryReq *qryReq);
int AddAccountIPO(void* api,unsigned int *sessionID,struct TapAPIAccountIPOAddReq *qryReq);
int CancelAccountIPO(void* api,unsigned int *sessionID,struct TapAPIAccountIPOCancelReq *qryReq);
int UnFreeze(void* api,struct TapAPITradeLoginAuth *loginAuth);
int VerificateUnFreezeInfo(void* api,unsigned int *sessionID,struct TapAPIVerifyIdentityReq *qryReq);



// ********************** 响应函数 ******************
void SetOnConnect(void*, void*);
void SetOnRspLogin(void*, void*);
void SetOnRtnContactInfo(void*, void*);
void SetOnRspRequestVertificateCode(void*, void*);
void SetOnExpriationDate(void*, void*);
void SetOnAPIReady(void*, void*);
void SetOnDisconnect(void*, void*);
void SetOnRspChangePassword(void*, void*);
void SetOnRspAuthPassword(void*, void*);
void SetOnRspQryTradingDate(void*, void*);
void SetOnRspSetReservedInfo(void*, void*);
void SetOnRspQryAccount(void*, void*);
void SetOnRspQryFund(void*, void*);
void SetOnRtnFund(void*, void*);
void SetOnRspQryExchange(void*, void*);
void SetOnRspQryCommodity(void*, void*);
void SetOnRspQryContract(void*, void*);
void SetOnRtnContract(void*, void*);
void SetOnRspOrderAction(void*, void*);
void SetOnRtnOrder(void*, void*);
void SetOnRspQryOrder(void*, void*);
void SetOnRspQryOrderProcess(void*, void*);
void SetOnRspQryFill(void*, void*);
void SetOnRtnFill(void*, void*);
void SetOnRspQryPosition(void*, void*);
void SetOnRtnPosition(void*, void*);
void SetOnRspQryPositionSummary(void*, void*);
void SetOnRtnPositionSummary(void*, void*);
void SetOnRtnPositionProfit(void*, void*);
void SetOnRspQryCurrency(void*, void*);
void SetOnRspQryTradeMessage(void*, void*);
void SetOnRtnTradeMessage(void*, void*);
void SetOnRspQryHisOrder(void*, void*);
void SetOnRspQryHisOrderProcess(void*, void*);
void SetOnRspQryHisMatch(void*, void*);
void SetOnRspQryHisPosition(void*, void*);
void SetOnRspQryHisDelivery(void*, void*);
void SetOnRspQryAccountCashAdjust(void*, void*);
void SetOnRspQryBill(void*, void*);
void SetOnRspQryAccountFeeRent(void*, void*);
void SetOnRspQryAccountMarginRent(void*, void*);
void SetOnRspHKMarketOrderInsert(void*, void*);
void SetOnRspHKMarketOrderDelete(void*, void*);
void SetOnHKMarketQuoteNotice(void*, void*);
void SetOnRspOrderLocalRemove(void*, void*);
void SetOnRspOrderLocalInput(void*, void*);
void SetOnRspOrderLocalModify(void*, void*);
void SetOnRspOrderLocalTransfer(void*, void*);
void SetOnRspFillLocalInput(void*, void*);
void SetOnRspFillLocalRemove(void*, void*);
void SetOnRspQrySpotLock(void*, void*);
void SetOnRtnSpotLock(void*, void*);
void SetOnRspSubmitUserLoginInfo(void*, void*);
void SetOnRspSpecialOrderAction(void*, void*);
void SetOnRtnSpecialOrder(void*, void*);
void SetOnRspQrySpecialOrder(void*, void*);
void SetOnRspQryCombinePosition(void*, void*);
void SetOnRtnCombinePosition(void*, void*);


// Onxxx 为go 的 export所用
void exOnConnect( char * HostAddress);
void exOnRspLogin( int errorCode,  struct TapAPITradeLoginRspInfo *loginRspInfo);
void exOnRtnContactInfo( int errorCode,  char isLast,  char * ContactInfo);
void exOnRspRequestVertificateCode( unsigned int sessionID,  int errorCode,  struct TapAPIRequestVertificateCodeRsp *rsp);
void exOnExpriationDate( char * date,  int days);
void exOnAPIReady( int errorCode);
void exOnDisconnect( int reasonCode);
void exOnRspChangePassword( unsigned int sessionID,  int errorCode);
void exOnRspAuthPassword( unsigned int sessionID,  int errorCode);
void exOnRspQryTradingDate( unsigned int sessionID,  int errorCode,  struct TapAPITradingCalendarQryRsp *info);
void exOnRspSetReservedInfo( unsigned int sessionID,  int errorCode,  char * info);
void exOnRspQryAccount( unsigned int sessionID,  unsigned int errorCode,  char isLast,  struct TapAPIAccountInfo *info);
void exOnRspQryFund( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIFundData *info);
void exOnRtnFund( struct TapAPIFundData *info);
void exOnRspQryExchange( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIExchangeInfo *info);
void exOnRspQryCommodity( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPICommodityInfo *info);
void exOnRspQryContract( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPITradeContractInfo *info);
void exOnRtnContract( struct TapAPITradeContractInfo *info);
void exOnRspOrderAction( unsigned int sessionID,  int errorCode,  struct TapAPIOrderActionRsp *info);
void exOnRtnOrder( struct TapAPIOrderInfoNotice *info);
void exOnRspQryOrder( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIOrderInfo *info);
void exOnRspQryOrderProcess( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIOrderInfo *info);
void exOnRspQryFill( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIFillInfo *info);
void exOnRtnFill( struct TapAPIFillInfo *info);
void exOnRspQryPosition( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIPositionInfo *info);
void exOnRtnPosition( struct TapAPIPositionInfo *info);
void exOnRspQryPositionSummary( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIPositionSummary *info);
void exOnRtnPositionSummary( struct TapAPIPositionSummary *info);
void exOnRtnPositionProfit( struct TapAPIPositionProfitNotice *info);
void exOnRspQryCurrency( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPICurrencyInfo *info);
void exOnRspQryTradeMessage( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPITradeMessage *info);
void exOnRtnTradeMessage( struct TapAPITradeMessage *info);
void exOnRspQryHisOrder( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIHisOrderQryRsp *info);
void exOnRspQryHisOrderProcess( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIHisOrderProcessQryReq *info);
void exOnRspQryHisMatch( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIHisMatchQryRsp *info);
void exOnRspQryHisPosition( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIHisPositionQryRsp *info);
void exOnRspQryHisDelivery( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIHisDeliveryQryRsp *info);
void exOnRspQryAccountCashAdjust( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIAccountCashAdjustQryRsp *info);
void exOnRspQryBill( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIBillQryRsp *info);
void exOnRspQryAccountFeeRent( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIAccountFeeRentQryRsp *info);
void exOnRspQryAccountMarginRent( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPIAccountMarginRentQryRsp *info);
void exOnRspHKMarketOrderInsert( unsigned int sessionID,  int errorCode,  struct TapAPIOrderMarketInsertReq *info);
void exOnRspHKMarketOrderDelete( unsigned int sessionID,  int errorCode,  struct TapAPIOrderMarketDeleteReq *info);
void exOnHKMarketQuoteNotice( struct TapAPIOrderQuoteMarketNotice *info);
void exOnRspOrderLocalRemove( unsigned int sessionID,  int errorCode,  struct TapAPIOrderLocalRemoveRsp *info);
void exOnRspOrderLocalInput( unsigned int sessionID,  int errorCode,  struct TapAPIOrderLocalInputReq *info);
void exOnRspOrderLocalModify( unsigned int sessionID,  int errorCode,  struct TapAPIOrderLocalModifyReq *info);
void exOnRspOrderLocalTransfer( unsigned int sessionID,  int errorCode,  struct TapAPIOrderLocalTransferReq *info);
void exOnRspFillLocalInput( unsigned int sessionID,  int errorCode,  struct TapAPIFillLocalInputReq *info);
void exOnRspFillLocalRemove( unsigned int sessionID,  int errorCode,  struct TapAPIFillLocalRemoveReq *info);
void exOnRspQrySpotLock( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPISpotLockDataRsp *info);
void exOnRtnSpotLock( struct TapAPISpotLockDataRsp *info);
void exOnRspSubmitUserLoginInfo( unsigned int sessionID,  int errorCode,  char isLast);
void exOnRspSpecialOrderAction( unsigned int sessionID,  int errorCode,  struct TapAPISpecialOrderInfo *info);
void exOnRtnSpecialOrder( struct TapAPISpecialOrderInfo *info);
void exOnRspQrySpecialOrder( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPISpecialOrderInfo *info);
void exOnRspQryCombinePosition( unsigned int sessionID,  int errorCode,  char isLast,  struct TapAPICombinePositionInfo *info);
void exOnRtnCombinePosition( struct TapAPICombinePositionInfo *info);


#include <stdlib.h>
#include <stdint.h>
*/
import "C"

import (
	"errors"
	"fmt"
	"io"
	"strings"
	"unsafe"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

func GBK(chars *C.char) string {
	rd := transform.NewReader(strings.NewReader(C.GoString(chars)), simplifiedchinese.GBK.NewDecoder())
	bs, _ := io.ReadAll(rd)
	return string(bs)
}

var t *Trade

// NewTrade
//
//	@param appID 外盘:""
//	@param authCode
//	@return *Trade
func NewTrade(appID, authCode string) (*Trade, error) {
	if t != nil {
		return t, nil
	}
	t = &Trade{}

	ver := t.GetITapTradeAPIVersion()
	fmt.Println("版本号:", ver)

	app := TapAPIApplicationInfo{
		// AuthCode: def.TAPIAUTHCODE(),
		// LogLevel: def.APILOGLEVEL_DEBUG,
	}
	if len(appID) > 0 {
		copy(app.APPID[:], []byte(appID))
	}
	copy(app.AuthCode[:], authCode)
	copy(app.KeyOperationLogPath[:], "./tap_trade_log")
	var res TAPIINT32
	t.CreateITapTradeAPI(&app, &res)
	if t.api == nil { // 创建api失败
		return nil, errors.New(t.GetITapErrorDescribe(res))
	}
	t.CreateITapTradeAPINotify()

	t.RegCallBack()
	t.SetSpi()
	return t, nil
}

// Trade 交易接口
type Trade struct {
	api, spi unsafe.Pointer
	// 定义响应函数变量
	OnConnect                   func(HostAddress string)
	OnRspLogin                  func(errorCode TAPIINT32, loginRspInfo *TapAPITradeLoginRspInfo)
	OnRtnContactInfo            func(errorCode TAPIINT32, isLast TAPIYNFLAG, ContactInfo string)
	OnRspRequestVertificateCode func(sessionID TAPIUINT32, errorCode TAPIINT32, rsp *TapAPIRequestVertificateCodeRsp)
	OnExpriationDate            func(date string, days int)
	OnAPIReady                  func(errorCode TAPIINT32)
	OnDisconnect                func(reasonCode TAPIINT32)
	OnRspChangePassword         func(sessionID TAPIUINT32, errorCode TAPIINT32)
	OnRspAuthPassword           func(sessionID TAPIUINT32, errorCode TAPIINT32)
	OnRspQryTradingDate         func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPITradingCalendarQryRsp)
	OnRspSetReservedInfo        func(sessionID TAPIUINT32, errorCode TAPIINT32, info string)
	OnRspQryAccount             func(sessionID TAPIUINT32, errorCode TAPIUINT32, isLast TAPIYNFLAG, info *TapAPIAccountInfo)
	OnRspQryFund                func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIFundData)
	OnRtnFund                   func(info *TapAPIFundData)
	OnRspQryExchange            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIExchangeInfo)
	OnRspQryCommodity           func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPICommodityInfo)
	OnRspQryContract            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPITradeContractInfo)
	OnRtnContract               func(info *TapAPITradeContractInfo)
	OnRspOrderAction            func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderActionRsp)
	OnRtnOrder                  func(info *TapAPIOrderInfoNotice)
	OnRspQryOrder               func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIOrderInfo)
	OnRspQryOrderProcess        func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIOrderInfo)
	OnRspQryFill                func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIFillInfo)
	OnRtnFill                   func(info *TapAPIFillInfo)
	OnRspQryPosition            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIPositionInfo)
	OnRtnPosition               func(info *TapAPIPositionInfo)
	OnRspQryPositionSummary     func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIPositionSummary)
	OnRtnPositionSummary        func(info *TapAPIPositionSummary)
	OnRtnPositionProfit         func(info *TapAPIPositionProfitNotice)
	OnRspQryCurrency            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPICurrencyInfo)
	OnRspQryTradeMessage        func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPITradeMessage)
	OnRtnTradeMessage           func(info *TapAPITradeMessage)
	OnRspQryHisOrder            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIHisOrderQryRsp)
	OnRspQryHisOrderProcess     func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIHisOrderProcessQryReq)
	OnRspQryHisMatch            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIHisMatchQryRsp)
	OnRspQryHisPosition         func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIHisPositionQryRsp)
	OnRspQryHisDelivery         func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIHisDeliveryQryRsp)
	OnRspQryAccountCashAdjust   func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIAccountCashAdjustQryRsp)
	OnRspQryBill                func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIBillQryRsp)
	OnRspQryAccountFeeRent      func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIAccountFeeRentQryRsp)
	OnRspQryAccountMarginRent   func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPIAccountMarginRentQryRsp)
	OnRspHKMarketOrderInsert    func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderMarketInsertReq)
	OnRspHKMarketOrderDelete    func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderMarketDeleteReq)
	OnHKMarketQuoteNotice       func(info *TapAPIOrderQuoteMarketNotice)
	OnRspOrderLocalRemove       func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderLocalRemoveRsp)
	OnRspOrderLocalInput        func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderLocalInputReq)
	OnRspOrderLocalModify       func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderLocalModifyReq)
	OnRspOrderLocalTransfer     func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIOrderLocalTransferReq)
	OnRspFillLocalInput         func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIFillLocalInputReq)
	OnRspFillLocalRemove        func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPIFillLocalRemoveReq)
	OnRspQrySpotLock            func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPISpotLockDataRsp)
	OnRtnSpotLock               func(info *TapAPISpotLockDataRsp)
	OnRspSubmitUserLoginInfo    func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG)
	OnRspSpecialOrderAction     func(sessionID TAPIUINT32, errorCode TAPIINT32, info *TapAPISpecialOrderInfo)
	OnRtnSpecialOrder           func(info *TapAPISpecialOrderInfo)
	OnRspQrySpecialOrder        func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPISpecialOrderInfo)
	OnRspQryCombinePosition     func(sessionID TAPIUINT32, errorCode TAPIINT32, isLast TAPIYNFLAG, info *TapAPICombinePositionInfo)
	OnRtnCombinePosition        func(info *TapAPICombinePositionInfo)
}

func (t *Trade) CreateITapTradeAPINotify() {
	t.spi = C.CreateITapTradeAPINotify()
}
func (t *Trade) CreateITapTradeAPI(appInfo *TapAPIApplicationInfo, res *TAPIINT32) {
	t.api = C.CreateTradeAPI((*C.struct_TapAPIApplicationInfo)(unsafe.Pointer(appInfo)), (*C.int)(unsafe.Pointer(res)))
}
func (t *Trade) FreeITapTradeAPI(api unsafe.Pointer) {
	C.FreeITapTradeAPI(api)
}
func (t *Trade) GetITapTradeAPIVersion() string {
	return C.GoString((*C.char)(C.GetAPIVersion()))
}
func (t *Trade) GetITapErrorDescribe(errorCode TAPIINT32) string {
	return GBK((*C.char)(C.GetErrorDescribe(C.int(errorCode.Int32()))))
}

// 替代 SetAPINotify
func (t *Trade) SetSpi() { C.SetAPINotify(t.api, t.spi) }

// ********************** 调用函数 ******************

func (t *Trade) SetAPINotify(pSpi unsafe.Pointer) C.int { return C.SetAPINotify(t.api, pSpi) }

func (t *Trade) SetHostAddress(IP string, port TAPIUINT16) C.int {
	ip := C.CString(IP)
	defer C.free(unsafe.Pointer(ip))
	return C.SetHostAddress(t.api, ip, C.ushort(port))
}

func (t *Trade) Login(loginAuth *TapAPITradeLoginAuth) C.int {
	return C.Login(t.api, (*C.struct_TapAPITradeLoginAuth)(unsafe.Pointer(loginAuth)))
}

func (t *Trade) RequestVertificateCode(sessionID *TAPIUINT32, ContactInfo TAPISTR_40) C.int {
	return C.RequestVertificateCode(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.char)(unsafe.Pointer(&ContactInfo[0])))
}

func (t *Trade) SetVertificateCode(sessionID *TAPIUINT32, req *TapAPISecondCertificationReq) C.int {
	return C.SetVertificateCode(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPISecondCertificationReq)(unsafe.Pointer(req)))
}

func (t *Trade) Disconnect() C.int { return C.Disconnect(t.api) }

func (t *Trade) ChangePassword(sessionID *TAPIUINT32, req *TapAPIChangePasswordReq) C.int {
	return C.ChangePassword(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIChangePasswordReq)(unsafe.Pointer(req)))
}

func (t *Trade) AuthPassword(sessionID *TAPIUINT32, req *TapAPIAuthPasswordReq) C.int {
	return C.AuthPassword(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAuthPasswordReq)(unsafe.Pointer(req)))
}

func (t *Trade) HaveCertainRight(rightID TAPIRightIDType) C.int {
	return C.HaveCertainRight(t.api, C.int(TAPIINT32(rightID).Int32()))
}

func (t *Trade) QryTradingDate(sessionID *TAPIUINT32) C.int {
	return C.QryTradingDate(t.api, (*C.uint)(unsafe.Pointer(sessionID)))
}

func (t *Trade) SetReservedInfo(sessionID *TAPIUINT32, info TAPISTR_50) C.int {
	return C.SetReservedInfo(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.char)(unsafe.Pointer(&info[0])))
}

func (t *Trade) QryAccount(sessionID *TAPIUINT32, qryReq *TapAPIAccQryReq) C.int {
	return C.QryAccount(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryFund(sessionID *TAPIUINT32, qryReq *TapAPIFundReq) C.int {
	return C.QryFund(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIFundReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryExchange(sessionID *TAPIUINT32) C.int {
	return C.QryExchange(t.api, (*C.uint)(unsafe.Pointer(sessionID)))
}

func (t *Trade) QryCommodity(sessionID *TAPIUINT32) C.int {
	return C.QryCommodity(t.api, (*C.uint)(unsafe.Pointer(sessionID)))
}

func (t *Trade) QryContract(sessionID *TAPIUINT32, qryReq *TapAPICommodity) C.int {
	return C.QryContract(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPICommodity)(unsafe.Pointer(qryReq)))
}

func (t *Trade) InsertOrder(sessionID *TAPIUINT32, ClientOrderNo *TAPISTR_50, order *TapAPINewOrder) C.int {
	return C.InsertOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.char)(unsafe.Pointer(&ClientOrderNo[0])), (*C.struct_TapAPINewOrder)(unsafe.Pointer(order)))
}

func (t *Trade) CancelOrder(sessionID *TAPIUINT32, order *TapAPIOrderCancelReq) C.int {
	return C.CancelOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderCancelReq)(unsafe.Pointer(order)))
}

func (t *Trade) AmendOrder(sessionID *TAPIUINT32, order *TapAPIAmendOrder) C.int {
	return C.AmendOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAmendOrder)(unsafe.Pointer(order)))
}

func (t *Trade) ActivateOrder(sessionID *TAPIUINT32, order *TapAPIOrderCancelReq) C.int {
	return C.ActivateOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderCancelReq)(unsafe.Pointer(order)))
}

func (t *Trade) QryOrder(sessionID *TAPIUINT32, qryReq *TapAPIOrderQryReq) C.int {
	return C.QryOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryOrderProcess(sessionID *TAPIUINT32, qryReq *TapAPIOrderProcessQryReq) C.int {
	return C.QryOrderProcess(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderProcessQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryFill(sessionID *TAPIUINT32, qryReq *TapAPIFillQryReq) C.int {
	return C.QryFill(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIFillQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryPosition(sessionID *TAPIUINT32, qryReq *TapAPIPositionQryReq) C.int {
	return C.QryPosition(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIPositionQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryPositionSummary(sessionID *TAPIUINT32, qryReq *TapAPIPositionQryReq) C.int {
	return C.QryPositionSummary(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIPositionQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryCurrency(sessionID *TAPIUINT32) C.int {
	return C.QryCurrency(t.api, (*C.uint)(unsafe.Pointer(sessionID)))
}

func (t *Trade) QryAccountCashAdjust(sessionID *TAPIUINT32, qryReq *TapAPIAccountCashAdjustQryReq) C.int {
	return C.QryAccountCashAdjust(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountCashAdjustQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryTradeMessage(sessionID *TAPIUINT32, qryReq *TapAPITradeMessageReq) C.int {
	return C.QryTradeMessage(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPITradeMessageReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryBill(sessionID *TAPIUINT32, qryReq *TapAPIBillQryReq) C.int {
	return C.QryBill(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIBillQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryHisOrder(sessionID *TAPIUINT32, qryReq *TapAPIHisOrderQryReq) C.int {
	return C.QryHisOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIHisOrderQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryHisOrderProcess(sessionID *TAPIUINT32, qryReq *TapAPIHisOrderProcessQryReq) C.int {
	return C.QryHisOrderProcess(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIHisOrderProcessQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryHisMatch(sessionID *TAPIUINT32, qryReq *TapAPIHisMatchQryReq) C.int {
	return C.QryHisMatch(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIHisMatchQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryHisPosition(sessionID *TAPIUINT32, qryReq *TapAPIHisPositionQryReq) C.int {
	return C.QryHisPosition(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIHisPositionQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryHisDelivery(sessionID *TAPIUINT32, qryReq *TapAPIHisDeliveryQryReq) C.int {
	return C.QryHisDelivery(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIHisDeliveryQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryAccountFeeRent(sessionID *TAPIUINT32, qryReq *TapAPIAccountFeeRentQryReq) C.int {
	return C.QryAccountFeeRent(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountFeeRentQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryAccountMarginRent(sessionID *TAPIUINT32, qryReq *TapAPIAccountMarginRentQryReq) C.int {
	return C.QryAccountMarginRent(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountMarginRentQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) InsertHKMarketOrder(sessionID *TAPIUINT32, ClientBuyOrderNo TAPISTR_50, ClientSellOrderNo TAPISTR_50, order *TapAPIOrderMarketInsertReq) C.int {
	return C.InsertHKMarketOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.char)(unsafe.Pointer(&ClientBuyOrderNo[0])), (*C.char)(unsafe.Pointer(&ClientSellOrderNo[0])), (*C.struct_TapAPIOrderMarketInsertReq)(unsafe.Pointer(order)))
}

func (t *Trade) CancelHKMarketOrder(sessionID *TAPIUINT32, order *TapAPIOrderMarketDeleteReq) C.int {
	return C.CancelHKMarketOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderMarketDeleteReq)(unsafe.Pointer(order)))
}

func (t *Trade) OrderLocalRemove(sessionID *TAPIUINT32, order *TapAPIOrderLocalRemoveReq) C.int {
	return C.OrderLocalRemove(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderLocalRemoveReq)(unsafe.Pointer(order)))
}

func (t *Trade) OrderLocalInput(sessionID *TAPIUINT32, order *TapAPIOrderLocalInputReq) C.int {
	return C.OrderLocalInput(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderLocalInputReq)(unsafe.Pointer(order)))
}

func (t *Trade) OrderLocalModify(sessionID *TAPIUINT32, order *TapAPIOrderLocalModifyReq) C.int {
	return C.OrderLocalModify(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderLocalModifyReq)(unsafe.Pointer(order)))
}

func (t *Trade) OrderLocalTransfer(sessionID *TAPIUINT32, order *TapAPIOrderLocalTransferReq) C.int {
	return C.OrderLocalTransfer(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIOrderLocalTransferReq)(unsafe.Pointer(order)))
}

func (t *Trade) FillLocalInput(sessionID *TAPIUINT32, fill *TapAPIFillLocalInputReq) C.int {
	return C.FillLocalInput(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIFillLocalInputReq)(unsafe.Pointer(fill)))
}

func (t *Trade) FillLocalRemove(sessionID *TAPIUINT32, fill *TapAPIFillLocalRemoveReq) C.int {
	return C.FillLocalRemove(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIFillLocalRemoveReq)(unsafe.Pointer(fill)))
}

func (t *Trade) QrySpotLock(sessionID *TAPIUINT32, qryReq *TapAPISpotLockQryReq) C.int {
	return C.QrySpotLock(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPISpotLockQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) SubmitUserLoginInfo(sessionID *TAPIUINT32, qryReq *TapAPISubmitUserLoginInfo) C.int {
	return C.SubmitUserLoginInfo(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPISubmitUserLoginInfo)(unsafe.Pointer(qryReq)))
}

func (t *Trade) InsertSpecialOrder(sessionID *TAPIUINT32, clientorderno TAPISTR_50, order *TapAPISpecialOrderInsertReq) C.int {
	return C.InsertSpecialOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.char)(unsafe.Pointer(&clientorderno[0])), (*C.struct_TapAPISpecialOrderInsertReq)(unsafe.Pointer(order)))
}

func (t *Trade) QrySpecialOrder(sessionID *TAPIUINT32, qryReq *TapAPISpecialOrderQryReq) C.int {
	return C.QrySpecialOrder(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPISpecialOrderQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryCombinePosition(sessionID *TAPIUINT32, qryReq *TapAPICombinePositionQryReq) C.int {
	return C.QryCombinePosition(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPICombinePositionQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryUserTrustDevice(sessionID *TAPIUINT32, qryReq *TapAPIUserTrustDeviceQryReq) C.int {
	return C.QryUserTrustDevice(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIUserTrustDeviceQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) AddUserTrustDevice(sessionID *TAPIUINT32, qryReq *TapAPIUserTrustDeviceAddReq) C.int {
	return C.AddUserTrustDevice(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIUserTrustDeviceAddReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) DelUserTrustDevice(sessionID *TAPIUINT32, qryReq *TapAPIUserTrustDeviceDelReq) C.int {
	return C.DelUserTrustDevice(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIUserTrustDeviceDelReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryIPOInfo(sessionID *TAPIUINT32, qryReq *TapAPIIPOInfoQryReq) C.int {
	return C.QryIPOInfo(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIIPOInfoQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryIPOStockQty(sessionID *TAPIUINT32, qryReq *TapAPIAvailableApplyQryReq) C.int {
	return C.QryIPOStockQty(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAvailableApplyQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) QryAccountIPO(sessionID *TAPIUINT32, qryReq *TapAPIAccountIPOQryReq) C.int {
	return C.QryAccountIPO(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountIPOQryReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) AddAccountIPO(sessionID *TAPIUINT32, qryReq *TapAPIAccountIPOAddReq) C.int {
	return C.AddAccountIPO(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountIPOAddReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) CancelAccountIPO(sessionID *TAPIUINT32, qryReq *TapAPIAccountIPOCancelReq) C.int {
	return C.CancelAccountIPO(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIAccountIPOCancelReq)(unsafe.Pointer(qryReq)))
}

func (t *Trade) UnFreeze(loginAuth *TapAPITradeLoginAuth) C.int {
	return C.UnFreeze(t.api, (*C.struct_TapAPITradeLoginAuth)(unsafe.Pointer(loginAuth)))
}

func (t *Trade) VerificateUnFreezeInfo(sessionID *TAPIUINT32, qryReq *TapAPIVerifyIdentityReq) C.int {
	return C.VerificateUnFreezeInfo(t.api, (*C.uint)(unsafe.Pointer(sessionID)), (*C.struct_TapAPIVerifyIdentityReq)(unsafe.Pointer(qryReq)))
}

// ********************** 响应函数 ******************
func (t *Trade) RegCallBack() {
	C.SetOnConnect(t.spi, C.exOnConnect)
	C.SetOnRspLogin(t.spi, C.exOnRspLogin)
	C.SetOnRtnContactInfo(t.spi, C.exOnRtnContactInfo)
	C.SetOnRspRequestVertificateCode(t.spi, C.exOnRspRequestVertificateCode)
	C.SetOnExpriationDate(t.spi, C.exOnExpriationDate)
	C.SetOnAPIReady(t.spi, C.exOnAPIReady)
	C.SetOnDisconnect(t.spi, C.exOnDisconnect)
	C.SetOnRspChangePassword(t.spi, C.exOnRspChangePassword)
	C.SetOnRspAuthPassword(t.spi, C.exOnRspAuthPassword)
	C.SetOnRspQryTradingDate(t.spi, C.exOnRspQryTradingDate)
	C.SetOnRspSetReservedInfo(t.spi, C.exOnRspSetReservedInfo)
	C.SetOnRspQryAccount(t.spi, C.exOnRspQryAccount)
	C.SetOnRspQryFund(t.spi, C.exOnRspQryFund)
	C.SetOnRtnFund(t.spi, C.exOnRtnFund)
	C.SetOnRspQryExchange(t.spi, C.exOnRspQryExchange)
	C.SetOnRspQryCommodity(t.spi, C.exOnRspQryCommodity)
	C.SetOnRspQryContract(t.spi, C.exOnRspQryContract)
	C.SetOnRtnContract(t.spi, C.exOnRtnContract)
	C.SetOnRspOrderAction(t.spi, C.exOnRspOrderAction)
	C.SetOnRtnOrder(t.spi, C.exOnRtnOrder)
	C.SetOnRspQryOrder(t.spi, C.exOnRspQryOrder)
	C.SetOnRspQryOrderProcess(t.spi, C.exOnRspQryOrderProcess)
	C.SetOnRspQryFill(t.spi, C.exOnRspQryFill)
	C.SetOnRtnFill(t.spi, C.exOnRtnFill)
	C.SetOnRspQryPosition(t.spi, C.exOnRspQryPosition)
	C.SetOnRtnPosition(t.spi, C.exOnRtnPosition)
	C.SetOnRspQryPositionSummary(t.spi, C.exOnRspQryPositionSummary)
	C.SetOnRtnPositionSummary(t.spi, C.exOnRtnPositionSummary)
	C.SetOnRtnPositionProfit(t.spi, C.exOnRtnPositionProfit)
	C.SetOnRspQryCurrency(t.spi, C.exOnRspQryCurrency)
	C.SetOnRspQryTradeMessage(t.spi, C.exOnRspQryTradeMessage)
	C.SetOnRtnTradeMessage(t.spi, C.exOnRtnTradeMessage)
	C.SetOnRspQryHisOrder(t.spi, C.exOnRspQryHisOrder)
	C.SetOnRspQryHisOrderProcess(t.spi, C.exOnRspQryHisOrderProcess)
	C.SetOnRspQryHisMatch(t.spi, C.exOnRspQryHisMatch)
	C.SetOnRspQryHisPosition(t.spi, C.exOnRspQryHisPosition)
	C.SetOnRspQryHisDelivery(t.spi, C.exOnRspQryHisDelivery)
	C.SetOnRspQryAccountCashAdjust(t.spi, C.exOnRspQryAccountCashAdjust)
	C.SetOnRspQryBill(t.spi, C.exOnRspQryBill)
	C.SetOnRspQryAccountFeeRent(t.spi, C.exOnRspQryAccountFeeRent)
	C.SetOnRspQryAccountMarginRent(t.spi, C.exOnRspQryAccountMarginRent)
	C.SetOnRspHKMarketOrderInsert(t.spi, C.exOnRspHKMarketOrderInsert)
	C.SetOnRspHKMarketOrderDelete(t.spi, C.exOnRspHKMarketOrderDelete)
	C.SetOnHKMarketQuoteNotice(t.spi, C.exOnHKMarketQuoteNotice)
	C.SetOnRspOrderLocalRemove(t.spi, C.exOnRspOrderLocalRemove)
	C.SetOnRspOrderLocalInput(t.spi, C.exOnRspOrderLocalInput)
	C.SetOnRspOrderLocalModify(t.spi, C.exOnRspOrderLocalModify)
	C.SetOnRspOrderLocalTransfer(t.spi, C.exOnRspOrderLocalTransfer)
	C.SetOnRspFillLocalInput(t.spi, C.exOnRspFillLocalInput)
	C.SetOnRspFillLocalRemove(t.spi, C.exOnRspFillLocalRemove)
	C.SetOnRspQrySpotLock(t.spi, C.exOnRspQrySpotLock)
	C.SetOnRtnSpotLock(t.spi, C.exOnRtnSpotLock)
	C.SetOnRspSubmitUserLoginInfo(t.spi, C.exOnRspSubmitUserLoginInfo)
	C.SetOnRspSpecialOrderAction(t.spi, C.exOnRspSpecialOrderAction)
	C.SetOnRtnSpecialOrder(t.spi, C.exOnRtnSpecialOrder)
	C.SetOnRspQrySpecialOrder(t.spi, C.exOnRspQrySpecialOrder)
	C.SetOnRspQryCombinePosition(t.spi, C.exOnRspQryCombinePosition)
	C.SetOnRtnCombinePosition(t.spi, C.exOnRtnCombinePosition)

}

//export exOnConnect
func exOnConnect(HostAddress *C.char) {
	if t.OnConnect != nil {
		t.OnConnect(GBK(HostAddress))
	} else {
		fmt.Println("OnConnect")
	}
}

//export exOnRspLogin
func exOnRspLogin(errorCode C.int, loginRspInfo *C.struct_TapAPITradeLoginRspInfo) {
	if t.OnRspLogin != nil {
		t.OnRspLogin(FromCint32(errorCode), (*TapAPITradeLoginRspInfo)(unsafe.Pointer(loginRspInfo)))
	} else {
		fmt.Println("OnRspLogin")
	}
}

//export exOnRtnContactInfo
func exOnRtnContactInfo(errorCode C.int, isLast C.char, ContactInfo *C.char) {
	if t.OnRtnContactInfo != nil {
		t.OnRtnContactInfo(FromCint32(errorCode), TAPIYNFLAG(isLast), GBK(ContactInfo))
	} else {
		fmt.Println("OnRtnContactInfo")
	}
}

//export exOnRspRequestVertificateCode
func exOnRspRequestVertificateCode(sessionID C.uint, errorCode C.int, rsp *C.struct_TapAPIRequestVertificateCodeRsp) {
	if t.OnRspRequestVertificateCode != nil {
		t.OnRspRequestVertificateCode(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIRequestVertificateCodeRsp)(unsafe.Pointer(rsp)))
	} else {
		fmt.Println("OnRspRequestVertificateCode")
	}
}

//export exOnExpriationDate
func exOnExpriationDate(date *C.char, days C.int) {
	if t.OnExpriationDate != nil {
		t.OnExpriationDate(GBK(date), int(days))
	} else {
		fmt.Println("OnExpriationDate")
	}
}

//export exOnAPIReady
func exOnAPIReady(errorCode C.int) {
	if t.OnAPIReady != nil {
		t.OnAPIReady(FromCint32(errorCode))
	} else {
		fmt.Println("OnAPIReady")
	}
}

//export exOnDisconnect
func exOnDisconnect(reasonCode C.int) {
	if t.OnDisconnect != nil {
		t.OnDisconnect(FromCint32(reasonCode))
	} else {
		fmt.Println("OnDisconnect")
	}
}

//export exOnRspChangePassword
func exOnRspChangePassword(sessionID C.uint, errorCode C.int) {
	if t.OnRspChangePassword != nil {
		t.OnRspChangePassword(FromCuint32(sessionID), FromCint32(errorCode))
	} else {
		fmt.Println("OnRspChangePassword")
	}
}

//export exOnRspAuthPassword
func exOnRspAuthPassword(sessionID C.uint, errorCode C.int) {
	if t.OnRspAuthPassword != nil {
		t.OnRspAuthPassword(FromCuint32(sessionID), FromCint32(errorCode))
	} else {
		fmt.Println("OnRspAuthPassword")
	}
}

//export exOnRspQryTradingDate
func exOnRspQryTradingDate(sessionID C.uint, errorCode C.int, info *C.struct_TapAPITradingCalendarQryRsp) {
	if t.OnRspQryTradingDate != nil {
		t.OnRspQryTradingDate(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPITradingCalendarQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryTradingDate")
	}
}

//export exOnRspSetReservedInfo
func exOnRspSetReservedInfo(sessionID C.uint, errorCode C.int, info *C.char) {
	if t.OnRspSetReservedInfo != nil {
		t.OnRspSetReservedInfo(FromCuint32(sessionID), FromCint32(errorCode), GBK(info))
	} else {
		fmt.Println("OnRspSetReservedInfo")
	}
}

//export exOnRspQryAccount
func exOnRspQryAccount(sessionID C.uint, errorCode C.uint, isLast C.char, info *C.struct_TapAPIAccountInfo) {
	if t.OnRspQryAccount != nil {
		t.OnRspQryAccount(FromCuint32(sessionID), FromUint32(uint32(errorCode)), TAPIYNFLAG(isLast), (*TapAPIAccountInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryAccount")
	}
}

//export exOnRspQryFund
func exOnRspQryFund(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIFundData) {
	if t.OnRspQryFund != nil {
		t.OnRspQryFund(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIFundData)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryFund")
	}
}

//export exOnRtnFund
func exOnRtnFund(info *C.struct_TapAPIFundData) {
	if t.OnRtnFund != nil {
		t.OnRtnFund((*TapAPIFundData)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnFund")
	}
}

//export exOnRspQryExchange
func exOnRspQryExchange(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIExchangeInfo) {
	if t.OnRspQryExchange != nil {
		t.OnRspQryExchange(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIExchangeInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryExchange")
	}
}

//export exOnRspQryCommodity
func exOnRspQryCommodity(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPICommodityInfo) {
	if t.OnRspQryCommodity != nil {
		t.OnRspQryCommodity(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPICommodityInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryCommodity")
	}
}

//export exOnRspQryContract
func exOnRspQryContract(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPITradeContractInfo) {
	if t.OnRspQryContract != nil {
		t.OnRspQryContract(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPITradeContractInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryContract")
	}
}

//export exOnRtnContract
func exOnRtnContract(info *C.struct_TapAPITradeContractInfo) {
	if t.OnRtnContract != nil {
		t.OnRtnContract((*TapAPITradeContractInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnContract")
	}
}

//export exOnRspOrderAction
func exOnRspOrderAction(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderActionRsp) {
	if t.OnRspOrderAction != nil {
		t.OnRspOrderAction(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderActionRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspOrderAction")
	}
}

//export exOnRtnOrder
func exOnRtnOrder(info *C.struct_TapAPIOrderInfoNotice) {
	if t.OnRtnOrder != nil {
		t.OnRtnOrder((*TapAPIOrderInfoNotice)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnOrder")
	}
}

//export exOnRspQryOrder
func exOnRspQryOrder(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIOrderInfo) {
	if t.OnRspQryOrder != nil {
		t.OnRspQryOrder(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIOrderInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryOrder")
	}
}

//export exOnRspQryOrderProcess
func exOnRspQryOrderProcess(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIOrderInfo) {
	if t.OnRspQryOrderProcess != nil {
		t.OnRspQryOrderProcess(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIOrderInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryOrderProcess")
	}
}

//export exOnRspQryFill
func exOnRspQryFill(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIFillInfo) {
	if t.OnRspQryFill != nil {
		t.OnRspQryFill(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIFillInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryFill")
	}
}

//export exOnRtnFill
func exOnRtnFill(info *C.struct_TapAPIFillInfo) {
	if t.OnRtnFill != nil {
		t.OnRtnFill((*TapAPIFillInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnFill")
	}
}

//export exOnRspQryPosition
func exOnRspQryPosition(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIPositionInfo) {
	if t.OnRspQryPosition != nil {
		t.OnRspQryPosition(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIPositionInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryPosition")
	}
}

//export exOnRtnPosition
func exOnRtnPosition(info *C.struct_TapAPIPositionInfo) {
	if t.OnRtnPosition != nil {
		t.OnRtnPosition((*TapAPIPositionInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnPosition")
	}
}

//export exOnRspQryPositionSummary
func exOnRspQryPositionSummary(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIPositionSummary) {
	if t.OnRspQryPositionSummary != nil {
		t.OnRspQryPositionSummary(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIPositionSummary)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryPositionSummary")
	}
}

//export exOnRtnPositionSummary
func exOnRtnPositionSummary(info *C.struct_TapAPIPositionSummary) {
	if t.OnRtnPositionSummary != nil {
		t.OnRtnPositionSummary((*TapAPIPositionSummary)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnPositionSummary")
	}
}

//export exOnRtnPositionProfit
func exOnRtnPositionProfit(info *C.struct_TapAPIPositionProfitNotice) {
	if t.OnRtnPositionProfit != nil {
		t.OnRtnPositionProfit((*TapAPIPositionProfitNotice)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnPositionProfit")
	}
}

//export exOnRspQryCurrency
func exOnRspQryCurrency(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPICurrencyInfo) {
	if t.OnRspQryCurrency != nil {
		t.OnRspQryCurrency(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPICurrencyInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryCurrency")
	}
}

//export exOnRspQryTradeMessage
func exOnRspQryTradeMessage(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPITradeMessage) {
	if t.OnRspQryTradeMessage != nil {
		t.OnRspQryTradeMessage(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPITradeMessage)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryTradeMessage")
	}
}

//export exOnRtnTradeMessage
func exOnRtnTradeMessage(info *C.struct_TapAPITradeMessage) {
	if t.OnRtnTradeMessage != nil {
		t.OnRtnTradeMessage((*TapAPITradeMessage)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnTradeMessage")
	}
}

//export exOnRspQryHisOrder
func exOnRspQryHisOrder(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIHisOrderQryRsp) {
	if t.OnRspQryHisOrder != nil {
		t.OnRspQryHisOrder(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIHisOrderQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryHisOrder")
	}
}

//export exOnRspQryHisOrderProcess
func exOnRspQryHisOrderProcess(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIHisOrderProcessQryReq) {
	if t.OnRspQryHisOrderProcess != nil {
		t.OnRspQryHisOrderProcess(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIHisOrderProcessQryReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryHisOrderProcess")
	}
}

//export exOnRspQryHisMatch
func exOnRspQryHisMatch(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIHisMatchQryRsp) {
	if t.OnRspQryHisMatch != nil {
		t.OnRspQryHisMatch(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIHisMatchQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryHisMatch")
	}
}

//export exOnRspQryHisPosition
func exOnRspQryHisPosition(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIHisPositionQryRsp) {
	if t.OnRspQryHisPosition != nil {
		t.OnRspQryHisPosition(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIHisPositionQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryHisPosition")
	}
}

//export exOnRspQryHisDelivery
func exOnRspQryHisDelivery(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIHisDeliveryQryRsp) {
	if t.OnRspQryHisDelivery != nil {
		t.OnRspQryHisDelivery(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIHisDeliveryQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryHisDelivery")
	}
}

//export exOnRspQryAccountCashAdjust
func exOnRspQryAccountCashAdjust(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIAccountCashAdjustQryRsp) {
	if t.OnRspQryAccountCashAdjust != nil {
		t.OnRspQryAccountCashAdjust(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIAccountCashAdjustQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryAccountCashAdjust")
	}
}

//export exOnRspQryBill
func exOnRspQryBill(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIBillQryRsp) {
	if t.OnRspQryBill != nil {
		t.OnRspQryBill(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIBillQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryBill")
	}
}

//export exOnRspQryAccountFeeRent
func exOnRspQryAccountFeeRent(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIAccountFeeRentQryRsp) {
	if t.OnRspQryAccountFeeRent != nil {
		t.OnRspQryAccountFeeRent(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIAccountFeeRentQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryAccountFeeRent")
	}
}

//export exOnRspQryAccountMarginRent
func exOnRspQryAccountMarginRent(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPIAccountMarginRentQryRsp) {
	if t.OnRspQryAccountMarginRent != nil {
		t.OnRspQryAccountMarginRent(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPIAccountMarginRentQryRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryAccountMarginRent")
	}
}

//export exOnRspHKMarketOrderInsert
func exOnRspHKMarketOrderInsert(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderMarketInsertReq) {
	if t.OnRspHKMarketOrderInsert != nil {
		t.OnRspHKMarketOrderInsert(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderMarketInsertReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspHKMarketOrderInsert")
	}
}

//export exOnRspHKMarketOrderDelete
func exOnRspHKMarketOrderDelete(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderMarketDeleteReq) {
	if t.OnRspHKMarketOrderDelete != nil {
		t.OnRspHKMarketOrderDelete(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderMarketDeleteReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspHKMarketOrderDelete")
	}
}

//export exOnHKMarketQuoteNotice
func exOnHKMarketQuoteNotice(info *C.struct_TapAPIOrderQuoteMarketNotice) {
	if t.OnHKMarketQuoteNotice != nil {
		t.OnHKMarketQuoteNotice((*TapAPIOrderQuoteMarketNotice)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnHKMarketQuoteNotice")
	}
}

//export exOnRspOrderLocalRemove
func exOnRspOrderLocalRemove(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderLocalRemoveRsp) {
	if t.OnRspOrderLocalRemove != nil {
		t.OnRspOrderLocalRemove(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderLocalRemoveRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspOrderLocalRemove")
	}
}

//export exOnRspOrderLocalInput
func exOnRspOrderLocalInput(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderLocalInputReq) {
	if t.OnRspOrderLocalInput != nil {
		t.OnRspOrderLocalInput(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderLocalInputReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspOrderLocalInput")
	}
}

//export exOnRspOrderLocalModify
func exOnRspOrderLocalModify(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderLocalModifyReq) {
	if t.OnRspOrderLocalModify != nil {
		t.OnRspOrderLocalModify(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderLocalModifyReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspOrderLocalModify")
	}
}

//export exOnRspOrderLocalTransfer
func exOnRspOrderLocalTransfer(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIOrderLocalTransferReq) {
	if t.OnRspOrderLocalTransfer != nil {
		t.OnRspOrderLocalTransfer(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIOrderLocalTransferReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspOrderLocalTransfer")
	}
}

//export exOnRspFillLocalInput
func exOnRspFillLocalInput(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIFillLocalInputReq) {
	if t.OnRspFillLocalInput != nil {
		t.OnRspFillLocalInput(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIFillLocalInputReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspFillLocalInput")
	}
}

//export exOnRspFillLocalRemove
func exOnRspFillLocalRemove(sessionID C.uint, errorCode C.int, info *C.struct_TapAPIFillLocalRemoveReq) {
	if t.OnRspFillLocalRemove != nil {
		t.OnRspFillLocalRemove(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPIFillLocalRemoveReq)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspFillLocalRemove")
	}
}

//export exOnRspQrySpotLock
func exOnRspQrySpotLock(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPISpotLockDataRsp) {
	if t.OnRspQrySpotLock != nil {
		t.OnRspQrySpotLock(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPISpotLockDataRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQrySpotLock")
	}
}

//export exOnRtnSpotLock
func exOnRtnSpotLock(info *C.struct_TapAPISpotLockDataRsp) {
	if t.OnRtnSpotLock != nil {
		t.OnRtnSpotLock((*TapAPISpotLockDataRsp)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnSpotLock")
	}
}

//export exOnRspSubmitUserLoginInfo
func exOnRspSubmitUserLoginInfo(sessionID C.uint, errorCode C.int, isLast C.char) {
	if t.OnRspSubmitUserLoginInfo != nil {
		t.OnRspSubmitUserLoginInfo(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast))
	} else {
		fmt.Println("OnRspSubmitUserLoginInfo")
	}
}

//export exOnRspSpecialOrderAction
func exOnRspSpecialOrderAction(sessionID C.uint, errorCode C.int, info *C.struct_TapAPISpecialOrderInfo) {
	if t.OnRspSpecialOrderAction != nil {
		t.OnRspSpecialOrderAction(FromCuint32(sessionID), FromCint32(errorCode), (*TapAPISpecialOrderInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspSpecialOrderAction")
	}
}

//export exOnRtnSpecialOrder
func exOnRtnSpecialOrder(info *C.struct_TapAPISpecialOrderInfo) {
	if t.OnRtnSpecialOrder != nil {
		t.OnRtnSpecialOrder((*TapAPISpecialOrderInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnSpecialOrder")
	}
}

//export exOnRspQrySpecialOrder
func exOnRspQrySpecialOrder(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPISpecialOrderInfo) {
	if t.OnRspQrySpecialOrder != nil {
		t.OnRspQrySpecialOrder(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPISpecialOrderInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQrySpecialOrder")
	}
}

//export exOnRspQryCombinePosition
func exOnRspQryCombinePosition(sessionID C.uint, errorCode C.int, isLast C.char, info *C.struct_TapAPICombinePositionInfo) {
	if t.OnRspQryCombinePosition != nil {
		t.OnRspQryCombinePosition(FromCuint32(sessionID), FromCint32(errorCode), TAPIYNFLAG(isLast), (*TapAPICombinePositionInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRspQryCombinePosition")
	}
}

//export exOnRtnCombinePosition
func exOnRtnCombinePosition(info *C.struct_TapAPICombinePositionInfo) {
	if t.OnRtnCombinePosition != nil {
		t.OnRtnCombinePosition((*TapAPICombinePositionInfo)(unsafe.Pointer(info)))
	} else {
		fmt.Println("OnRtnCombinePosition")
	}
}
