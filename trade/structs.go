package trade

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
)

type TapAPIApplicationInfo struct {
	// < 授权码
	AuthCode TAPIAUTHCODE
	// < 关键操作日志路径 APIRun日志
	KeyOperationLogPath TAPISTR_300
	// < 日志级别
	LogLevel TAPILOGLEVEL
	// < 看穿式监管使用,北斗星系统不使用该字段
	APPID TAPISTR_30
	// < 重连次数
	ReConnectCount TAPIINT32
	// < 重连时间间隔(秒)
	ReConnectSeconds TAPIINT32
}

type TapAPICommodity struct {
	// < 交易所编码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编号
	CommodityNo TAPISTR_10
}

type TapAPIContract struct {
	// < 品种
	Commodity TapAPICommodity
	// < 合约代码1
	ContractNo1 TAPISTR_10
	// < 执行价1
	StrikePrice1 TAPISTR_10
	// < 看涨看跌标示1
	CallOrPutFlag1 TAPICallOrPutFlagType
	// < 合约代码2
	ContractNo2 TAPISTR_10
	// < 执行价2
	StrikePrice2 TAPISTR_10
	// < 看涨看跌标示2
	CallOrPutFlag2 TAPICallOrPutFlagType
}

type TapAPIExchangeInfo struct {
	// < 交易所编码
	ExchangeNo TAPISTR_10
	// < 交易所名称
	ExchangeName TAPISTR_20
}

type TapAPIChangePasswordReq struct {
	// TAPIPasswordType PasswordType;  ///< 密码类型
	AccountNo TAPISTR_20
	// < 旧密码
	OldPassword TAPISTR_20
	// < 新密码
	NewPassword TAPISTR_20
}

type TapAPIAuthPasswordReq struct {
	// < 客户账号
	AccountNo TAPISTR_20
	// < 密码类型
	PasswordType TAPIPasswordType
	// < 账户密码
	Password TAPISTR_20
}

type TapAPITradeLoginAuth struct {
	// < 用户名
	UserNo TAPISTR_20
	// < 是否修改密码
	ISModifyPassword TAPIYNFLAG
	// < 密码
	Password TAPISTR_20
	// < 新密码
	NewPassword TAPISTR_20
	// < 登录IP（使用此字段需易盛授权）
	LoginIP TAPISTR_40
	// < 登录MAC,格式12-34-56-78-90-11（使用此字段需易盛授权）
	LoginMac TAPISTR_50
	// < 登录设备名称（使用此字段需易盛授权）
	DeviceName TAPISTR_50
}

type TapAPITradeLoginRspInfo struct {
	// < 用户编号
	UserNo TAPISTR_20
	// < 用户类型
	UserType TAPIUserTypeType
	// < 用户名
	UserName TAPISTR_20
	// < 交易中心和后台版本号
	ReservedInfo TAPISTR_50
	// < 上次登录IP
	LastLoginIP TAPISTR_40
	// < 上次登录端口
	LastLoginProt TAPIUINT32
	// < 上次登录时间
	LastLoginTime TAPIDATETIME
	// < 上次退出时间
	LastLogoutTime TAPIDATETIME
	// < 当前交易日期
	TradeDate TAPIDATE
	// < 上次结算时间
	LastSettleTime TAPIDATETIME
	// < 系统启动时间
	StartTime TAPIDATETIME
	// < 下次二次认证日期
	NextSecondDate TAPIDATETIME
	// < 登录附加信息
	LastLoginInfo TAPISTR_300
}

type TapAPIRequestVertificateCodeRsp struct {
	// < 二次认证授权码序号
	SecondSerialID TAPISecondSerialIDType
	// < 二次认证授权码有效期（分）。
	Effective TAPIINT32
}

type TapAPISecondCertificationReq struct {
	// < 二次认证码
	VertificateCode TAPISTR_10
	// < 二次认证登录类型
	LoginType TAPILoginTypeType
}

type TapAPIAccQryReq struct {
}

type TapAPIAccountInfo struct {
	// < 资金账号
	AccountNo TAPISTR_20
	// < 账号类型
	AccountType TAPIAccountType
	// < 账号状态
	AccountState TAPIAccountState
	// < 交易状态
	AccountTradeRight TAPIAccountRightType
	// < 可交易品种组.
	CommodityGroupNo TAPISTR_10
	// < 账号简称
	AccountShortName TAPISTR_20
	// < 账号英文简称
	AccountEnShortName TAPISTR_20
}

type TapAPINewOrder struct {
	// < 客户资金帐号，必填
	AccountNo TAPISTR_20
	// < 交易所编号，必填
	ExchangeNo TAPISTR_10
	// < 品种类型，必填
	CommodityType TAPICommodityType
	// < 品种编码类型，必填
	CommodityNo TAPISTR_10
	// < 合约1，必填
	ContractNo TAPISTR_10
	// < 执行价格1，期权填写
	StrikePrice TAPISTR_10
	// < 看张看跌1 默认N
	CallOrPutFlag TAPICallOrPutFlagType
	// < 合约2，默认空
	ContractNo2 TAPISTR_10
	// < 执行价格2，默认空
	StrikePrice2 TAPISTR_10
	// < 看张看跌2 默认N
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 委托类型 必填
	OrderType TAPIOrderTypeType
	// < 委托来源，默认程序单。
	OrderSource TAPIOrderSourceType
	// < 委托有效类型,默认当日有效
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 是否风险报单，默认非风险保单
	IsRiskOrder TAPIYNFLAG
	// < 买入卖出
	OrderSide TAPISideType
	// < 开平标志1,默认N
	PositionEffect TAPIPositionEffectType
	// < 开平标志2，默认N
	PositionEffect2 TAPIPositionEffectType
	// < 询价号
	InquiryNo TAPISTR_50
	// < 投机保值，默认N
	HedgeFlag TAPIHedgeFlagType
	// < 委托价格1
	OrderPrice TAPIREAL64
	// < 委托价格2，做市商应价使用
	OrderPrice2 TAPIREAL64
	// < 触发价格
	StopPrice TAPIREAL64
	// < 委托数量，必填
	OrderQty TAPIUINT32
	// < 最小成交量，默认1
	OrderMinQty TAPIUINT32
	// < 冰山单最小随机量
	MinClipSize TAPIUINT32
	// < 冰山单最大随机量
	MaxClipSize TAPIUINT32
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考值
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 客户子账号，如果存在子账号，则自行上报子账号
	ClientID TAPIClientIDType
	// < 策略单类型，默认N
	TacticsType TAPITacticsTypeType
	// < 触发条件，默认N
	TriggerCondition TAPITriggerConditionType
	// < 触发价格类型，默认N
	TriggerPriceType TAPITriggerPriceTypeType
	// < 是否T+1有效,默认T+1有效。
	AddOneIsValid TAPIYNFLAG
	// < 下单人区域
	ClientLocationID TAPIClientLocationIDType
}

type TapAPIOrderInfo struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格1
	StrikePrice TAPISTR_10
	// < 看张看跌1
	CallOrPutFlag TAPICallOrPutFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 委托有效类型
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 是否风险报单
	IsRiskOrder TAPIYNFLAG
	// < 买入卖出
	OrderSide TAPISideType
	// < 开平标志1
	PositionEffect TAPIPositionEffectType
	// < 开平标志2
	PositionEffect2 TAPIPositionEffectType
	// < 询价号
	InquiryNo TAPISTR_50
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 委托价格1
	OrderPrice TAPIREAL64
	// < 委托价格2，做市商应价使用
	OrderPrice2 TAPIREAL64
	// < 触发价格
	StopPrice TAPIREAL64
	// < 委托数量
	OrderQty TAPIUINT32
	// < 最小成交量
	OrderMinQty TAPIUINT32
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考值
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 冰山单最小随机量
	MinClipSize TAPIUINT32
	// < 冰山单最大随机量
	MaxClipSize TAPIUINT32
	// < 软件授权号
	LicenseNo TAPISTR_50
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
	// < 客户端本地委托编号
	ClientOrderNo TAPISTR_50
	// < 客户子账号
	ClientID TAPIClientIDType
	// < 策略单类型
	TacticsType TAPITacticsTypeType
	// < 触发条件
	TriggerCondition TAPITriggerConditionType
	// < 触发价格类型
	TriggerPriceType TAPITriggerPriceTypeType
	// < 是否T+1有效
	AddOneIsValid TAPIYNFLAG
	// < 终端本地IP
	ClientLocalIP TAPISTR_40
	// < 终端本地Mac地址
	ClientMac TAPIMACTYPE
	// < 终端网络地址.
	ClientIP TAPISTR_40
	// < 委托流水号
	OrderStreamID TAPIUINT32
	// < 上手号
	UpperNo TAPISTR_10
	// < 上手通道号
	UpperChannelNo TAPISTR_10
	// < 本地号
	OrderLocalNo TAPISTR_20
	// < 上手流号
	UpperStreamID TAPIUINT32
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 交易所系统号
	OrderExchangeSystemNo TAPISTR_50
	// < 父单系统号
	OrderParentSystemNo TAPISTR_50
	// < 下单人
	OrderInsertUserNo TAPISTR_20
	// < 下单时间
	OrderInsertTime TAPIDATETIME
	// < 录单操作人
	OrderCommandUserNo TAPISTR_20
	// < 委托更新人
	OrderUpdateUserNo TAPISTR_20
	// < 委托更新时间
	OrderUpdateTime TAPIDATETIME
	// < 委托状态
	OrderState TAPIOrderStateType
	// < 成交价1
	OrderMatchPrice TAPIREAL64
	// < 成交价2
	OrderMatchPrice2 TAPIREAL64
	// < 成交量1
	OrderMatchQty TAPIUINT32
	// < 成交量2
	OrderMatchQty2 TAPIUINT32
	// < 最后一次操作错误信息码
	ErrorCode TAPIUINT32
	// < 错误信息
	ErrorText TAPISTR_50
	// < 是否为录入委托单
	IsBackInput TAPIYNFLAG
	// < 委托成交删除标
	IsDeleted TAPIYNFLAG
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 下单人区域
	ClientLocationID TAPIClientLocationIDType
}

type TapAPIOrderInfoNotice struct {
	// < 会话ID
	SessionID TAPIUINT32
	// < 错误码
	ErrorCode TAPIUINT32
}

type TapAPIOrderActionRsp struct {
	// < 操作类型
	ActionType TAPIORDERACT
}

type TapAPIAmendOrder struct {
	// < 报单请求数据
	ReqData TapAPINewOrder
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编号
	OrderNo TAPISTR_20
}

type TapAPIOrderCancelReq struct {
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考值
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
}

type TapAPIOrderQryReq struct {
	// < 资金账号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 委托有效类型
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 是否风险报单
	IsRiskOrder TAPIYNFLAG
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编号
	OrderNo TAPISTR_20
	// < 是否为录入委托单
	IsBackInput TAPIYNFLAG
	// < 委托成交删除标
	IsDeleted TAPIYNFLAG
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 是否只查询未完成委托
	OrderQryType TAPIOrderQryTypeType
}

type TapAPIOrderProcessQryReq struct {
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
}

type TapAPIFillQryReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 委托来源
	MatchSource TAPIMatchSourceType
	// < 买入卖出
	MatchSide TAPISideType
	// < 开平标志1
	PositionEffect TAPIPositionEffectType
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
	// < 上手号
	UpperNo TAPISTR_10
	// < 委托成交删除标
	IsDeleted TAPIYNFLAG
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
}

type TapAPIFillInfo struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 委托来源
	MatchSource TAPIMatchSourceType
	// < 买入卖出
	MatchSide TAPISideType
	// < 开平标志1
	PositionEffect TAPIPositionEffectType
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 本地成交号
	MatchNo TAPISTR_20
	// < 上手成交号
	UpperMatchNo TAPISTR_70
	// < 交易所成交号
	ExchangeMatchNo TAPISTR_70
	// < 成交时间
	MatchDateTime TAPIDATETIME
	// < 上手成交时间
	UpperMatchDateTime TAPIDATETIME
	// < 上手号
	UpperNo TAPISTR_10
	// < 成交价
	MatchPrice TAPIREAL64
	// < 成交量
	MatchQty TAPIUINT32
	// < 委托成交删除标
	IsDeleted TAPIYNFLAG
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 客户手续费币种组
	FeeCurrencyGroup TAPISTR_10
	// < 客户手续费币种
	FeeCurrency TAPISTR_10
	// < 手续费
	FeeValue TAPIREAL64
	// < 人工客户手续费标记
	IsManualFee TAPIYNFLAG
	// < 指定价格平仓
	ClosePrositionPrice TAPIREAL64
	// < 平仓盈亏
	CloseProfit TAPIREAL64
}

type TapAPICloseQryReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
}

type TapAPICloseInfo struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 平仓一边的买入卖出
	CloseSide TAPISideType
	// < 平仓成交量
	CloseQty TAPIUINT32
	// < 开仓成交价
	OpenPrice TAPIREAL64
	// < 平仓成交价
	ClosePrice TAPIREAL64
	// < 本地成交号
	OpenMatchNo TAPISTR_20
	// < 成交时间
	OpenMatchDateTime TAPIDATETIME
	// < 本地成交号
	CloseMatchNo TAPISTR_20
	// < 成交时间
	CloseMatchDateTime TAPIDATETIME
	// < 平仓流号
	CloseStreamId TAPIUINT32
	// < 品种币种组
	CommodityCurrencyGroup TAPISTR_10
	// < 品种币种
	CommodityCurrency TAPISTR_10
	// < 平仓盈亏
	CloseProfit TAPIREAL64
}

type TapAPIPositionQryReq struct {
	//
	AccountNo TAPISTR_20
}

type TapAPIPositionInfo struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买入卖出
	MatchSide TAPISideType
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 本地持仓号，服务器编写
	PositionNo TAPISTR_70
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
	// < 本地成交号
	MatchNo TAPISTR_20
	// < 上手号
	UpperNo TAPISTR_10
	// < 持仓价
	PositionPrice TAPIREAL64
	// < 持仓量
	PositionQty TAPIUINT32
	// < 持仓流号
	PositionStreamId TAPIUINT32
	// < 品种币种组
	CommodityCurrencyGroup TAPISTR_10
	// < 品种币种
	CommodityCurrency TAPISTR_10
	// < 当前计算价格
	CalculatePrice TAPIREAL64
	// < 客户初始保证金
	AccountInitialMargin TAPIREAL64
	// < 客户维持保证金
	AccountMaintenanceMargin TAPIREAL64
	// < 上手初始保证金
	UpperInitialMargin TAPIREAL64
	// < 上手维持保证金
	UpperMaintenanceMargin TAPIREAL64
	// < 持仓盈亏
	PositionProfit TAPIREAL64
	// < LME持仓盈亏
	LMEPositionProfit TAPIREAL64
	// < 期权市值
	OptionMarketValue TAPIREAL64
	// < 是否为昨仓。
	IsHistory TAPIYNFLAG
}

type TapAPIPositionProfit struct {
	// < 本地持仓号，服务器编写
	PositionNo TAPISTR_70
	// < 持仓流号
	PositionStreamId TAPIUINT32
	// < 持仓盈亏
	PositionProfit TAPIREAL64
	// < LME持仓盈亏
	LMEPositionProfit TAPIREAL64
	// < 期权市值
	OptionMarketValue TAPIREAL64
	// < 计算价格
	CalculatePrice TAPIREAL64
}

type TapAPIPositionProfitNotice struct {
	// < 是否最后一包
	IsLast TAPIYNFLAG
}

type TapAPIPositionSummary struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买入卖出
	MatchSide TAPISideType
	// < 持仓均价。
	PositionPrice TAPIREAL64
	// < 持仓量
	PositionQty TAPIUINT32
	// < 历史持仓量
	HisPositionQty TAPIUINT32
}

// fmt.Sprintf("%s|%c|%s|%s", p.ExchangeNo.String(), p.CommodityType, p.CommodityNo.String(), p.ContractNo.String())
func (p *TapAPIPositionSummary) Key() string {
	return fmt.Sprintf("%s|%c|%s|%s", p.ExchangeNo.String(), p.CommodityType, p.CommodityNo.String(), p.ContractNo.String())
}

func (p *TapAPIPositionSummary) ToBuf() []byte {
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.LittleEndian, p)
	return buf.Bytes()
}

func (p *TapAPIPositionSummary) Json() string {
	mp := p.Map()
	bs, _ := json.Marshal(mp)
	return string(bs)
}

func (p *TapAPIPositionSummary) FromJSON(str string) {
	mp := make(map[string]any, 0)
	json.Unmarshal([]byte(str), &mp)
	copy(p.AccountNo[:], mp["AccountNo"].(string))
	copy(p.ExchangeNo[:], mp["ExchangeNo"].(string))
	copy(p.CommodityNo[:], mp["CommodityNo"].(string))
	copy(p.ContractNo[:], mp["ContractNo"].(string))
	copy(p.StrikePrice[:], mp["StrikePrice"].(string))
	p.PositionPrice = FromFloat64(mp["PositionPrice"].(float64))
	p.PositionQty = FromUint32(uint32(mp["PositionQty"].(float64)))
	p.HisPositionQty = FromUint32(uint32(mp["HisPositionQty"].(float64)))

	p.CommodityType = TAPICommodityType(rune(mp["CommodityType"].(float64)))
	p.CallOrPutFlag = TAPICallOrPutFlagType(rune(mp["CallOrPutFlag"].(float64)))
	p.MatchSide = TAPISideType(rune(mp["MatchSide"].(float64)))
}

func (p *TapAPIPositionSummary) Map() map[string]any {
	return map[string]any{
		// < 客户资金帐号
		"AccountNo": p.AccountNo.String(),
		// 合约名称
		"InstrumentID": p.Key(),
		// < 交易所编号
		"ExchangeNo": p.ExchangeNo.String(),
		// < 品种类型
		"CommodityType": p.CommodityType,
		// < 品种编码类型
		"CommodityNo": p.CommodityNo.String(),
		// < 合约1
		"ContractNo": p.ContractNo.String(),
		// < 执行价格
		"StrikePrice": p.StrikePrice.String(),
		// < 看张看跌
		"CallOrPutFlag": p.CallOrPutFlag,
		// < 买入卖出
		"MatchSide": p.MatchSide,
		// < 持仓均价。
		"PositionPrice": p.PositionPrice.Float64(),
		// < 持仓量
		"PositionQty": p.PositionQty.Uint32(),
		// < 历史持仓量
		"HisPositionQty": p.HisPositionQty.Uint32(),
	}
}

type TapAPIFundReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
}

type TapAPIFundData struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 币种组号
	CurrencyGroupNo TAPISTR_10
	// < 币种号(为空表示币种组基币资金)
	CurrencyNo TAPISTR_10
	// < 交易汇率
	TradeRate TAPIREAL64
	// < 期货算法
	FutureAlg TAPIFutureAlgType
	// < 期权算法
	OptionAlg TAPIOptionAlgType
	// < 上日结存
	PreBalance TAPIREAL64
	// < 上日未到期平盈
	PreUnExpProfit TAPIREAL64
	// < 上日LME持仓平盈
	PreLMEPositionProfit TAPIREAL64
	// < 上日权益
	PreEquity TAPIREAL64
	// < 上日可用
	PreAvailable1 TAPIREAL64
	// < 上日市值权益
	PreMarketEquity TAPIREAL64
	// < 入金
	CashInValue TAPIREAL64
	// < 出金
	CashOutValue TAPIREAL64
	// < 资金调整
	CashAdjustValue TAPIREAL64
	// < 质押资金
	CashPledged TAPIREAL64
	// < 冻结手续费
	FrozenFee TAPIREAL64
	// < 冻结保证金
	FrozenDeposit TAPIREAL64
	// < 客户手续费包含交割手续费
	AccountFee TAPIREAL64
	// < 汇入资金
	SwapInValue TAPIREAL64
	// < 汇出资金
	SwapOutValue TAPIREAL64
	// < 权利金收取
	PremiumIncome TAPIREAL64
	// < 权利金支付
	PremiumPay TAPIREAL64
	// < 平仓盈亏
	CloseProfit TAPIREAL64
	// < 冻结资金
	FrozenFund TAPIREAL64
	// < 未到期平盈
	UnExpProfit TAPIREAL64
	// < 到期平仓盈亏
	ExpProfit TAPIREAL64
	// < 不含LME持仓盈亏
	PositionProfit TAPIREAL64
	// < LME持仓盈亏
	LmePositionProfit TAPIREAL64
	// < 期权市值
	OptionMarketValue TAPIREAL64
	// < 客户初始保证金
	AccountIntialMargin TAPIREAL64
	// < 客户维持保证金
	AccountMaintenanceMargin TAPIREAL64
	// < 上手初始保证金
	UpperInitalMargin TAPIREAL64
	// < 上手维持保证金
	UpperMaintenanceMargin TAPIREAL64
	// < LME贴现
	Discount TAPIREAL64
	// < 当日结存
	Balance TAPIREAL64
	// < 当日权益
	Equity TAPIREAL64
	// < 当日可用
	Available TAPIREAL64
	// < 可提取
	CanDraw TAPIREAL64
	// < 账户市值
	MarketEquity TAPIREAL64
	// < 授信资金
	AuthMoney TAPIREAL64
}

type TapAPICommodityInfo struct {
	//  交易所编码
	ExchangeNo TAPISTR_10
	//  品种类型
	CommodityType TAPICommodityType
	//  品种编号
	CommodityNo TAPISTR_10
	//  品种名称
	CommodityName TAPISTR_20
	//  品种英文名称
	CommodityEngName TAPISTR_30
	// TAPICommodityType RelateCommodityType;
	RelateExchangeNo TAPISTR_10
	// TAPISTR_10 RelateExchangeNo2;
	RelateCommodityNo TAPISTR_10
	// TAPISTR_10 RelateCommodityNo2;
	RelateCommodityType2 TAPICommodityType
	// TAPISTR_10 TradeCurrency;             // 交易币种
	CurrencyGroupNo TAPISTR_10
	//  每手乘数
	ContractSize TAPIREAL64
	//  开平方式
	OpenCloseMode TAPIOpenCloseModeType
	//  执行价格倍数
	StrikePriceTimes TAPIREAL64
	//  最小变动价位
	CommodityTickSize TAPIREAL64
	//  报价分母
	CommodityDenominator TAPIINT32
	//  组合方向
	CmbDirect TAPICmbDirectType
	//  交割行权方式
	DeliveryMode TAPIDeliveryModeType
	//  交割日偏移
	DeliveryDays TAPIINT32
	//  T+1分割时间
	AddOneTime TAPITIME
	//  品种时区
	CommodityTimeZone TAPIINT32
	//  是否处于T+1时段。
	IsAddOne TAPIYNFLAG
	//  期权类型
	OptionType TAPIOptionType
}

func (p *TapAPICommodityInfo) Map() map[string]any {
	return map[string]any{
		//  交易所编码
		"ExchangeNo": p.ExchangeNo.String(),
		//  品种类型
		"CommodityType": p.CommodityType,
		//  品种编号
		"CommodityNo": p.CommodityNo.String(),
		//  品种名称
		"CommodityName": p.CommodityName.String(),
		//  品种英文名称
		"CommodityEngName": p.CommodityEngName.String(),
		// 交易币种
		"CurrencyGroupNo": p.CurrencyGroupNo.String(),
		//  每手乘数
		"ContractSize": p.ContractSize.Float64(),
		//  开平方式
		"OpenCloseMode": p.OpenCloseMode,
		//  最小变动价位
		"CommodityTickSize": p.CommodityTickSize.Float64(),
		//  期权类型
		"OptionType": p.OptionType,
	}
}

type TapAPITradeContractInfo struct {
	// < 交易所编码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编号
	CommodityNo TAPISTR_10
	// < 合约代码1
	ContractNo1 TAPISTR_10
	// < 执行价1
	StrikePrice1 TAPISTR_10
	// < 看涨看跌标示1
	CallOrPutFlag1 TAPICallOrPutFlagType
	// < 合约代码2
	ContractNo2 TAPISTR_10
	// < 执行价2
	StrikePrice2 TAPISTR_10
	// < 看涨看跌标示2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 合约类型
	ContractType TAPIContractTypeType
	// < 行情真实合约
	QuoteUnderlyingContract TAPISTR_10
	// < 合约名称
	ContractName TAPISTR_70
	// < 合约到期日
	ContractExpDate TAPIDATE
	// < 最后交易日
	LastTradeDate TAPIDATE
	// < 首次通知日
	FirstNoticeDate TAPIDATE
	// < 特殊合约层每手乘数
	ContractSize TAPIREAL64
}

type TapAPICurrencyInfo struct {
	// < 币种编号
	CurrencyNo TAPISTR_10
	// < 币种组编号
	CurrencyGroupNo TAPISTR_10
	// < 交易汇率
	TradeRate TAPIREAL64
	// < 交易汇率2
	TradeRate2 TAPIREAL64
	// < 币种组独立标志
	FutureAlg TAPIFutureAlgType
	// < 是否是基币
	OptionAlg TAPIOptionAlgType
}

type TapAPITradeMessageReq struct {
	// TAPIDATETIME BenginSendDateTime;
	AccountNo TAPISTR_20
	//
	EndSendDateTime TAPIDATETIME
}

type TapAPITradeMessage struct {
	// < 流号
	SerialID TAPIUINT32
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 消息有效时间
	TMsgValidDateTime TAPIDATETIME
	// < 消息标题
	TMsgTitle TAPISTR_50
	// < 消息内容
	TMsgContent TAPISTR_500
	// < 消息类型
	TMsgType TAPIMsgTypeType
	// < 消息级别
	TMsgLevel TAPIMsgLevelType
	// < 是否发送短信
	IsSendBySMS TAPIYNFLAG
	// < 是否发送邮件
	IsSendByEMail TAPIYNFLAG
	// < 发送者
	Sender TAPISTR_20
	// < 发送时间
	SendDateTime TAPIDATETIME
}

type TapAPIBillQryReq struct {
	// TAPIBillTypeType BillType;
	UserNo TAPISTR_20
	// TAPIBillFileTypeType BillFileType;
	BillDate TAPIDATE
}

type TapAPIBillQryRsp struct {
	// TAPIINT32 BillLen;
	Reqdata TapAPIBillQryReq
	// < 变长账单内容，长度由BillLen指定
	BillText [1]TAPICHAR
}

type TapAPIHisOrderQryReq struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 开始时间 (必填)
	BeginDate TAPIDATE
	// < 结束时间 (必填)
	EndDate TAPIDATE
}

type TapAPIHisOrderQryRsp struct {
	// < 日期
	Date TAPIDATE
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 委托有效类型
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 是否风险报单
	IsRiskOrder TAPIYNFLAG
	// < 买入卖出
	OrderSide TAPISideType
	// < 开平标志
	PositionEffect TAPIPositionEffectType
	// < 开平标志2
	PositionEffect2 TAPIPositionEffectType
	// < 询价号
	InquiryNo TAPISTR_50
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 委托价格
	OrderPrice TAPIREAL64
	// < 委托价格2，做市商应价使用
	OrderPrice2 TAPIREAL64
	// < 触发价格
	StopPrice TAPIREAL64
	// < 委托数量
	OrderQty TAPIUINT32
	// < 最小成交量
	OrderMinQty TAPIUINT32
	// < 撤单数量
	OrderCanceledQty TAPIUINT32
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考型。
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
	// < 委托流水号
	OrderStreamID TAPIUINT32
	// < 上手号
	UpperNo TAPISTR_10
	// < 上手通道编号
	UpperChannelNo TAPISTR_10
	// < 本地号
	OrderLocalNo TAPISTR_20
	// < 上手流号
	UpperStreamID TAPIUINT32
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 交易所系统号
	OrderExchangeSystemNo TAPISTR_50
	// < 父单系统号
	OrderParentSystemNo TAPISTR_50
	// < 下单人
	OrderInsertUserNo TAPISTR_20
	// < 下单时间
	OrderInsertTime TAPIDATETIME
	// < 指令下达人
	OrderCommandUserNo TAPISTR_20
	// < 委托更新人
	OrderUpdateUserNo TAPISTR_20
	// < 委托更新时间
	OrderUpdateTime TAPIDATETIME
	// < 委托状态
	OrderState TAPIOrderStateType
	// < 成交价
	OrderMatchPrice TAPIREAL64
	// < 成交价2
	OrderMatchPrice2 TAPIREAL64
	// < 成交量
	OrderMatchQty TAPIUINT32
	// < 成交量2
	OrderMatchQty2 TAPIUINT32
	// < 最后一次操作错误信息码
	ErrorCode TAPIUINT32
	// < 错误信息
	ErrorText TAPISTR_50
	// < 是否为录入委托单
	IsBackInput TAPIYNFLAG
	// < 委托成交删除标记
	IsDeleted TAPIYNFLAG
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 是否T+1有效
	AddOneIsValid TAPIYNFLAG
	// < 冰山单最小随机量
	MinClipSize TAPIUINT32
	// < 冰山单最大随机量
	MaxClipSize TAPIUINT32
	// < 软件授权号
	LicenseNo TAPISTR_50
	// < 策略单类型
	TacticsType TAPITacticsTypeType
	// < 触发条件
	TriggerCondition TAPITriggerConditionType
	// < 触发价格类型
	TriggerPriceType TAPITriggerPriceTypeType
}

type TapAPIHisMatchQryReq struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 开始日期，必填
	BeginDate TAPIDATE
	// < 结束日期，必填
	EndDate TAPIDATE
}

type TapAPIHisMatchQryRsp struct {
	// < 结算日期
	SettleDate TAPIDATE
	// < 交易日期
	TradeDate TAPIDATE
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
	// < 合约号
	ContractNo TAPISTR_10
	// < 执行价
	StrikePrice TAPISTR_10
	// < 看涨看跌标志
	CallOrPutFlag TAPICallOrPutFlagType
	// < 成交来源
	MatchSource TAPIMatchSourceType
	// < 买卖方向
	MatchSide TAPISideType
	// < 开平标志
	PositionEffect TAPIPositionEffectType
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 成交价
	MatchPrice TAPIREAL64
	// < 成交量
	MatchQty TAPIUINT32
	// < 委托号
	OrderNo TAPISTR_20
	// < 成交序号
	MatchNo TAPISTR_20
	// < 成交流水号
	MatchStreamID TAPIUINT32
	// < 上手号
	UpperNo TAPISTR_10
	// < 组合号
	MatchCmbNo TAPISTR_20
	// < 成交编号(交易所成交号)
	ExchangeMatchNo TAPISTR_70
	// < 上手流水号
	MatchUpperStreamID TAPIUINT32
	// TAPISTR_10 CommodityCurrency;  // 品种币种
	CommodityCurrencyGroup TAPISTR_10
	// < 成交金额
	Turnover TAPIREAL64
	// < 权利金收入
	PremiumIncome TAPIREAL64
	// < 权利金支出
	PremiumPay TAPIREAL64
	// < 客户手续费
	AccountFee TAPIREAL64
	// TAPISTR_10 AccountFeeCurrency;  ///< 客户手续费币种
	AccountFeeCurrencyGroup TAPISTR_10
	// < 人工客户手续费标记
	IsManualFee TAPIYNFLAG
	//  客户其他费用
	AccountOtherFee TAPIREAL64
	// < 上手手续费
	UpperFee TAPIREAL64
	// TAPISTR_10 UpperFeeCurrency;  ///< 上手手续费币种
	UpperFeeCurrencyGroup TAPISTR_10
	// < 人工上手手续费标记
	IsUpperManualFee TAPIYNFLAG
	//  上手其他费用
	UpperOtherFee TAPIREAL64
	// < 成交时间
	MatchDateTime TAPIDATETIME
	// < 上手成交时间
	UpperMatchDateTime TAPIDATETIME
	// < 平仓盈亏
	CloseProfit TAPIREAL64
	// < 指定平仓价格；
	ClosePrice TAPIREAL64
	// < 平仓量
	CloseQty TAPIUINT32
	// < 结算分组
	SettleGroupNo TAPISTR_10
	// < 操作员
	OperatorNo TAPISTR_20
	// < 操作时间
	OperateTime TAPIDATETIME
}

type TapAPIHisOrderProcessQryReq struct {
	// TAPISTR_20 OrderNo;
	Date TAPIDATE
}

type TapAPIHisPositionQryReq struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 日期
	Date TAPIDATE
	// < 数据类型
	SettleFlag TAPISettleFlagType
}

type TapAPIHisPositionQryRsp struct {
	// < 结算日期
	SettleDate TAPIDATE
	// < 开仓日期
	OpenDate TAPIDATE
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码
	CommodityNo TAPISTR_10
	// < 合约号
	ContractNo TAPISTR_10
	// < 执行价
	StrikePrice TAPISTR_10
	// < 看涨看跌标志
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买卖方向
	MatchSide TAPISideType
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 持仓价格
	PositionPrice TAPIREAL64
	// < 持仓量
	PositionQty TAPIUINT32
	// <
	OrderNo TAPISTR_20
	// < 持仓编号
	PositionNo TAPISTR_20
	// < 上手号
	UpperNo TAPISTR_10
	// < 品种币种组
	CurrencyGroup TAPISTR_10
	// < 品种币种
	Currency TAPISTR_10
	// < 上日结算价格
	PreSettlePrice TAPIREAL64
	// < 结算价格
	SettlePrice TAPIREAL64
	// < 持仓盈亏(盯市)
	PositionDProfit TAPIREAL64
	// < LME持仓盈亏
	LMEPositionProfit TAPIREAL64
	// < 期权市值
	OptionMarketValue TAPIREAL64
	// < 客户初始保证金
	AccountInitialMargin TAPIREAL64
	// < 客户维持保证金
	AccountMaintenanceMargin TAPIREAL64
	// < 上手初始保证金
	UpperInitialMargin TAPIREAL64
	// < 上手维持保证金
	UpperMaintenanceMargin TAPIREAL64
	// < 结算分组
	SettleGroupNo TAPISTR_10
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 上级账号
	SuperiorAccount TAPISTR_20
}

type TapAPIHisDeliveryQryReq struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 开始日期（必填）
	BeginDate TAPIDATE
	// < 结束日期（必填）
	EndDate TAPIDATE
	// < 结算类型
	SettleFlag TAPISettleFlagType
}

type TapAPIHisDeliveryQryRsp struct {
	// < 交割日期
	DeliveryDate TAPIDATE
	// < 开仓日期
	OpenDate TAPIDATE
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 市场号或交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码
	CommodityNo TAPISTR_10
	// < 合约编码
	ContractNo TAPISTR_10
	// < 执行价
	StrikePrice TAPISTR_10
	// < 看涨看跌标志
	CallOrPutFlag TAPICallOrPutFlagType
	// < 成交来源
	MatchSource TAPIMatchSourceType
	// < 开仓方向
	OpenSide TAPISideType
	// < 开仓价格
	OpenPrice TAPIREAL64
	// < 交割价格
	DeliveryPrice TAPIREAL64
	// < 交割量
	DeliveryQty TAPIUINT32
	// < 冻结量
	FrozenQty TAPIUINT32
	// < 开仓成交号
	OpenNo TAPISTR_20
	// < 上手编号
	UpperNo TAPISTR_10
	// < 品种币种
	CommodityCurrencyGroupy TAPISTR_10
	// < 品种币种
	CommodityCurrency TAPISTR_10
	// < 上日结算价
	PreSettlePrice TAPIREAL64
	// < 交割盈亏
	DeliveryProfit TAPIREAL64
	// < 客户初始冻结保证金
	AccountFrozenInitialMargin TAPIREAL64
	// < 客户维持冻结保证金
	AccountFrozenMaintenanceMargin TAPIREAL64
	// < 上手初始冻结保证金
	UpperFrozenInitialMargin TAPIREAL64
	// < 上手维持冻结保证金
	UpperFrozenMaintenanceMargin TAPIREAL64
	// TAPISTR_10 AccountFeeCurrency;  ///< 客户手续费币种
	AccountFeeCurrencyGroup TAPISTR_10
	// < 客户交割手续费
	AccountDeliveryFee TAPIREAL64
	// TAPISTR_10 UpperFeeCurrency;  ///< 上手手续费币种
	UpperFeeCurrencyGroup TAPISTR_10
	// < 上手交割手续费
	UpperDeliveryFee TAPIREAL64
	// < 交割行权方式
	DeliveryMode TAPIDeliveryModeType
	// < 操作员
	OperatorNo TAPISTR_20
	// < 操作时间
	OperateTime TAPIDATETIME
	// < 结算分组
	SettleGourpNo TAPISTR_20
	// < 特殊期权标的合约
	FutureContractNo TAPISTR_10
	// < 期权真实执行价
	OptionStrikePrice TAPISTR_10
	// < 上级账号
	SuperiorAccount TAPISTR_20
}

type TapAPIAccountCashAdjustQryReq struct {
	// TAPISTR_20 AccountNo;
	SerialID TAPIUINT32
	// < 必填
	BeginDate TAPIDATE
	// < 必填
	EndDate TAPIDATE
}

type TapAPIAccountCashAdjustQryRsp struct {
	// < 日期
	Date TAPIDATE
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 资金调整类型
	CashAdjustType TAPICashAdjustTypeType
	// < 币种组号
	CurrencyGroupNo TAPISTR_10
	// < 币种号
	CurrencyNo TAPISTR_10
	// < 资金调整金额
	CashAdjustValue TAPIREAL64
	// < 资金调整备注
	CashAdjustRemark TAPISTR_100
	// < 操作时间
	OperateTime TAPIDATETIME
	// < 操作员
	OperatorNo TAPISTR_20
	// < 客户银行
	AccountBank TAPISTR_10
	// < 客户银行账号
	BankAccount TAPISTR_20
	// < 客户本外币标识
	AccountLWFlag TAPIBankAccountLWFlagType
	// < 公司银行
	CompanyBank TAPISTR_10
	// < 公司银行账户
	InternalBankAccount TAPISTR_20
	// < 公司本外币标识
	CompanyLWFlag TAPIBankAccountLWFlagType
}

type TapAPIAccountFeeRentQryReq struct {
	//
	AccountNo TAPISTR_20
}

type TapAPIAccountFeeRentQryRsp struct {
	// TAPISTR_10 ExchangeNo;
	AccountNo TAPISTR_20
	// TAPISTR_10 CommodityNo;
	CommodityType TAPICommodityType
	// TAPICalculateModeType CalculateMode;
	MatchSource TAPIMatchSourceType
	// TAPISTR_10 CurrencyNo;
	CurrencyGroupNo TAPISTR_10
	// TAPIREAL64 CloseTodayFee;
	OpenCloseFee TAPIREAL64
}

type TapAPIAccountMarginRentQryReq struct {
	// TAPISTR_10 ExchangeNo;
	AccountNo TAPISTR_20
	// TAPISTR_10 CommodityNo;
	CommodityType TAPICommodityType
}

type TapAPIAccountMarginRentQryRsp struct {
	// TAPISTR_10 ExchangeNo;
	AccountNo TAPISTR_20
	// TAPISTR_10 CommodityNo;
	CommodityType TAPICommodityType
	// TAPISTR_10 StrikePrice;
	ContractNo TAPISTR_10
	// TAPICalculateModeType CalculateMode;
	CallOrPutFlag TAPICallOrPutFlagType
	// TAPISTR_10 CurrencyNo;
	CurrencyGroupNo TAPISTR_10
	// TAPIREAL64 MaintenanceMargin;
	InitialMargin TAPIREAL64
	// TAPIREAL64 SellMaintenanceMargin;
	SellInitialMargin TAPIREAL64
	//
	LockMargin TAPIREAL64
}

type TapAPIOrderQuoteMarketNotice struct {
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编号
	CommodityNo TAPISTR_10
	// < 合约
	ContractNo TAPISTR_10
	// < 执行价
	StrikePrice TAPISTR_10
	// < 看涨看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买卖方向
	OrderSide TAPISideType
	// < 委托量
	OrderQty TAPIUINT32
}

type TapAPIOrderMarketInsertReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 有效类型
	TimeInForce TAPITimeInForceType
	// < 有效期
	ExpireTime TAPIDATETIME
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 买开平标志
	BuyPositionEffect TAPIPositionEffectType
	// < 卖开平标志
	SellPositionEffect TAPIPositionEffectType
	// < 是否T+1有效
	AddOneIsValid TAPIYNFLAG
	// < 买委托价
	OrderBuyPrice TAPIREAL64
	// < 卖委托价
	OrderSellPrice TAPIREAL64
	// < 买委托量
	OrderBuyQty TAPIUINT32
	// < 卖委托量
	OrderSellQty TAPIUINT32
	// < 本地委托编号
	ClientBuyOrderNo TAPISTR_50
	// < 本地委托编号
	ClientSellOrderNo TAPISTR_50
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考值
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 备注
	Remark TAPISTR_100
}

type TapAPIOrderMarketInsertRsp struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 委托有效类型
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 买开平标志
	BuyPositionEffect TAPIPositionEffectType
	// < 卖开平标志
	SellPositionEffect TAPIPositionEffectType
	// < 买委托价
	OrderBuyPrice TAPIREAL64
	// < 卖委托价
	OrderSellPrice TAPIREAL64
	// < 买委托量
	OrderBuyQty TAPIUINT32
	// < 卖委托量
	OrderSellQty TAPIUINT32
	// < 交易服务标识
	ServerFlag TAPICHAR
	// < 买委托号
	OrderBuyNo TAPISTR_20
	// < 卖委托号
	OrderSellNo TAPISTR_20
	// < 是否T+1有效
	AddOneIsValid TAPIYNFLAG
	// < 下单人
	OrderMarketUserNo TAPISTR_20
	// < 下单时间
	OrderMarketTime TAPIDATETIME
	// < 整型参考值
	RefInt TAPIINT32
	// < 浮点参考值
	RefDouble TAPIREAL64
	// < 字符串参考值
	RefString TAPISTR_50
	// < 买本地委托编号
	ClientBuyOrderNo TAPISTR_50
	// < 卖本地委托编号
	ClientSellOrderNo TAPISTR_50
	// < 错误信息码
	ErrorCode TAPIUINT32
	// < 错误信息
	ErrorText TAPISTR_50
	// < 终端本地IP地址（客户端填写）
	ClientLocalIP TAPISTR_40
	// < 终端本地Mac地址（客户端填写）
	ClientMac TAPIMACTYPE
	// < 前置记录的终端IP地址（前置填写）
	ClientIP TAPISTR_40
	// < 备注
	Remark TAPISTR_100
}

type TapAPIOrderMarketDeleteReq struct {
	// TAPISTR_20 OrderBuyNo;   ///< 买委托号
	ServerFlag TAPICHAR
	// < 卖委托号
	OrderSellNo TAPISTR_20
}

type TapAPIOrderLocalRemoveReq struct {
	// TAPISTR_20 OrderNo;
	ServerFlag TAPICHAR
}

type TapAPIOrderLocalRemoveRsp struct {
	// TAPISTR_40 ClientLocalIP;  // 终端本地IP地址（客户端填写）
	req TapAPIOrderLocalRemoveReq
	//  终端本地Mac地址（客户端填写）
	ClientMac TAPIMACTYPE
	//  前置记录的终端IP地址（前置填写）
	ClientIP TAPISTR_40
}

type TapAPIOrderLocalInputReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格1
	StrikePrice TAPISTR_10
	// < 看张看跌1
	CallOrPutFlag TAPICallOrPutFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 委托类型
	OrderType TAPIOrderTypeType
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 委托有效类型
	TimeInForce TAPITimeInForceType
	// < 有效日期(GTD情况下使用)
	ExpireTime TAPIDATETIME
	// < 是否风险报单
	IsRiskOrder TAPIYNFLAG
	// < 买入卖出
	OrderSide TAPISideType
	// < 开平标志1
	PositionEffect TAPIPositionEffectType
	// < 开平标志2
	PositionEffect2 TAPIPositionEffectType
	// < 询价号
	InquiryNo TAPISTR_50
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 委托价格1
	OrderPrice TAPIREAL64
	// < 委托价格2，做市商应价使用
	OrderPrice2 TAPIREAL64
	// < 触发价格
	StopPrice TAPIREAL64
	// < 委托数量
	OrderQty TAPIUINT32
	// < 最小成交量
	OrderMinQty TAPIUINT32
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 交易所系统号
	OrderExchangeSystemNo TAPISTR_50
	// < 上手号
	UpperNo TAPISTR_10
	// < 成交价1
	OrderMatchPrice TAPIREAL64
	// < 成交价2
	OrderMatchPrice2 TAPIREAL64
	// < 成交量1
	OrderMatchQty TAPIUINT32
	// < 成交量2
	OrderMatchQty2 TAPIUINT32
	// < 委托状态
	OrderState TAPIOrderStateType
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 下级资金帐号
	LowerAccount TAPISTR_20
}

type TapAPIOrderLocalModifyReq struct {
	// TAPICHAR ServerFlag;  ///< 服务器标识
	req TapAPIOrderLocalInputReq
	// < 委托编码
	OrderNo TAPISTR_20
}

type TapAPIOrderLocalTransferReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 委托编码
	OrderNo TAPISTR_20
}

type TapAPIFillLocalInputReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格
	StrikePrice TAPISTR_10
	// < 看张看跌
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买入卖出
	MatchSide TAPISideType
	// < 开平标志1
	PositionEffect TAPIPositionEffectType
	// < 投机保值
	HedgeFlag TAPIHedgeFlagType
	// < 成交价
	MatchPrice TAPIREAL64
	// < 成交量
	MatchQty TAPIUINT32
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 上手成交号
	UpperMatchNo TAPISTR_70
	// < 成交时间
	MatchDateTime TAPIDATETIME
	// < 上手成交时间
	UpperMatchDateTime TAPIDATETIME
	// < 上手号
	UpperNo TAPISTR_10
	// < 是否为T+1单
	IsAddOne TAPIYNFLAG
	// < 客户手续费币种组
	FeeCurrencyGroup TAPISTR_10
	// < 客户手续费币种
	FeeCurrency TAPISTR_10
	// < 手续费
	FeeValue TAPIREAL64
	// < 人工客户手续费标记
	IsManualFee TAPIYNFLAG
	// < 指定价格平仓
	ClosePositionPrice TAPIREAL64
}

type TapAPIFillLocalRemoveReq struct {
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 本地成交号
	MatchNo TAPISTR_20
}

type TapAPITradingCalendarQryRsp struct {
	// < 当前交易日
	CurrTradeDate TAPIDATE
	// < 上次结算日
	LastSettlementDate TAPIDATE
	// < LME到期日
	PromptDate TAPIDATE
	// < 上日LME到期日
	LastPromptDate TAPIDATE
}

type TapAPISpotLockQryReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
}

type TapAPISpotLockDataRsp struct {
	// < 客户资金账号
	AccountNo TAPISTR_20
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码类型
	CommodityNo TAPISTR_10
	// < 锁定量
	LockQty TAPIUINT32
	// < 冻结量
	FrozenQty TAPIUINT32
	// < 可解锁量
	CanUnLockQty TAPIUINT32
}

type TapAPISubmitUserLoginInfo struct {
	// < 用户名
	UserNo TAPISTR_20
	// < 用户终端采集信息
	GatherInfo TAPISTR_500
	// < 用户公网IP
	ClientLoginIP TAPISTR_40
	// < 用户公网Port
	ClientLoginPort TAPIUINT32
	// < 用户登录时间
	ClientLoginDateTime TAPIDATETIME
	// < 用户AppID
	ClientAppID TAPISTR_30
	// < 用户终端信息加密密钥版本号
	AuthKeyVersion TAPIUINT32
	// < 用户采集信息异常标识
	AbnormalNo TapAPIAbnormalFalgType
}

type TapAPISubmitUserLoginRspInfo struct {
	// < 用户名
	UserNo TAPISTR_20
}

type TapAPISpecialOrderInsertReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 特殊业务类型
	SpecialOrderType TapAPISpecialOrderTypeType
	// < 委托来源
	OrderSource TAPIOrderSourceType
	// < 组合编码
	CombineNo TAPISTR_50
	// < 委托数量
	OrderQty TAPIUINT32
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格1
	StrikePrice TAPISTR_10
	// < 看张看跌1
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买卖方向1
	OrderSide1 TAPISideType
	// < 投机备兑1
	HedgeFlag1 TAPIHedgeFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
}

type TapAPISpecialOrderQryReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 特殊业务委托编号
	OrderNo TAPISTR_20
}

type TapAPISpecialOrderInfo struct {
	// < 会话ID
	SessionID TAPIUINT32
	// < 错误码
	ErrorCode TAPIUINT32
	// < 错误信息
	ErrorText TAPISTR_50
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 特殊业务委托编号
	OrderNo TAPISTR_20
	// < 客户端本地委托编号
	ClientOrderNo TAPISTR_50
	// < 特殊业务类型
	SpecialOrderType TapAPISpecialOrderTypeType
	// < 委托来源，默认程序单。
	OrderSource TAPIOrderSourceType
	// < 组合策略代码
	CombineStrategy TapAPICombineStrategyType
	// < 组合编码
	CombineNo TAPISTR_50
	// < 委托数量
	OrderQty TAPIUINT32
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格1
	StrikePrice TAPISTR_10
	// < 看张看跌1
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买卖方向1
	OrderSide1 TAPISideType
	// < 组合数量1
	CombineQty1 TAPIUINT32
	// < 投机备兑1
	HedgeFlag1 TAPIHedgeFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 买卖方向2
	OrderSide2 TAPISideType
	// < 组合数量2
	CombineQty2 TAPIUINT32
	// < 投机备兑2
	HedgeFlag2 TAPIHedgeFlagType
	// < 软件授权号
	LicenseNo TAPISTR_50
	// < 终端本地IP
	ClientLocalIP TAPISTR_40
	// < 终端本地Mac地址
	ClientMac TAPIMACTYPE
	// < 终端网络地址.
	ClientIP TAPISTR_40
	// < 委托流水号
	OrderStreamID TAPIUINT32
	// < 上手号
	UpperNo TAPISTR_10
	// < 上手通道号
	UpperChannelNo TAPISTR_10
	// < 网关本地号
	OrderLocalNo TAPISTR_20
	// < 系统号
	OrderSystemNo TAPISTR_50
	// < 交易所系统号
	OrderExchangeSystemNo TAPISTR_50
	// < 下单人
	OrderInsertUserNo TAPISTR_20
	// < 下单时间
	OrderInsertTime TAPIDATETIME
	// < 委托状态
	OrderState TAPIOrderStateType
}

type TapAPICombinePositionQryReq struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
}

type TapAPICombinePositionInfo struct {
	// < 客户资金帐号
	AccountNo TAPISTR_20
	// < 组合持仓流号
	PositionStreamID TAPIUINT32
	// < 服务器标识
	ServerFlag TAPICHAR
	// < 上手号
	UpperNo TAPISTR_10
	// < 组合策略代码
	CombineStrategy TapAPICombineStrategyType
	// < 组合编码
	CombineNo TAPISTR_50
	// < 委托数量
	PositionQty TAPIUINT32
	// < 交易所编号
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编码
	CommodityNo TAPISTR_10
	// < 合约1
	ContractNo TAPISTR_10
	// < 执行价格1
	StrikePrice TAPISTR_10
	// < 看张看跌1
	CallOrPutFlag TAPICallOrPutFlagType
	// < 买卖方向1
	OrderSide1 TAPISideType
	// < 组合数量1
	CombineQty1 TAPIUINT32
	// < 投机备兑1
	HedgeFlag1 TAPIHedgeFlagType
	// < 合约2
	ContractNo2 TAPISTR_10
	// < 执行价格2
	StrikePrice2 TAPISTR_10
	// < 看张看跌2
	CallOrPutFlag2 TAPICallOrPutFlagType
	// < 买卖方向2
	OrderSide2 TAPISideType
	// < 组合数量2
	CombineQty2 TAPIUINT32
	// < 投机备兑2
	HedgeFlag2 TAPIHedgeFlagType
	// < 品种币种组
	CommodityCurrencyGroup TAPISTR_10
	// < 品种币种
	CommodityCurrency TAPISTR_10
	// < 初始组合保证金
	AccountInitialMargin TAPIREAL64
	// < 维持组合保证金
	AccountMaintenanceMargin TAPIREAL64
	// < 上手初始组合保证金
	UpperInitialMargin TAPIREAL64
	// < 上手维持组合保证金
	UpperMaintenanceMargin TAPIREAL64
}

type TapAPIUserTrustDeviceQryReq struct {
}

type TapAPIUserTrustDeviceQryRsp struct {
	// < 登录账号
	UserNo TAPISTR_20
	// < 软件授权码
	LicenseNo TAPISTR_50
	// < MAC
	Mac TAPISTR_50
	// < 设备名称
	DeviceName TAPISTR_50
	// < 操作员
	OperatorNo TAPISTR_20
	// < 操作时间
	OperateTime TAPIDATETIME
}

type TapAPIUserTrustDeviceAddReq struct {
}

type TapAPIUserTrustDeviceDelReq struct {
	// < 软件授权码
	LicenseNo TAPISTR_50
	// < MAC
	Mac TAPISTR_50
}

type TapAPIIPOInfoQryReq struct {
}

type TapAPIIPOInfoQryRsp struct {
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
	// < 币种组
	CurrencyGroupNo TAPISTR_10
	// < 币种
	CurrencyNo TAPISTR_10
	// < 申购开始日期
	BeginDate TAPIDATETIME
	// < 申购结束日期
	EndDate TAPIDATETIME
	// < IPODate
	IPODate TAPIDATETIME
	// < 中签日
	ResultDate TAPIDATETIME
	// < IPO手续费
	IPOFee TAPIREAL64
	// < 融资手续费
	FinancingFee TAPIREAL64
	// < 融资利率
	LoanRatio TAPIREAL64
	// < 融资天数
	FinancingDays TAPIUINT32
	// < 最高融资比例
	MaxLoanRatio TAPIREAL64
	// < 最高融资金额
	MaxLoanValue TAPIREAL64
	// < 认购/配售价
	Price TAPIREAL64
	// TAPIDATETIME OperateTime;
	OperatorNo TAPISTR_20
}

type TapAPIAvailableApplyQryReq struct {
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
}

type TapAPIAvailableApplyQryRsp struct {
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
	// < 可申购股数
	StockQty TAPIUINT32
}

type TapAPIAccountIPOQryReq struct {
	//
	AccountNo TAPISTR_20
}

type TapAPIAccountIPOQryRsp struct {
	// < 申购结束日期
	EndDate TAPIDATETIME
	// < 中签日
	ResultDate TAPIDATETIME
	// TAPISTR_20 AccountNo;
	IPODate TAPIDATETIME
	// < 市场或者交易所代码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
	// < 申购类型
	ApplyType TapAPIApplyTypeType
	// < 申购数量
	ApplyQty TAPIUINT32
	// < 申购金额
	ApplyCash TAPIREAL64
	// < 融资比例
	LoanRatio TAPIREAL64
	// < 融资利息
	LoanInterest TAPIREAL64
	// < 申购手续费
	ApplyFee TAPIREAL64
	// < 申购状态
	ApplyStatus TapAPIApplyStatusType
	// < 中签量
	ResultQty TAPIUINT32
	// TAPIDATETIME OperateTime;
	OperatorNo TAPISTR_20
}

type TapAPIAccountIPOAddReq struct {
	// TAPISTR_10 ExchangeNo;            ///< 市场或者交易所代码
	AccountNo TAPISTR_20
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
	// < 申购类型
	ApplyType TapAPIApplyTypeType
	// < 申购数量
	ApplyQty TAPIUINT32
	// < 融资比例
	LoanRatio TAPIREAL64
}

type TapAPIAccountIPOCancelReq struct {
	// TAPISTR_10 ExchangeNo;            ///< 市场或者交易所代码
	AccountNo TAPISTR_20
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种号
	CommodityNo TAPISTR_10
}

type TapAPIVerifyIdentityReq struct {
	// TapAPICertificateTypeType CertificateType;  // 证件类型
	UserNo TAPISTR_20
	//  证件号码
	CertificateNo TAPISTR_50
	//  电子邮箱账号
	EMail TapAPIContactContentType
	//  手机号
	PhoneNo TapAPIContactContentType
}
