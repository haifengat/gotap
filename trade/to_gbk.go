package trade

import (
	"bytes"

	"golang.org/x/text/encoding/simplifiedchinese"
)

func toGBK(bs []byte) string {
	msg, _ := simplifiedchinese.GB18030.NewDecoder().Bytes(bytes.Split(bs, []byte("\x00"))[0])
	return string(msg)
}
