package quote

type TapAPIApplicationInfo struct {
	// < 授权码
	AuthCode TAPIAUTHCODE
	// < 关键操作日志路径
	KeyOperationLogPath TAPISTR_300
}

type TapAPICommodity struct {
	// < 交易所编码
	ExchangeNo TAPISTR_10
	// < 品种类型
	CommodityType TAPICommodityType
	// < 品种编号
	CommodityNo TAPISTR_10
}

type TapAPIContract struct {
	// < 品种
	Commodity TapAPICommodity
	// < 合约代码1
	ContractNo1 TAPISTR_10
	// < 执行价1
	StrikePrice1 TAPISTR_10
	// < 看涨看跌标示1
	CallOrPutFlag1 TAPICallOrPutFlagType
	// < 合约代码2
	ContractNo2 TAPISTR_10
	// < 执行价2
	StrikePrice2 TAPISTR_10
	// < 看涨看跌标示2
	CallOrPutFlag2 TAPICallOrPutFlagType
}

type TapAPIExchangeInfo struct {
	// < 交易所编码
	ExchangeNo TAPISTR_10
	// < 交易所名称
	ExchangeName TAPISTR_20
}

type TapAPIChangePasswordReq struct {
	// < 旧密码
	OldPassword TAPISTR_20
	// < 新密码
	NewPassword TAPISTR_20
}

type TapAPIQuoteLoginAuth struct {
	// < 用户名
	UserNo TAPISTR_20
	// < 是否修改密码，'Y'表示是，'N'表示否
	ISModifyPassword TAPIYNFLAG
	// < 用户密码
	Password TAPISTR_20
	// < 新密码，如果设置了修改密码则需要填写此字段
	NewPassword TAPISTR_20
	// < 行情临时密码
	QuoteTempPassword TAPISTR_20
	// < 是否需呀动态认证，'Y'表示是，'N'表示否
	ISDDA TAPIYNFLAG
	// < 动态认证码
	DDASerialNo TAPISTR_30
}

type TapAPIQuotLoginRspInfo struct {
	// < 用户名
	UserNo TAPISTR_20
	// < 用户类型
	UserType TAPIINT32
	// < 昵称，GBK编码格式
	UserName TAPISTR_20
	// < 行情临时密码
	QuoteTempPassword TAPISTR_20
	// < 用户自己设置的预留信息
	ReservedInfo TAPISTR_50
	// < 上次登录的地址
	LastLoginIP TAPISTR_40
	// < 上次登录使用的端口
	LastLoginProt TAPIUINT32
	// < 上次登录的时间
	LastLoginTime TAPIDATETIME
	// < 上次退出的时间
	LastLogoutTime TAPIDATETIME
	// < 当前交易日期
	TradeDate TAPIDATE
	// < 上次结算时间
	LastSettleTime TAPIDATETIME
	// < 系统启动时间
	StartTime TAPIDATETIME
	// < 系统初始化时间
	InitTime TAPIDATETIME
}

type TapAPIQuoteCommodityInfo struct {
	// < 品种
	Commodity TapAPICommodity
	// < 品种名称,GBK编码格式
	CommodityName TAPISTR_20
	// < 品种英文名称
	CommodityEngName TAPISTR_30
	// < 每手乘数
	ContractSize TAPIREAL64
	// < 最小变动价位
	CommodityTickSize TAPIREAL64
	// < 报价分母
	CommodityDenominator TAPIINT32
	// < 组合方向
	CmbDirect TAPICHAR
	// < 品种合约年限
	CommodityContractLen TAPIINT32
	// < 是否夏令时,'Y'为是,'N'为否
	IsDST TAPIYNFLAG
	// < 关联品种1
	RelateCommodity1 TapAPICommodity
	// < 关联品种2
	RelateCommodity2 TapAPICommodity
}

type TapAPIQuoteContractInfo struct {
	// < 合约
	Contract TapAPIContract
	// < 合约类型,'1'表示交易行情合约,'2'表示行情合约
	ContractType TAPICHAR
	// < 行情真实合约
	QuoteUnderlyingContract TAPISTR_10
	// < 合约名称
	ContractName TAPISTR_70
	// < 合约到期日
	ContractExpDate TAPIDATE
	// < 最后交易日
	LastTradeDate TAPIDATE
	// < 首次通知日
	FirstNoticeDate TAPIDATE
}

type TapAPIQuoteWhole struct {
	// < 合约
	Contract TapAPIContract
	// < 币种编号
	CurrencyNo TAPISTR_10
	// < 交易状态。1,集合竞价;2,集合竞价撮合;3,连续交易;4,交易暂停;5,闭市
	TradingState TAPICHAR
	// < 时间戳
	DateTimeStamp TAPIDTSTAMP
	// < 昨收盘价
	QPreClosingPrice TAPIQPRICE
	// < 昨结算价
	QPreSettlePrice TAPIQPRICE
	// < 昨持仓量
	QPrePositionQty TAPIQVOLUME
	// < 开盘价
	QOpeningPrice TAPIQPRICE
	// < 最新价
	QLastPrice TAPIQPRICE
	// < 最高价
	QHighPrice TAPIQPRICE
	// < 最低价
	QLowPrice TAPIQPRICE
	// < 历史最高价
	QHisHighPrice TAPIQPRICE
	// < 历史最低价
	QHisLowPrice TAPIQPRICE
	// < 涨停价
	QLimitUpPrice TAPIQPRICE
	// < 跌停价
	QLimitDownPrice TAPIQPRICE
	// < 当日总成交量
	QTotalQty TAPIQVOLUME
	// < 当日成交金额
	QTotalTurnover TAPIQPRICE
	// < 持仓量
	QPositionQty TAPIQVOLUME
	// < 均价
	QAveragePrice TAPIQPRICE
	// < 收盘价
	QClosingPrice TAPIQPRICE
	// < 结算价
	QSettlePrice TAPIQPRICE
	// < 最新成交量
	QLastQty TAPIQVOLUME
	// < 买价1-20档
	QBidPrice [20]TAPIQPRICE
	// < 买量1-20档
	QBidQty [20]TAPIQVOLUME
	// < 卖价1-20档
	QAskPrice [20]TAPIQPRICE
	// < 卖量1-20档
	QAskQty [20]TAPIQVOLUME
	// < 隐含买价
	QImpliedBidPrice TAPIQPRICE
	// < 隐含买量
	QImpliedBidQty TAPIQVOLUME
	// < 隐含卖价
	QImpliedAskPrice TAPIQPRICE
	// < 隐含卖量
	QImpliedAskQty TAPIQVOLUME
	// < 昨虚实度
	QPreDelta TAPIQPRICE
	// < 今虚实度
	QCurrDelta TAPIQPRICE
	// < 内盘量
	QInsideQty TAPIQVOLUME
	// < 外盘量
	QOutsideQty TAPIQVOLUME
	// < 换手率
	QTurnoverRate TAPIQPRICE
	// < 五日均量
	Q5DAvgQty TAPIQVOLUME
	// < 市盈率
	QPERatio TAPIQPRICE
	// < 总市值
	QTotalValue TAPIQPRICE
	// < 流通市值
	QNegotiableValue TAPIQPRICE
	// < 持仓走势
	QPositionTrend TAPIQDIFF
	// < 涨速
	QChangeSpeed TAPIQPRICE
	// < 涨幅
	QChangeRate TAPIQPRICE
	// < 涨跌值
	QChangeValue TAPIQPRICE
	// < 振幅
	QSwing TAPIQPRICE
	// < 委买总量
	QTotalBidQty TAPIQVOLUME
	// < 委卖总量
	QTotalAskQty TAPIQVOLUME
	// < 虚拟合约对应的真实合约
	UnderlyContract TapAPIContract
}
