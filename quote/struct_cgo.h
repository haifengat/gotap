

struct TapAPIApplicationInfo 
{
    
	char AuthCode[513]; // < 授权码
    
	char KeyOperationLogPath[300]; // < 关键操作日志路径
    
};

struct TapAPICommodity 
{
    
	char ExchangeNo[10]; // < 交易所编码
    
	char CommodityType; // < 品种类型
    
	char CommodityNo[10]; // < 品种编号
    
};

struct TapAPIContract 
{
    
	struct TapAPICommodity Commodity; // < 品种
    
	char ContractNo1[10]; // < 合约代码1
    
	char StrikePrice1[10]; // < 执行价1
    
	char CallOrPutFlag1; // < 看涨看跌标示1
    
	char ContractNo2[10]; // < 合约代码2
    
	char StrikePrice2[10]; // < 执行价2
    
	char CallOrPutFlag2; // < 看涨看跌标示2
    
};

struct TapAPIExchangeInfo 
{
    
	char ExchangeNo[10]; // < 交易所编码
    
	char ExchangeName[20]; // < 交易所名称
    
};

struct TapAPIChangePasswordReq 
{
    
	char OldPassword[20]; // < 旧密码
    
	char NewPassword[20]; // < 新密码
    
};

struct TapAPIQuoteLoginAuth 
{
    
	char UserNo[20]; // < 用户名
    
	char ISModifyPassword; // < 是否修改密码，'Y'表示是，'N'表示否
    
	char Password[20]; // < 用户密码
    
	char NewPassword[20]; // < 新密码，如果设置了修改密码则需要填写此字段
    
	char QuoteTempPassword[20]; // < 行情临时密码
    
	char ISDDA; // < 是否需呀动态认证，'Y'表示是，'N'表示否
    
	char DDASerialNo[30]; // < 动态认证码
    
};

struct TapAPIQuotLoginRspInfo 
{
    
	char UserNo[20]; // < 用户名
    
	int UserType; // < 用户类型
    
	char UserName[20]; // < 昵称，GBK编码格式
    
	char QuoteTempPassword[20]; // < 行情临时密码
    
	char ReservedInfo[50]; // < 用户自己设置的预留信息
    
	char LastLoginIP[40]; // < 上次登录的地址
    
	unsigned int LastLoginProt; // < 上次登录使用的端口
    
	char LastLoginTime[20]; // < 上次登录的时间
    
	char LastLogoutTime[20]; // < 上次退出的时间
    
	char TradeDate[11]; // < 当前交易日期
    
	char LastSettleTime[20]; // < 上次结算时间
    
	char StartTime[20]; // < 系统启动时间
    
	char InitTime[20]; // < 系统初始化时间
    
};

struct TapAPIQuoteCommodityInfo 
{
    
	struct TapAPICommodity Commodity; // < 品种
    
	char CommodityName[20]; // < 品种名称,GBK编码格式
    
	char CommodityEngName[30]; // < 品种英文名称
    
	double ContractSize; // < 每手乘数
    
	double CommodityTickSize; // < 最小变动价位
    
	int CommodityDenominator; // < 报价分母
    
	char CmbDirect; // < 组合方向
    
	int CommodityContractLen; // < 品种合约年限
    
	char IsDST; // < 是否夏令时,'Y'为是,'N'为否
    
	struct TapAPICommodity RelateCommodity1; // < 关联品种1
    
	struct TapAPICommodity RelateCommodity2; // < 关联品种2
    
};

struct TapAPIQuoteContractInfo 
{
    
	struct TapAPIContract Contract; // < 合约
    
	char ContractType; // < 合约类型,'1'表示交易行情合约,'2'表示行情合约
    
	char QuoteUnderlyingContract[10]; // < 行情真实合约
    
	char ContractName[70]; // < 合约名称
    
	char ContractExpDate[11]; // < 合约到期日	
    
	char LastTradeDate[11]; // < 最后交易日
    
	char FirstNoticeDate[11]; // < 首次通知日
    
};

struct TapAPIQuoteWhole 
{
    
	struct TapAPIContract Contract; // < 合约
    
	char CurrencyNo[10]; // < 币种编号
    
	char TradingState; // < 交易状态。1,集合竞价;2,集合竞价撮合;3,连续交易;4,交易暂停;5,闭市
    
	char DateTimeStamp[24]; // < 时间戳
    
	double QPreClosingPrice; // < 昨收盘价
    
	double QPreSettlePrice; // < 昨结算价
    
	unsigned long long QPrePositionQty; // < 昨持仓量
    
	double QOpeningPrice; // < 开盘价
    
	double QLastPrice; // < 最新价
    
	double QHighPrice; // < 最高价
    
	double QLowPrice; // < 最低价
    
	double QHisHighPrice; // < 历史最高价
    
	double QHisLowPrice; // < 历史最低价
    
	double QLimitUpPrice; // < 涨停价
    
	double QLimitDownPrice; // < 跌停价
    
	unsigned long long QTotalQty; // < 当日总成交量
    
	double QTotalTurnover; // < 当日成交金额
    
	unsigned long long QPositionQty; // < 持仓量
    
	double QAveragePrice; // < 均价
    
	double QClosingPrice; // < 收盘价
    
	double QSettlePrice; // < 结算价
    
	unsigned long long QLastQty; // < 最新成交量
    
	double QBidPrice[20]; // < 买价1-20档
    
	unsigned long long QBidQty[20]; // < 买量1-20档
    
	double QAskPrice[20]; // < 卖价1-20档
    
	unsigned long long QAskQty[20]; // < 卖量1-20档
    
	double QImpliedBidPrice; // < 隐含买价
    
	unsigned long long QImpliedBidQty; // < 隐含买量
    
	double QImpliedAskPrice; // < 隐含卖价
    
	unsigned long long QImpliedAskQty; // < 隐含卖量
    
	double QPreDelta; // < 昨虚实度
    
	double QCurrDelta; // < 今虚实度
    
	unsigned long long QInsideQty; // < 内盘量
    
	unsigned long long QOutsideQty; // < 外盘量
    
	double QTurnoverRate; // < 换手率
    
	unsigned long long Q5DAvgQty; // < 五日均量
    
	double QPERatio; // < 市盈率
    
	double QTotalValue; // < 总市值
    
	double QNegotiableValue; // < 流通市值
    
	long long QPositionTrend; // < 持仓走势
    
	double QChangeSpeed; // < 涨速
    
	double QChangeRate; // < 涨幅
    
	double QChangeValue; // < 涨跌值
    
	double QSwing; // < 振幅
    
	unsigned long long QTotalBidQty; // < 委买总量
    
	unsigned long long QTotalAskQty; // < 委卖总量
    
	struct TapAPIContract UnderlyContract; // < 虚拟合约对应的真实合约
    
};
