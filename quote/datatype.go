package quote

// uint 64
type TAPIUINT64  uint64 

// 长度为10的字符串
type TAPISTR_10  [11]byte 

// 长度为20的字符串
type TAPISTR_20  [21]byte 

// 长度为30的字符串
type TAPISTR_30  [31]byte 

// 长度为40的字符串
type TAPISTR_40  [41]byte 

// 长度为50的字符串
type TAPISTR_50  [51]byte 

// 长度为70的字符串
type TAPISTR_70  [71]byte 

// 长度为100的字符串
type TAPISTR_100  [101]byte 

// 长度为300的字符串
type TAPISTR_300  [301]byte 

// 长度为500的字符串
type TAPISTR_500  [501]byte 

// 长度为2000的字符串
type TAPISTR_2000  [2001]byte 

// Authorization Code
type TAPIAUTHCODE  [513]byte 

// 单字符定义，可用于定义其他类型
type TAPICHAR  byte 

// int 32
type TAPIINT32  int 

// unsigned 32
type TAPIUINT32  uint 

// int 64
type TAPIINT64  int64 

// unsigned 64
type long  uint64 

// unsigned 16
type TAPIUINT16  uint16 

// unsigned 8
type TAPIUINT8  uint8 

// real 64
type TAPIREAL64  float64 

// 是否标示
type TAPIYNFLAG  TAPICHAR 
const APIYNFLAG_YES TAPIYNFLAG = 'Y' // 是
const APIYNFLAG_NO TAPIYNFLAG = 'N' // 否

// 时间戳类型(格式 yyyy-MM-dd hh:nn:ss.xxx)
type TAPIDTSTAMP  [24]byte 

// 日期和时间类型(格式 yyyy-MM-dd hh:nn:ss)
type TAPIDATETIME  [20]byte 

// 日期类型(格式 yyyy-MM-dd)
type TAPIDATE  [11]byte 

// 时间类型(格式 hh:nn:ss)
type TAPITIME  [9]byte 

// 日志级别
type TAPILOGLEVEL  TAPICHAR 
const APILOGLEVEL_NONE TAPILOGLEVEL = 'N' // 无
const APILOGLEVEL_ERROR TAPILOGLEVEL = 'E' // Error
const APILOGLEVEL_WARNING TAPILOGLEVEL = 'W' // Warning
const APILOGLEVEL_DEBUG TAPILOGLEVEL = 'D' // Debug

// 品种类型
type TAPICommodityType  TAPICHAR 
const TAPI_COMMODITY_TYPE_NONE TAPICommodityType = 'N' // 无
const TAPI_COMMODITY_TYPE_SPOT TAPICommodityType = 'P' // 现货
const TAPI_COMMODITY_TYPE_FUTURES TAPICommodityType = 'F' // 期货
const TAPI_COMMODITY_TYPE_OPTION TAPICommodityType = 'O' // 期权
const TAPI_COMMODITY_TYPE_SPREAD_MONTH TAPICommodityType = 'S' // 跨期套利
const TAPI_COMMODITY_TYPE_SPREAD_COMMODITY TAPICommodityType = 'M' // 跨品种套利
const TAPI_COMMODITY_TYPE_BUL TAPICommodityType = 'U' // 看涨垂直套利
const TAPI_COMMODITY_TYPE_BER TAPICommodityType = 'E' // 看跌垂直套利
const TAPI_COMMODITY_TYPE_STD TAPICommodityType = 'D' // 跨式套利
const TAPI_COMMODITY_TYPE_STG TAPICommodityType = 'G' // 宽跨式套利
const TAPI_COMMODITY_TYPE_PRT TAPICommodityType = 'R' // 备兑组合
const TAPI_COMMODITY_TYPE_DIRECTFOREX TAPICommodityType = 'X' // 外汇——直接汇率
const TAPI_COMMODITY_TYPE_INDIRECTFOREX TAPICommodityType = 'I' // 外汇——间接汇率
const TAPI_COMMODITY_TYPE_CROSSFOREX TAPICommodityType = 'C' // 外汇——交叉汇率
const TAPI_COMMODITY_TYPE_INDEX TAPICommodityType = 'Z' // 指数
const TAPI_COMMODITY_TYPE_STOCK TAPICommodityType = 'T' // 股票

// 看涨看跌标示
type TAPICallOrPutFlagType  TAPICHAR 
const TAPI_CALLPUT_FLAG_CALL TAPICallOrPutFlagType = 'C' // 买权
const TAPI_CALLPUT_FLAG_PUT TAPICallOrPutFlagType = 'P' // 卖权
const TAPI_CALLPUT_FLAG_NONE TAPICallOrPutFlagType = 'N' // 无

// 行情价格
type TAPIQPRICE  TAPIREAL64 

// 行情量
type TAPIQVOLUME  TAPIUINT64 

// 变化值
type TAPIQDIFF  TAPIINT64 

