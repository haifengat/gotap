#include "trade.h"
#include <iostream>

using namespace std;
using namespace ITapTrade;
Trade::Trade(void) {
    _OnConnect = NULL;
    _OnRspLogin = NULL;
    _OnRtnContactInfo = NULL;
    _OnRspRequestVertificateCode = NULL;
    _OnExpriationDate = NULL;
    _OnAPIReady = NULL;
    _OnDisconnect = NULL;
    _OnRspChangePassword = NULL;
    _OnRspAuthPassword = NULL;
    _OnRspQryTradingDate = NULL;
    _OnRspSetReservedInfo = NULL;
    _OnRspQryAccount = NULL;
    _OnRspQryFund = NULL;
    _OnRtnFund = NULL;
    _OnRspQryExchange = NULL;
    _OnRspQryCommodity = NULL;
    _OnRspQryContract = NULL;
    _OnRtnContract = NULL;
    _OnRspOrderAction = NULL;
    _OnRtnOrder = NULL;
    _OnRspQryOrder = NULL;
    _OnRspQryOrderProcess = NULL;
    _OnRspQryFill = NULL;
    _OnRtnFill = NULL;
    _OnRspQryPosition = NULL;
    _OnRtnPosition = NULL;
    _OnRspQryPositionSummary = NULL;
    _OnRtnPositionSummary = NULL;
    _OnRtnPositionProfit = NULL;
    _OnRspQryCurrency = NULL;
    _OnRspQryTradeMessage = NULL;
    _OnRtnTradeMessage = NULL;
    _OnRspQryHisOrder = NULL;
    _OnRspQryHisOrderProcess = NULL;
    _OnRspQryHisMatch = NULL;
    _OnRspQryHisPosition = NULL;
    _OnRspQryHisDelivery = NULL;
    _OnRspQryAccountCashAdjust = NULL;
    _OnRspQryBill = NULL;
    _OnRspQryAccountFeeRent = NULL;
    _OnRspQryAccountMarginRent = NULL;
    _OnRspHKMarketOrderInsert = NULL;
    _OnRspHKMarketOrderDelete = NULL;
    _OnHKMarketQuoteNotice = NULL;
    _OnRspOrderLocalRemove = NULL;
    _OnRspOrderLocalInput = NULL;
    _OnRspOrderLocalModify = NULL;
    _OnRspOrderLocalTransfer = NULL;
    _OnRspFillLocalInput = NULL;
    _OnRspFillLocalRemove = NULL;
    _OnRspQrySpotLock = NULL;
    _OnRtnSpotLock = NULL;
    _OnRspSubmitUserLoginInfo = NULL;
    _OnRspSpecialOrderAction = NULL;
    _OnRtnSpecialOrder = NULL;
    _OnRspQrySpecialOrder = NULL;
    _OnRspQryCombinePosition = NULL;
    _OnRtnCombinePosition = NULL;
}

DLL_EXPORT_C_DECL void WINAPI SetOnConnect(Trade* spi, void* func) {
    spi->_OnConnect = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspLogin(Trade* spi, void* func) {
    spi->_OnRspLogin = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnContactInfo(Trade* spi, void* func) {
    spi->_OnRtnContactInfo = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspRequestVertificateCode(Trade* spi, void* func) {
    spi->_OnRspRequestVertificateCode = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnExpriationDate(Trade* spi, void* func) {
    spi->_OnExpriationDate = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnAPIReady(Trade* spi, void* func) {
    spi->_OnAPIReady = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnDisconnect(Trade* spi, void* func) {
    spi->_OnDisconnect = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspChangePassword(Trade* spi, void* func) {
    spi->_OnRspChangePassword = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspAuthPassword(Trade* spi, void* func) {
    spi->_OnRspAuthPassword = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryTradingDate(Trade* spi, void* func) {
    spi->_OnRspQryTradingDate = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspSetReservedInfo(Trade* spi, void* func) {
    spi->_OnRspSetReservedInfo = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryAccount(Trade* spi, void* func) {
    spi->_OnRspQryAccount = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryFund(Trade* spi, void* func) {
    spi->_OnRspQryFund = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnFund(Trade* spi, void* func) {
    spi->_OnRtnFund = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryExchange(Trade* spi, void* func) {
    spi->_OnRspQryExchange = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryCommodity(Trade* spi, void* func) {
    spi->_OnRspQryCommodity = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryContract(Trade* spi, void* func) {
    spi->_OnRspQryContract = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnContract(Trade* spi, void* func) {
    spi->_OnRtnContract = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspOrderAction(Trade* spi, void* func) {
    spi->_OnRspOrderAction = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnOrder(Trade* spi, void* func) {
    spi->_OnRtnOrder = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryOrder(Trade* spi, void* func) {
    spi->_OnRspQryOrder = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryOrderProcess(Trade* spi, void* func) {
    spi->_OnRspQryOrderProcess = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryFill(Trade* spi, void* func) {
    spi->_OnRspQryFill = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnFill(Trade* spi, void* func) {
    spi->_OnRtnFill = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryPosition(Trade* spi, void* func) {
    spi->_OnRspQryPosition = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnPosition(Trade* spi, void* func) {
    spi->_OnRtnPosition = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryPositionSummary(Trade* spi, void* func) {
    spi->_OnRspQryPositionSummary = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnPositionSummary(Trade* spi, void* func) {
    spi->_OnRtnPositionSummary = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnPositionProfit(Trade* spi, void* func) {
    spi->_OnRtnPositionProfit = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryCurrency(Trade* spi, void* func) {
    spi->_OnRspQryCurrency = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryTradeMessage(Trade* spi, void* func) {
    spi->_OnRspQryTradeMessage = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnTradeMessage(Trade* spi, void* func) {
    spi->_OnRtnTradeMessage = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryHisOrder(Trade* spi, void* func) {
    spi->_OnRspQryHisOrder = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryHisOrderProcess(Trade* spi, void* func) {
    spi->_OnRspQryHisOrderProcess = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryHisMatch(Trade* spi, void* func) {
    spi->_OnRspQryHisMatch = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryHisPosition(Trade* spi, void* func) {
    spi->_OnRspQryHisPosition = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryHisDelivery(Trade* spi, void* func) {
    spi->_OnRspQryHisDelivery = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryAccountCashAdjust(Trade* spi, void* func) {
    spi->_OnRspQryAccountCashAdjust = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryBill(Trade* spi, void* func) {
    spi->_OnRspQryBill = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryAccountFeeRent(Trade* spi, void* func) {
    spi->_OnRspQryAccountFeeRent = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryAccountMarginRent(Trade* spi, void* func) {
    spi->_OnRspQryAccountMarginRent = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspHKMarketOrderInsert(Trade* spi, void* func) {
    spi->_OnRspHKMarketOrderInsert = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspHKMarketOrderDelete(Trade* spi, void* func) {
    spi->_OnRspHKMarketOrderDelete = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnHKMarketQuoteNotice(Trade* spi, void* func) {
    spi->_OnHKMarketQuoteNotice = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspOrderLocalRemove(Trade* spi, void* func) {
    spi->_OnRspOrderLocalRemove = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspOrderLocalInput(Trade* spi, void* func) {
    spi->_OnRspOrderLocalInput = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspOrderLocalModify(Trade* spi, void* func) {
    spi->_OnRspOrderLocalModify = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspOrderLocalTransfer(Trade* spi, void* func) {
    spi->_OnRspOrderLocalTransfer = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspFillLocalInput(Trade* spi, void* func) {
    spi->_OnRspFillLocalInput = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspFillLocalRemove(Trade* spi, void* func) {
    spi->_OnRspFillLocalRemove = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQrySpotLock(Trade* spi, void* func) {
    spi->_OnRspQrySpotLock = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnSpotLock(Trade* spi, void* func) {
    spi->_OnRtnSpotLock = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspSubmitUserLoginInfo(Trade* spi, void* func) {
    spi->_OnRspSubmitUserLoginInfo = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspSpecialOrderAction(Trade* spi, void* func) {
    spi->_OnRspSpecialOrderAction = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnSpecialOrder(Trade* spi, void* func) {
    spi->_OnRtnSpecialOrder = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQrySpecialOrder(Trade* spi, void* func) {
    spi->_OnRspQrySpecialOrder = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRspQryCombinePosition(Trade* spi, void* func) {
    spi->_OnRspQryCombinePosition = func;
}

DLL_EXPORT_C_DECL void WINAPI SetOnRtnCombinePosition(Trade* spi, void* func) {
    spi->_OnRtnCombinePosition = func;
}

// 导出的请求函数
/* DLL_EXPORT_C_DECL ITapTrade::ITapTradeAPI *TAP_CDECL CreateITapTradeAPI(const ITapTrade::TapAPIApplicationInfo *appInfo, ITapTrade::TAPIINT32 &iResult);
DLL_EXPORT_C_DECL void TAP_CDECL FreeITapTradeAPI(ITapTrade::ITapTradeAPI *apiObj);
DLL_EXPORT_C_DECL const ITapTrade::TAPICHAR *TAP_CDECL GetITapTradeAPIVersion();
DLL_EXPORT_C_DECL const char  * TAP_CDECL GetITapErrorDescribe(ITapTrade::TAPIINT32 errorCode); */

DLL_EXPORT_C_DECL void* WINAPI CreateITapTradeAPINotify() {
    // 要确保编译通过
    return new Trade();
}
DLL_EXPORT_C_DECL ITapTrade::ITapTradeAPI* WINAPI CreateTradeAPI(const ITapTrade::TapAPIApplicationInfo* appInfo, TAPIINT32* res) {
    TAPIINT32 iResult = 0;
    ITapTrade::ITapTradeAPI* api = CreateITapTradeAPI(appInfo, iResult);
    *res = iResult;
    return api;
}
DLL_EXPORT_C_DECL void WINAPI FreeITapTradeAPI(ITapTrade::ITapTradeAPI* apiObj) {
    return FreeITapTradeAPI(apiObj);
}
DLL_EXPORT_C_DECL void* WINAPI GetAPIVersion() {
    cout << "C++编译版本: " << __cplusplus << endl;
    return (void*)GetITapTradeAPIVersion();
}
DLL_EXPORT_C_DECL void* WINAPI GetErrorDescribe(ITapTrade::TAPIINT32 errorCode) {
    return (void*)GetITapErrorDescribe(errorCode);
}

DLL_EXPORT_C_DECL int WINAPI SetAPINotify(ITapTrade::ITapTradeAPI* api, ITapTradeAPINotify* pSpi) {
    return api->SetAPINotify(pSpi);
}

DLL_EXPORT_C_DECL int WINAPI SetHostAddress(ITapTrade::ITapTradeAPI* api, const TAPICHAR* IP, TAPIUINT16 port) {
    return api->SetHostAddress(IP, port);
}

DLL_EXPORT_C_DECL int WINAPI Login(ITapTrade::ITapTradeAPI* api, const TapAPITradeLoginAuth* loginAuth) {
    return api->Login(loginAuth);
}

DLL_EXPORT_C_DECL int WINAPI RequestVertificateCode(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, TAPISTR_40 ContactInfo) {
    return api->RequestVertificateCode(sessionID, ContactInfo);
}

DLL_EXPORT_C_DECL int WINAPI SetVertificateCode(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, TapAPISecondCertificationReq* req) {
    return api->SetVertificateCode(sessionID, req);
}

DLL_EXPORT_C_DECL int WINAPI Disconnect(ITapTrade::ITapTradeAPI* api) {
    return api->Disconnect();
}

DLL_EXPORT_C_DECL int WINAPI ChangePassword(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIChangePasswordReq* req) {
    return api->ChangePassword(sessionID, req);
}

DLL_EXPORT_C_DECL int WINAPI AuthPassword(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAuthPasswordReq* req) {
    return api->AuthPassword(sessionID, req);
}

DLL_EXPORT_C_DECL int WINAPI HaveCertainRight(ITapTrade::ITapTradeAPI* api, TAPIRightIDType rightID) {
    return api->HaveCertainRight(rightID);
}

DLL_EXPORT_C_DECL int WINAPI QryTradingDate(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID) {
    return api->QryTradingDate(sessionID);
}

DLL_EXPORT_C_DECL int WINAPI SetReservedInfo(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TAPISTR_50 info) {
    return api->SetReservedInfo(sessionID, info);
}

DLL_EXPORT_C_DECL int WINAPI QryAccount(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccQryReq* qryReq) {
    return api->QryAccount(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryFund(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIFundReq* qryReq) {
    return api->QryFund(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryExchange(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID) {
    return api->QryExchange(sessionID);
}

DLL_EXPORT_C_DECL int WINAPI QryCommodity(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID) {
    return api->QryCommodity(sessionID);
}

DLL_EXPORT_C_DECL int WINAPI QryContract(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPICommodity* qryReq) {
    return api->QryContract(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI InsertOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, TAPISTR_50* ClientOrderNo, const TapAPINewOrder* order) {
    return api->InsertOrder(sessionID, ClientOrderNo, order);
}

DLL_EXPORT_C_DECL int WINAPI CancelOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderCancelReq* order) {
    return api->CancelOrder(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI AmendOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAmendOrder* order) {
    return api->AmendOrder(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI ActivateOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderActivateReq* order) {
    return api->ActivateOrder(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI QryOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderQryReq* qryReq) {
    return api->QryOrder(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryOrderProcess(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderProcessQryReq* qryReq) {
    return api->QryOrderProcess(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryFill(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIFillQryReq* qryReq) {
    return api->QryFill(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryPosition(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIPositionQryReq* qryReq) {
    return api->QryPosition(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryPositionSummary(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIPositionQryReq* qryReq) {
    return api->QryPositionSummary(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryCurrency(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID) {
    return api->QryCurrency(sessionID);
}

DLL_EXPORT_C_DECL int WINAPI QryAccountCashAdjust(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountCashAdjustQryReq* qryReq) {
    return api->QryAccountCashAdjust(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryTradeMessage(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPITradeMessageReq* qryReq) {
    return api->QryTradeMessage(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryBill(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIBillQryReq* qryReq) {
    return api->QryBill(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryHisOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIHisOrderQryReq* qryReq) {
    return api->QryHisOrder(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryHisOrderProcess(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIHisOrderProcessQryReq* qryReq) {
    return api->QryHisOrderProcess(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryHisMatch(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIHisMatchQryReq* qryReq) {
    return api->QryHisMatch(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryHisPosition(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIHisPositionQryReq* qryReq) {
    return api->QryHisPosition(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryHisDelivery(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIHisDeliveryQryReq* qryReq) {
    return api->QryHisDelivery(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryAccountFeeRent(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountFeeRentQryReq* qryReq) {
    return api->QryAccountFeeRent(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryAccountMarginRent(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountMarginRentQryReq* qryReq) {
    return api->QryAccountMarginRent(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI InsertHKMarketOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, TAPISTR_50* ClientBuyOrderNo, TAPISTR_50* ClientSellOrderNo, const TapAPIOrderMarketInsertReq* order) {
    return api->InsertHKMarketOrder(sessionID, ClientBuyOrderNo, ClientSellOrderNo, order);
}

DLL_EXPORT_C_DECL int WINAPI CancelHKMarketOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderMarketDeleteReq* order) {
    return api->CancelHKMarketOrder(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI OrderLocalRemove(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderLocalRemoveReq* order) {
    return api->OrderLocalRemove(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI OrderLocalInput(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderLocalInputReq* order) {
    return api->OrderLocalInput(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI OrderLocalModify(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderLocalModifyReq* order) {
    return api->OrderLocalModify(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI OrderLocalTransfer(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIOrderLocalTransferReq* order) {
    return api->OrderLocalTransfer(sessionID, order);
}

DLL_EXPORT_C_DECL int WINAPI FillLocalInput(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIFillLocalInputReq* fill) {
    return api->FillLocalInput(sessionID, fill);
}

DLL_EXPORT_C_DECL int WINAPI FillLocalRemove(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIFillLocalRemoveReq* fill) {
    return api->FillLocalRemove(sessionID, fill);
}

DLL_EXPORT_C_DECL int WINAPI QrySpotLock(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPISpotLockQryReq* qryReq) {
    return api->QrySpotLock(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI SubmitUserLoginInfo(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPISubmitUserLoginInfo* qryReq) {
    return api->SubmitUserLoginInfo(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI InsertSpecialOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, TAPISTR_50* clientorderno, const TapAPISpecialOrderInsertReq* order) {
    return api->InsertSpecialOrder(sessionID, clientorderno, order);
}

DLL_EXPORT_C_DECL int WINAPI QrySpecialOrder(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPISpecialOrderQryReq* qryReq) {
    return api->QrySpecialOrder(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryCombinePosition(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPICombinePositionQryReq* qryReq) {
    return api->QryCombinePosition(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryUserTrustDevice(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIUserTrustDeviceQryReq* qryReq) {
    return api->QryUserTrustDevice(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI AddUserTrustDevice(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIUserTrustDeviceAddReq* qryReq) {
    return api->AddUserTrustDevice(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI DelUserTrustDevice(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIUserTrustDeviceDelReq* qryReq) {
    return api->DelUserTrustDevice(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryIPOInfo(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIIPOInfoQryReq* qryReq) {
    return api->QryIPOInfo(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryIPOStockQty(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAvailableApplyQryReq* qryReq) {
    return api->QryIPOStockQty(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI QryAccountIPO(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountIPOQryReq* qryReq) {
    return api->QryAccountIPO(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI AddAccountIPO(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountIPOAddReq* qryReq) {
    return api->AddAccountIPO(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI CancelAccountIPO(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIAccountIPOCancelReq* qryReq) {
    return api->CancelAccountIPO(sessionID, qryReq);
}

DLL_EXPORT_C_DECL int WINAPI UnFreeze(ITapTrade::ITapTradeAPI* api, const TapAPITradeLoginAuth* loginAuth) {
    return api->UnFreeze(loginAuth);
}

DLL_EXPORT_C_DECL int WINAPI VerificateUnFreezeInfo(ITapTrade::ITapTradeAPI* api, TAPIUINT32* sessionID, const TapAPIVerifyIdentityReq* qryReq) {
    return api->VerificateUnFreezeInfo(sessionID, qryReq);
}
