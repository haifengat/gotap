#pragma once
#ifdef _WINDOWS     // 64位系统没有预定义 WIN32
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C" __declspec(dllexport)
#else
#define DLL_EXPORT_DECL __declspec(dllexport)
#endif
#else
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C"
#else
#define DLL_EXPORT_DECL extern
#endif
#endif

#include "../quote_v9.3.1.4_20190925/TapQuoteAPI.h"

#if defined WIN32 || defined WIN64
#define WIN32_LEAN_AND_MEAN  //  从 Windows 头文件中排除极少使用的信息
#include "stddef.h"
#pragma comment(lib, "../lib/iTapTradeAPI_64.lib")
#ifdef WIN32
#define WINAPI __cdecl
#else
#define WINAPI __stdcall
#endif
#else
#define WINAPI
#endif

class Quote : ITapQuoteAPINotify {
   public:
    Quote(void);

    typedef void(WINAPI* OnRspLoginType)(TAPIINT32 errorCode, const TapAPIQuotLoginRspInfo* info);
    void* _OnRspLogin;
    virtual void OnRspLogin(TAPIINT32 errorCode, const TapAPIQuotLoginRspInfo* info) {
        if (_OnRspLogin)
            ((OnRspLoginType)_OnRspLogin)(errorCode, info);
    }

    typedef void(WINAPI* OnAPIReadyType)();
    void* _OnAPIReady;
    virtual void OnAPIReady() {
        if (_OnAPIReady)
            ((OnAPIReadyType)_OnAPIReady)();
    }

    typedef void(WINAPI* OnDisconnectType)(TAPIINT32 reasonCode);
    void* _OnDisconnect;
    virtual void OnDisconnect(TAPIINT32 reasonCode) {
        if (_OnDisconnect)
            ((OnDisconnectType)_OnDisconnect)(reasonCode);
    }

    typedef void(WINAPI* OnRspQryCommodityType)(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteCommodityInfo* info);
    void* _OnRspQryCommodity;
    virtual void OnRspQryCommodity(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteCommodityInfo* info) {
        if (_OnRspQryCommodity)
            ((OnRspQryCommodityType)_OnRspQryCommodity)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryContractType)(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteContractInfo* info);
    void* _OnRspQryContract;
    virtual void OnRspQryContract(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteContractInfo* info) {
        if (_OnRspQryContract)
            ((OnRspQryContractType)_OnRspQryContract)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspSubscribeQuoteType)(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteWhole* info);
    void* _OnRspSubscribeQuote;
    virtual void OnRspSubscribeQuote(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIQuoteWhole* info) {
        if (_OnRspSubscribeQuote)
            ((OnRspSubscribeQuoteType)_OnRspSubscribeQuote)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspUnSubscribeQuoteType)(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIContract* info);
    void* _OnRspUnSubscribeQuote;
    virtual void OnRspUnSubscribeQuote(TAPIUINT32 sessionID, TAPIINT32 errorCode, TAPIYNFLAG isLast, const TapAPIContract* info) {
        if (_OnRspUnSubscribeQuote)
            ((OnRspUnSubscribeQuoteType)_OnRspUnSubscribeQuote)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnQuoteType)(const TapAPIQuoteWhole* info);
    void* _OnRtnQuote;
    virtual void OnRtnQuote(const TapAPIQuoteWhole* info) {
        if (_OnRtnQuote)
            ((OnRtnQuoteType)_OnRtnQuote)(info);
    }
};
