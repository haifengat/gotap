#pragma once
#ifdef _WINDOWS     // 64位系统没有预定义 WIN32
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C" __declspec(dllexport)
#else
#define DLL_EXPORT_DECL __declspec(dllexport)
#endif
#else
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C"
#else
#define DLL_EXPORT_DECL extern
#endif
#endif

#include "../v9.3.8.7_20230117/iTapTradeAPI.h"

#if defined WIN32 || defined WIN64
#define WIN32_LEAN_AND_MEAN  //  从 Windows 头文件中排除极少使用的信息
#include "stddef.h"
#pragma comment(lib, "../lib/iTapTradeAPI_64.lib")
#ifdef WIN32
#define WINAPI __cdecl
#else
#define WINAPI __stdcall
#endif
#else
#define WINAPI
#endif

#include <string.h>

class Trade : public ITapTrade::ITapTradeAPINotify {
   public:
    Trade(void);

    typedef void(WINAPI* OnConnectType)(const ITapTrade::TAPISTR_40 HostAddress);
    void* _OnConnect;
    virtual void OnConnect(const ITapTrade::TAPISTR_40 HostAddress) {
        if (_OnConnect)
            ((OnConnectType)_OnConnect)(HostAddress);
    }

    typedef void(WINAPI* OnRspLoginType)(ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPITradeLoginRspInfo* loginRspInfo);
    void* _OnRspLogin;
    virtual void OnRspLogin(ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPITradeLoginRspInfo* loginRspInfo) {
        if (_OnRspLogin)
            ((OnRspLoginType)_OnRspLogin)(errorCode, loginRspInfo);
    }

    typedef void(WINAPI* OnRtnContactInfoType)(ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TAPISTR_40 ContactInfo);
    void* _OnRtnContactInfo;
    virtual void OnRtnContactInfo(ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TAPISTR_40 ContactInfo) {
        if (_OnRtnContactInfo)
            ((OnRtnContactInfoType)_OnRtnContactInfo)(errorCode, isLast, ContactInfo);
    }

    typedef void(WINAPI* OnRspRequestVertificateCodeType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIRequestVertificateCodeRsp* rsp);
    void* _OnRspRequestVertificateCode;
    virtual void OnRspRequestVertificateCode(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIRequestVertificateCodeRsp* rsp) {
        if (_OnRspRequestVertificateCode)
            ((OnRspRequestVertificateCodeType)_OnRspRequestVertificateCode)(sessionID, errorCode, rsp);
    }

    typedef void(WINAPI* OnExpriationDateType)(ITapTrade::TAPIDATE date, int days);
    void* _OnExpriationDate;
    virtual void OnExpriationDate(ITapTrade::TAPIDATE date, int days) {
        if (_OnExpriationDate)
            ((OnExpriationDateType)_OnExpriationDate)(date, days);
    }

    typedef void(WINAPI* OnAPIReadyType)(ITapTrade::TAPIINT32 errorCode);
    void* _OnAPIReady;
    virtual void OnAPIReady(ITapTrade::TAPIINT32 errorCode) {
        if (_OnAPIReady)
            ((OnAPIReadyType)_OnAPIReady)(errorCode);
    }

    typedef void(WINAPI* OnDisconnectType)(ITapTrade::TAPIINT32 reasonCode);
    void* _OnDisconnect;
    virtual void OnDisconnect(ITapTrade::TAPIINT32 reasonCode) {
        if (_OnDisconnect)
            ((OnDisconnectType)_OnDisconnect)(reasonCode);
    }

    typedef void(WINAPI* OnRspChangePasswordType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode);
    void* _OnRspChangePassword;
    virtual void OnRspChangePassword(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode) {
        if (_OnRspChangePassword)
            ((OnRspChangePasswordType)_OnRspChangePassword)(sessionID, errorCode);
    }

    typedef void(WINAPI* OnRspAuthPasswordType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode);
    void* _OnRspAuthPassword;
    virtual void OnRspAuthPassword(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode) {
        if (_OnRspAuthPassword)
            ((OnRspAuthPasswordType)_OnRspAuthPassword)(sessionID, errorCode);
    }

    typedef void(WINAPI* OnRspQryTradingDateType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPITradingCalendarQryRsp* info);
    void* _OnRspQryTradingDate;
    virtual void OnRspQryTradingDate(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPITradingCalendarQryRsp* info) {
        if (_OnRspQryTradingDate)
            ((OnRspQryTradingDateType)_OnRspQryTradingDate)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspSetReservedInfoType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TAPISTR_50 info);
    void* _OnRspSetReservedInfo;
    virtual void OnRspSetReservedInfo(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TAPISTR_50 info) {
        if (_OnRspSetReservedInfo)
            ((OnRspSetReservedInfoType)_OnRspSetReservedInfo)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspQryAccountType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIUINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountInfo* info);
    void* _OnRspQryAccount;
    virtual void OnRspQryAccount(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIUINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountInfo* info) {
        if (_OnRspQryAccount)
            ((OnRspQryAccountType)_OnRspQryAccount)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryFundType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIFundData* info);
    void* _OnRspQryFund;
    virtual void OnRspQryFund(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIFundData* info) {
        if (_OnRspQryFund)
            ((OnRspQryFundType)_OnRspQryFund)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnFundType)(const ITapTrade::TapAPIFundData* info);
    void* _OnRtnFund;
    virtual void OnRtnFund(const ITapTrade::TapAPIFundData* info) {
        if (_OnRtnFund)
            ((OnRtnFundType)_OnRtnFund)(info);
    }

    typedef void(WINAPI* OnRspQryExchangeType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIExchangeInfo* info);
    void* _OnRspQryExchange;
    virtual void OnRspQryExchange(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIExchangeInfo* info) {
        if (_OnRspQryExchange)
            ((OnRspQryExchangeType)_OnRspQryExchange)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryCommodityType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPICommodityInfo* info);
    void* _OnRspQryCommodity;
    virtual void OnRspQryCommodity(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPICommodityInfo* info) {
        if (_OnRspQryCommodity)
            ((OnRspQryCommodityType)_OnRspQryCommodity)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryContractType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPITradeContractInfo* info);
    void* _OnRspQryContract;
    virtual void OnRspQryContract(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPITradeContractInfo* info) {
        if (_OnRspQryContract)
            ((OnRspQryContractType)_OnRspQryContract)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnContractType)(const ITapTrade::TapAPITradeContractInfo* info);
    void* _OnRtnContract;
    virtual void OnRtnContract(const ITapTrade::TapAPITradeContractInfo* info) {
        if (_OnRtnContract)
            ((OnRtnContractType)_OnRtnContract)(info);
    }

    typedef void(WINAPI* OnRspOrderActionType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderActionRsp* info);
    void* _OnRspOrderAction;
    virtual void OnRspOrderAction(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderActionRsp* info) {
        if (_OnRspOrderAction)
            ((OnRspOrderActionType)_OnRspOrderAction)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRtnOrderType)(const ITapTrade::TapAPIOrderInfoNotice* info);
    void* _OnRtnOrder;
    virtual void OnRtnOrder(const ITapTrade::TapAPIOrderInfoNotice* info) {
        if (_OnRtnOrder)
            ((OnRtnOrderType)_OnRtnOrder)(info);
    }

    typedef void(WINAPI* OnRspQryOrderType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIOrderInfo* info);
    void* _OnRspQryOrder;
    virtual void OnRspQryOrder(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIOrderInfo* info) {
        if (_OnRspQryOrder)
            ((OnRspQryOrderType)_OnRspQryOrder)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryOrderProcessType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIOrderInfo* info);
    void* _OnRspQryOrderProcess;
    virtual void OnRspQryOrderProcess(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIOrderInfo* info) {
        if (_OnRspQryOrderProcess)
            ((OnRspQryOrderProcessType)_OnRspQryOrderProcess)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryFillType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIFillInfo* info);
    void* _OnRspQryFill;
    virtual void OnRspQryFill(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIFillInfo* info) {
        if (_OnRspQryFill)
            ((OnRspQryFillType)_OnRspQryFill)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnFillType)(const ITapTrade::TapAPIFillInfo* info);
    void* _OnRtnFill;
    virtual void OnRtnFill(const ITapTrade::TapAPIFillInfo* info) {
        if (_OnRtnFill)
            ((OnRtnFillType)_OnRtnFill)(info);
    }

    typedef void(WINAPI* OnRspQryPositionType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIPositionInfo* info);
    void* _OnRspQryPosition;
    virtual void OnRspQryPosition(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIPositionInfo* info) {
        if (_OnRspQryPosition)
            ((OnRspQryPositionType)_OnRspQryPosition)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnPositionType)(const ITapTrade::TapAPIPositionInfo* info);
    void* _OnRtnPosition;
    virtual void OnRtnPosition(const ITapTrade::TapAPIPositionInfo* info) {
        if (_OnRtnPosition)
            ((OnRtnPositionType)_OnRtnPosition)(info);
    }

    typedef void(WINAPI* OnRspQryPositionSummaryType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIPositionSummary* info);
    void* _OnRspQryPositionSummary;
    virtual void OnRspQryPositionSummary(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIPositionSummary* info) {
        if (_OnRspQryPositionSummary)
            ((OnRspQryPositionSummaryType)_OnRspQryPositionSummary)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnPositionSummaryType)(const ITapTrade::TapAPIPositionSummary* info);
    void* _OnRtnPositionSummary;
    virtual void OnRtnPositionSummary(const ITapTrade::TapAPIPositionSummary* info) {
        if (_OnRtnPositionSummary)
            ((OnRtnPositionSummaryType)_OnRtnPositionSummary)(info);
    }

    typedef void(WINAPI* OnRtnPositionProfitType)(const ITapTrade::TapAPIPositionProfitNotice* info);
    void* _OnRtnPositionProfit;
    virtual void OnRtnPositionProfit(const ITapTrade::TapAPIPositionProfitNotice* info) {
        if (_OnRtnPositionProfit)
            ((OnRtnPositionProfitType)_OnRtnPositionProfit)(info);
    }

    typedef void(WINAPI* OnRspQryCurrencyType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPICurrencyInfo* info);
    void* _OnRspQryCurrency;
    virtual void OnRspQryCurrency(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPICurrencyInfo* info) {
        if (_OnRspQryCurrency)
            ((OnRspQryCurrencyType)_OnRspQryCurrency)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryTradeMessageType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPITradeMessage* info);
    void* _OnRspQryTradeMessage;
    virtual void OnRspQryTradeMessage(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPITradeMessage* info) {
        if (_OnRspQryTradeMessage)
            ((OnRspQryTradeMessageType)_OnRspQryTradeMessage)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnTradeMessageType)(const ITapTrade::TapAPITradeMessage* info);
    void* _OnRtnTradeMessage;
    virtual void OnRtnTradeMessage(const ITapTrade::TapAPITradeMessage* info) {
        if (_OnRtnTradeMessage)
            ((OnRtnTradeMessageType)_OnRtnTradeMessage)(info);
    }

    typedef void(WINAPI* OnRspQryHisOrderType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisOrderQryRsp* info);
    void* _OnRspQryHisOrder;
    virtual void OnRspQryHisOrder(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisOrderQryRsp* info) {
        if (_OnRspQryHisOrder)
            ((OnRspQryHisOrderType)_OnRspQryHisOrder)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryHisOrderProcessType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisOrderProcessQryRsp* info);
    void* _OnRspQryHisOrderProcess;
    virtual void OnRspQryHisOrderProcess(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisOrderProcessQryRsp* info) {
        if (_OnRspQryHisOrderProcess)
            ((OnRspQryHisOrderProcessType)_OnRspQryHisOrderProcess)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryHisMatchType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisMatchQryRsp* info);
    void* _OnRspQryHisMatch;
    virtual void OnRspQryHisMatch(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisMatchQryRsp* info) {
        if (_OnRspQryHisMatch)
            ((OnRspQryHisMatchType)_OnRspQryHisMatch)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryHisPositionType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisPositionQryRsp* info);
    void* _OnRspQryHisPosition;
    virtual void OnRspQryHisPosition(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisPositionQryRsp* info) {
        if (_OnRspQryHisPosition)
            ((OnRspQryHisPositionType)_OnRspQryHisPosition)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryHisDeliveryType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisDeliveryQryRsp* info);
    void* _OnRspQryHisDelivery;
    virtual void OnRspQryHisDelivery(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIHisDeliveryQryRsp* info) {
        if (_OnRspQryHisDelivery)
            ((OnRspQryHisDeliveryType)_OnRspQryHisDelivery)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryAccountCashAdjustType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountCashAdjustQryRsp* info);
    void* _OnRspQryAccountCashAdjust;
    virtual void OnRspQryAccountCashAdjust(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountCashAdjustQryRsp* info) {
        if (_OnRspQryAccountCashAdjust)
            ((OnRspQryAccountCashAdjustType)_OnRspQryAccountCashAdjust)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryBillType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIBillQryRsp* info);
    void* _OnRspQryBill;
    virtual void OnRspQryBill(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIBillQryRsp* info) {
        if (_OnRspQryBill)
            ((OnRspQryBillType)_OnRspQryBill)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryAccountFeeRentType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountFeeRentQryRsp* info);
    void* _OnRspQryAccountFeeRent;
    virtual void OnRspQryAccountFeeRent(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountFeeRentQryRsp* info) {
        if (_OnRspQryAccountFeeRent)
            ((OnRspQryAccountFeeRentType)_OnRspQryAccountFeeRent)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryAccountMarginRentType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountMarginRentQryRsp* info);
    void* _OnRspQryAccountMarginRent;
    virtual void OnRspQryAccountMarginRent(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPIAccountMarginRentQryRsp* info) {
        if (_OnRspQryAccountMarginRent)
            ((OnRspQryAccountMarginRentType)_OnRspQryAccountMarginRent)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspHKMarketOrderInsertType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderMarketInsertRsp* info);
    void* _OnRspHKMarketOrderInsert;
    virtual void OnRspHKMarketOrderInsert(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderMarketInsertRsp* info) {
        if (_OnRspHKMarketOrderInsert)
            ((OnRspHKMarketOrderInsertType)_OnRspHKMarketOrderInsert)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspHKMarketOrderDeleteType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderMarketDeleteRsp* info);
    void* _OnRspHKMarketOrderDelete;
    virtual void OnRspHKMarketOrderDelete(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderMarketDeleteRsp* info) {
        if (_OnRspHKMarketOrderDelete)
            ((OnRspHKMarketOrderDeleteType)_OnRspHKMarketOrderDelete)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnHKMarketQuoteNoticeType)(const ITapTrade::TapAPIOrderQuoteMarketNotice* info);
    void* _OnHKMarketQuoteNotice;
    virtual void OnHKMarketQuoteNotice(const ITapTrade::TapAPIOrderQuoteMarketNotice* info) {
        if (_OnHKMarketQuoteNotice)
            ((OnHKMarketQuoteNoticeType)_OnHKMarketQuoteNotice)(info);
    }

    typedef void(WINAPI* OnRspOrderLocalRemoveType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalRemoveRsp* info);
    void* _OnRspOrderLocalRemove;
    virtual void OnRspOrderLocalRemove(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalRemoveRsp* info) {
        if (_OnRspOrderLocalRemove)
            ((OnRspOrderLocalRemoveType)_OnRspOrderLocalRemove)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspOrderLocalInputType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalInputRsp* info);
    void* _OnRspOrderLocalInput;
    virtual void OnRspOrderLocalInput(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalInputRsp* info) {
        if (_OnRspOrderLocalInput)
            ((OnRspOrderLocalInputType)_OnRspOrderLocalInput)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspOrderLocalModifyType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalModifyRsp* info);
    void* _OnRspOrderLocalModify;
    virtual void OnRspOrderLocalModify(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalModifyRsp* info) {
        if (_OnRspOrderLocalModify)
            ((OnRspOrderLocalModifyType)_OnRspOrderLocalModify)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspOrderLocalTransferType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalTransferRsp* info);
    void* _OnRspOrderLocalTransfer;
    virtual void OnRspOrderLocalTransfer(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIOrderLocalTransferRsp* info) {
        if (_OnRspOrderLocalTransfer)
            ((OnRspOrderLocalTransferType)_OnRspOrderLocalTransfer)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspFillLocalInputType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIFillLocalInputRsp* info);
    void* _OnRspFillLocalInput;
    virtual void OnRspFillLocalInput(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIFillLocalInputRsp* info) {
        if (_OnRspFillLocalInput)
            ((OnRspFillLocalInputType)_OnRspFillLocalInput)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspFillLocalRemoveType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIFillLocalRemoveRsp* info);
    void* _OnRspFillLocalRemove;
    virtual void OnRspFillLocalRemove(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPIFillLocalRemoveRsp* info) {
        if (_OnRspFillLocalRemove)
            ((OnRspFillLocalRemoveType)_OnRspFillLocalRemove)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRspQrySpotLockType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPISpotLockDataRsp* info);
    void* _OnRspQrySpotLock;
    virtual void OnRspQrySpotLock(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPISpotLockDataRsp* info) {
        if (_OnRspQrySpotLock)
            ((OnRspQrySpotLockType)_OnRspQrySpotLock)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnSpotLockType)(const ITapTrade::TapAPISpotLockDataRsp* info);
    void* _OnRtnSpotLock;
    virtual void OnRtnSpotLock(const ITapTrade::TapAPISpotLockDataRsp* info) {
        if (_OnRtnSpotLock)
            ((OnRtnSpotLockType)_OnRtnSpotLock)(info);
    }

    typedef void(WINAPI* OnRspSubmitUserLoginInfoType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast);
    void* _OnRspSubmitUserLoginInfo;
    virtual void OnRspSubmitUserLoginInfo(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast) {
        if (_OnRspSubmitUserLoginInfo)
            ((OnRspSubmitUserLoginInfoType)_OnRspSubmitUserLoginInfo)(sessionID, errorCode, isLast);
    }

    typedef void(WINAPI* OnRspSpecialOrderActionType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPISpecialOrderInfo* info);
    void* _OnRspSpecialOrderAction;
    virtual void OnRspSpecialOrderAction(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, const ITapTrade::TapAPISpecialOrderInfo* info) {
        if (_OnRspSpecialOrderAction)
            ((OnRspSpecialOrderActionType)_OnRspSpecialOrderAction)(sessionID, errorCode, info);
    }

    typedef void(WINAPI* OnRtnSpecialOrderType)(const ITapTrade::TapAPISpecialOrderInfo* info);
    void* _OnRtnSpecialOrder;
    virtual void OnRtnSpecialOrder(const ITapTrade::TapAPISpecialOrderInfo* info) {
        if (_OnRtnSpecialOrder)
            ((OnRtnSpecialOrderType)_OnRtnSpecialOrder)(info);
    }

    typedef void(WINAPI* OnRspQrySpecialOrderType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPISpecialOrderInfo* info);
    void* _OnRspQrySpecialOrder;
    virtual void OnRspQrySpecialOrder(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, const ITapTrade::TapAPISpecialOrderInfo* info) {
        if (_OnRspQrySpecialOrder)
            ((OnRspQrySpecialOrderType)_OnRspQrySpecialOrder)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRspQryCombinePositionType)(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, ITapTrade::TapAPICombinePositionInfo* info);
    void* _OnRspQryCombinePosition;
    virtual void OnRspQryCombinePosition(ITapTrade::TAPIUINT32 sessionID, ITapTrade::TAPIINT32 errorCode, ITapTrade::TAPIYNFLAG isLast, ITapTrade::TapAPICombinePositionInfo* info) {
        if (_OnRspQryCombinePosition)
            ((OnRspQryCombinePositionType)_OnRspQryCombinePosition)(sessionID, errorCode, isLast, info);
    }

    typedef void(WINAPI* OnRtnCombinePositionType)(const ITapTrade::TapAPICombinePositionInfo* info);
    void* _OnRtnCombinePosition;
    virtual void OnRtnCombinePosition(const ITapTrade::TapAPICombinePositionInfo* info) {
        if (_OnRtnCombinePosition)
            ((OnRtnCombinePositionType)_OnRtnCombinePosition)(info);
    }
};
