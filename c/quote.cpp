#include "quote.h"
#include <iostream>

using namespace std;

Quote::Quote(void)
{	
	_OnRspLogin = NULL;
	_OnAPIReady = NULL;
	_OnDisconnect = NULL;
	_OnRspQryCommodity = NULL;
	_OnRspQryContract = NULL;
	_OnRspSubscribeQuote = NULL;
	_OnRspUnSubscribeQuote = NULL;
	_OnRtnQuote = NULL;
	
}


DLL_EXPORT_C_DECL void WINAPI qSetOnRspLogin(Quote* spi, void* func){spi->_OnRspLogin = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnAPIReady(Quote* spi, void* func){spi->_OnAPIReady = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnDisconnect(Quote* spi, void* func){spi->_OnDisconnect = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnRspQryCommodity(Quote* spi, void* func){spi->_OnRspQryCommodity = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnRspQryContract(Quote* spi, void* func){spi->_OnRspQryContract = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnRspSubscribeQuote(Quote* spi, void* func){spi->_OnRspSubscribeQuote = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnRspUnSubscribeQuote(Quote* spi, void* func){spi->_OnRspUnSubscribeQuote = func;}

DLL_EXPORT_C_DECL void WINAPI qSetOnRtnQuote(Quote* spi, void* func){spi->_OnRtnQuote = func;}


DLL_EXPORT_C_DECL void* WINAPI CreateQuoteAPINotify(){
    // 要确保编译通过
	return new Quote();
}
DLL_EXPORT_C_DECL ITapQuoteAPI* WINAPI CreateQuoteAPI(const TapAPIApplicationInfo *appInfo){
    TAPIINT32 iResult = 0;
    return CreateTapQuoteAPI(appInfo, iResult);
}
DLL_EXPORT_C_DECL void WINAPI FreeQuoteAPI(ITapQuoteAPI *apiObj){
    return FreeTapQuoteAPI(apiObj);
}
DLL_EXPORT_C_DECL void * WINAPI GetQuoteAPIVersion(){
	cout << "C++编译版本: " << __cplusplus << endl;
    return (void *)GetTapQuoteAPIVersion();
}
DLL_EXPORT_C_DECL int WINAPI SetQuoteAPIDataPath(const TAPICHAR *path){
    return SetTapQuoteAPIDataPath(path);
}
DLL_EXPORT_C_DECL int WINAPI SetQuoteAPILogLevel(TAPILOGLEVEL level){
    return SetTapQuoteAPILogLevel(level);
}


DLL_EXPORT_C_DECL int WINAPI qSetAPINotify(ITapQuoteAPI *api, ITapQuoteAPINotify *apiNotify){return api->SetAPINotify(apiNotify);}

DLL_EXPORT_C_DECL int WINAPI qSetHostAddress(ITapQuoteAPI *api, const TAPICHAR *IP, TAPIUINT16 port){return api->SetHostAddress(IP, port);}

DLL_EXPORT_C_DECL int WINAPI qLogin(ITapQuoteAPI *api, const TapAPIQuoteLoginAuth *loginAuth){return api->Login(loginAuth);}

DLL_EXPORT_C_DECL int WINAPI qDisconnect(ITapQuoteAPI *api){return api->Disconnect();}

DLL_EXPORT_C_DECL int WINAPI qQryCommodity(ITapQuoteAPI *api, TAPIUINT32 *sessionID){return api->QryCommodity(sessionID);}

DLL_EXPORT_C_DECL int WINAPI qQryContract(ITapQuoteAPI *api, TAPIUINT32 *sessionID, const TapAPICommodity *qryReq){return api->QryContract(sessionID, qryReq);}

DLL_EXPORT_C_DECL int WINAPI qSubscribeQuote(ITapQuoteAPI *api, TAPIUINT32 *sessionID, const TapAPIContract *contract){return api->SubscribeQuote(sessionID, contract);}

DLL_EXPORT_C_DECL int WINAPI qUnSubscribeQuote(ITapQuoteAPI *api, TAPIUINT32 *sessionID, const TapAPIContract *contract){return api->UnSubscribeQuote(sessionID, contract);}

