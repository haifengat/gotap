module gitee.com/haifengat/gotap_dipper

go 1.21

require (
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/text v0.13.0
)

require golang.org/x/sys v0.5.0 // indirect
