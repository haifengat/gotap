package quote

/*
#cgo CPPFLAGS: -fPIC -I./
#cgo linux LDFLAGS: -fPIC -L${SRCDIR} -Wl,-rpath ${SRCDIR} -ltapquote -lstdc++

#include "struct_cgo.h"

void* CreateQuoteAPINotify();
void* CreateQuoteAPI(struct TapAPIApplicationInfo *appInfo);
void FreeQuoteAPI(void* api);
void* GetQuoteAPIVersion();
int SetQuoteAPIDataPath(char* path);
int SetQuoteAPILogLevel(char level);

// ********************** 调用函数 ******************
[[ range .Fn ]]int q[[ .FuncName ]](void* api[[ range .Params ]],[[ CHeaderBaseType .Type .Name]] [[ .Name ]][[end]]);
[[ end ]]


// ********************** 响应函数 ******************
[[ range .On ]]void qSet[[ .FuncName ]](void*, void*);
[[ end ]]

// Onxxx 为go 的 export所用
[[ range .On ]]void [[ .FuncName ]]([[ range $i,$v := .Params ]][[ if gt $i 0 ]], [[ end ]] [[ CHeaderBaseType .Type .Name ]] [[ .Name ]][[end]]);
[[ end ]]

#include <stdlib.h>
#include <stdint.h>
*/
import "C"

import (
	"fmt"
	"unsafe"
)

var q *Quote
func NewQuote()*Quote{
	if q==nil {
		q = &Quote{}
	}
	return q
}

// Quote 行情接口
type Quote struct {
	api, spi unsafe.Pointer
	// 定义响应函数变量
	[[ range .On ]][[ .FuncName ]] func([[ range $i,$v := .Params ]][[ if gt $i 0 ]], [[ end ]] [[ DefineVar .Type .Name ]][[end]])
	[[ end ]]
}

func (q *Quote) CreateTapQuoteAPINotify() {
	q.spi = C.CreateQuoteAPINotify()
}
func (q *Quote) CreateTapQuoteAPI(appInfo *TapAPIApplicationInfo) {
	q.api = C.CreateQuoteAPI((*C.struct_TapAPIApplicationInfo)(unsafe.Pointer(appInfo)))
}
func (q *Quote)FreeTapQuoteAPI(api unsafe.Pointer)  {
	C.FreeQuoteAPI(api)
}
func (q *Quote)GetTapQuoteAPIVersion() string {
	return C.GoString((*C.char)(C.GetQuoteAPIVersion()))
}
func (q *Quote) SetTapQuoteAPIDataPath(path string) C.int {
	return C.SetQuoteAPIDataPath(C.CString(path))
}
func (q *Quote) SetTapQuoteAPILogLevel(level byte) C.int {
	return C.SetQuoteAPILogLevel(C.char(level))
}

// 替代 SetAPINotify
func (q *Quote) SetSpi() { C.qSetAPINotify(q.api, q.spi) }

// ********************** 调用函数 ******************
[[ range .Fn ]]
func (q *Quote) [[ .FuncName ]]([[ range $i,$v := .Params ]][[ if gt $i 0 ]], [[ end ]][[ C2Go .Type .Name]][[ end ]]) C.int {return C.q[[ .FuncName ]](q.api[[ range .Params ]], [[ Go2C .Type .Name ]][[ end ]])}
[[ end ]]

// ********************** 响应函数 ******************
func (q *Quote)RegCallBack(){
	[[ range .On ]]C.qSet[[ .FuncName ]](q.spi, C.[[ .FuncName ]])
	[[ end ]]
}

[[ range .On ]]
//export [[ .FuncName ]]
func [[ .FuncName ]]([[ range $i,$v := .Params ]][[ if gt $i 0 ]], [[ end ]] [[ CCB .Type .Name ]][[end]]){
	if q.[[ .FuncName ]] != nil {
		q.[[ .FuncName ]]([[ range $i,$v := .Params ]][[ if gt $i 0 ]], [[ end ]] [[ cgo .Type .Name ]][[end]])
	} else {
		fmt.Println("[[ .FuncName ]]")
	}
}
[[ end ]]