#include "trade.h"
#include <iostream>

using namespace std;
using namespace ITapTrade;
Trade::Trade(void)
{	
	[[ range .On ]]_[[ .FuncName ]] = NULL;
	[[ end ]]
}

[[ range .On ]]
DLL_EXPORT_C_DECL void WINAPI Set[[ .FuncName ]](Trade* spi, void* func){spi->_[[ .FuncName ]] = func;}
[[ end ]]

// 导出的请求函数
/* DLL_EXPORT_C_DECL ITapTrade::ITapTradeAPI *TAP_CDECL CreateITapTradeAPI(const ITapTrade::TapAPIApplicationInfo *appInfo, ITapTrade::TAPIINT32 &iResult);
DLL_EXPORT_C_DECL void TAP_CDECL FreeITapTradeAPI(ITapTrade::ITapTradeAPI *apiObj);
DLL_EXPORT_C_DECL const ITapTrade::TAPICHAR *TAP_CDECL GetITapTradeAPIVersion();
DLL_EXPORT_C_DECL const char  * TAP_CDECL GetITapErrorDescribe(ITapTrade::TAPIINT32 errorCode); */

DLL_EXPORT_C_DECL void* WINAPI CreateITapTradeAPINotify(){
    // 要确保编译通过
	return new Trade();
}
DLL_EXPORT_C_DECL ITapTrade::ITapTradeAPI* WINAPI CreateTradeAPI(const ITapTrade::TapAPIApplicationInfo *appInfo){
    TAPIINT32 iResult = 0;
    return CreateITapTradeAPI(appInfo, iResult);
}
DLL_EXPORT_C_DECL void WINAPI FreeITapTradeAPI(ITapTrade::ITapTradeAPI *apiObj){
    return FreeITapTradeAPI(apiObj);
}
DLL_EXPORT_C_DECL void * WINAPI GetAPIVersion(){
	cout << "C++编译版本: " << __cplusplus << endl;
    return (void *)GetITapTradeAPIVersion();
}
DLL_EXPORT_C_DECL void * WINAPI GetErrorDescribe(ITapTrade::TAPIINT32 errorCode){
    return (void *)GetITapErrorDescribe(errorCode);
}

[[ range .Fn ]]
DLL_EXPORT_C_DECL int WINAPI [[ .FuncName ]](ITapTrade::ITapTradeAPI *api[[ range .Params ]], [[.Type]] [[.Name]][[end]]){return api->[[.FuncName]]([[ range $i, $v := .Params ]][[if gt $i 0]], [[end]][[ .Name|trim ]][[end]]);}
[[ end ]]
