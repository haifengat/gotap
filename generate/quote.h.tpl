#pragma once
#ifdef _WINDOWS  //64位系统没有预定义 WIN32
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C" __declspec(dllexport)
#else
#define DLL_EXPORT_DECL __declspec(dllexport)
#endif
#else
#ifdef __cplusplus  // 有定义表示是 c++ 代码, 此值为 ld 类型, 内容为c+标准版本号
#define DLL_EXPORT_C_DECL extern "C"
#else
#define DLL_EXPORT_DECL extern
#endif
#endif

#include "../v9.3.7.5_20220228/TapQuoteAPI.h"

#if defined WIN32 || defined WIN64
#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
#include "stddef.h"
#pragma comment(lib, "../v9.3.7.5_20220228/iTapTradeAPI_64.lib")
#ifdef WIN32
#define WINAPI      __cdecl
#else
#define WINAPI      __stdcall
#endif
#else
#define WINAPI
#endif

class Quote: ITapQuoteAPINotify
{
public:
    Quote(void);
    [[ range .On ]]
	typedef void (WINAPI *[[ .FuncName ]]Type)([[ range $n, $var := .Params ]][[if gt $n 0]], [[end]][[ .Type ]] [[ .Name ]][[ end ]]);
	void *_[[ .FuncName ]];
	virtual void [[ .FuncName ]]([[ range $n, $var := .Params ]][[if gt $n 0]], [[end]][[ .Type ]] [[ .Name ]][[ end ]]){if (_[[ .FuncName ]]) (([[ .FuncName ]]Type)_[[ .FuncName ]])([[ range $n, $var := .Params ]][[if gt $n 0]], [[end]] [[ .Name|trim ]][[ end ]]);}
    [[ end ]]
};
