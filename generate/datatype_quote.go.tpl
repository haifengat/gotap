package quote

// uint 64
type TAPIUINT64  uint64 

[[ range .]]// [[ .Comment ]]
type [[ .TypeName ]] [[ if eq .TypeName "TAPICHAR" ]] byte [[ else ]] [[ .CType|typeTrans ]] [[ end ]]
[[ range .Const ]]const [[ .Name ]] [[ .TypeName ]] = [[ if eq (len .Value) 1 ]]'[[ .Value ]]'[[ else ]]"[[ .Value ]]"[[ end ]] // [[ .Comment ]]
[[ end ]]
[[ end ]]