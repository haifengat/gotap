package generate

import "testing"

func TestGenQuote(t *testing.T) {
	genDataTypeGoQuote()
	genStructGoQuote()
	genStructCgoQuote()
	genQuote()
}

func TestGenTrade(t *testing.T) {
	// genDataTypeGoTrade()
	genStructGoTrade()
	// genStructCgoTrade()
	// genTrade()
}
