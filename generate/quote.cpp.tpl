#include "quote.h"
#include <iostream>

using namespace std;

Quote::Quote(void)
{	
	[[ range .On ]]_[[ .FuncName ]] = NULL;
	[[ end ]]
}

[[ range .On ]]
DLL_EXPORT_C_DECL void WINAPI qSet[[ .FuncName ]](Quote* spi, void* func){spi->_[[ .FuncName ]] = func;}
[[ end ]]

DLL_EXPORT_C_DECL void* WINAPI CreateQuoteAPINotify(){
    // 要确保编译通过
	return new Quote();
}
DLL_EXPORT_C_DECL ITapQuoteAPI* WINAPI CreateQuoteAPI(const TapAPIApplicationInfo *appInfo){
    TAPIINT32 iResult = 0;
    return CreateTapQuoteAPI(appInfo, iResult);
}
DLL_EXPORT_C_DECL void WINAPI FreeQuoteAPI(ITapQuoteAPI *apiObj){
    return FreeTapQuoteAPI(apiObj);
}
DLL_EXPORT_C_DECL void * WINAPI GetQuoteAPIVersion(){
	cout << "C++编译版本: " << __cplusplus << endl;
    return (void *)GetTapQuoteAPIVersion();
}
DLL_EXPORT_C_DECL int WINAPI SetQuoteAPIDataPath(const TAPICHAR *path){
    return SetTapQuoteAPIDataPath(path);
}
DLL_EXPORT_C_DECL int WINAPI SetQuoteAPILogLevel(TAPILOGLEVEL level){
    return SetTapQuoteAPILogLevel(level);
}

[[ range .Fn ]]
DLL_EXPORT_C_DECL int WINAPI q[[ .FuncName ]](ITapQuoteAPI *api[[ range .Params ]], [[.Type]] [[.Name]][[end]]){return api->[[.FuncName]]([[ range $i, $v := .Params ]][[if gt $i 0]], [[end]][[ .Name|trim ]][[end]]);}
[[ end ]]
