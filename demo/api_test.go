package demo

import (
	"fmt"
	"os"
	"testing"

	"gitee.com/haifengat/gotap_dipper/quote"
	"gitee.com/haifengat/gotap_dipper/trade"
)

func init() {
	os.Chdir("../")
}

func TestQuote(t *testing.T) {
	q := quote.NewQuote()
	fmt.Println("接口版本: ", q.GetTapQuoteAPIVersion())
	qApp := quote.TapAPIApplicationInfo{}
	copy(qApp.AuthCode[:], "B112F916FE7D27BCE7B97EB620206457946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC5211AF9FEE541DDE9D6F622F72E25D5DEF7F47AA93A738EF5A51B81D8526AB6A9D19E65B41F59D6A946CED32E26C1EACCAF8D4C61E28E2B1ABD9B8F170E14F8847D3EA0BF4E191F5DCB1B791E63DC196D1576DEAF5EC563CA3E560313C0C3411B45076795F550EB050A62C4F74D5892D2D14892E812723FAC858DEBD8D4AF9410729FB849D5D8D6EA48A1B8DC67E037381A279CE9426070929D5DA085659772E24A6F5EA52CF92A4D403F9E46083F27B19A88AD99812DADA44100324759F9FD1964EBD4F2F0FB50B51CD31C0B02BB437")
	q.CreateTapQuoteAPI(&qApp)
	fmt.Println("行情 api created")

	q.CreateTapQuoteAPINotify()
	fmt.Println("行情 spi created")

	q.RegCallBack()
	q.SetSpi()

	q.OnRspLogin = func(errorCode quote.TAPIINT32, info *quote.TapAPIQuotLoginRspInfo) {
		fmt.Println("登录: ", errorCode)
		if errorCode == 0 {
			fmt.Printf("%+v\n", info)
		}
	}
	q.OnAPIReady = func() {
		fmt.Println("api ready")
		s := quote.TAPIUINT32(1)
		q.QryCommodity(&s)
	}
	q.OnRspQryCommodity = func(sessionID quote.TAPIUINT32, errorCode quote.TAPIINT32, isLast quote.TAPIYNFLAG, info *quote.TapAPIQuoteCommodityInfo) {
		if errorCode != 0 {
			fmt.Println(errorCode)
		} else {
			fmt.Printf("No:%s, exc:%s, type: %c\n", string(info.Commodity.CommodityNo[:]), string(info.Commodity.ExchangeNo[:]), info.Commodity.CommodityType)
			if isLast == quote.APIYNFLAG_YES {
				comm := quote.TapAPICommodity{}
				copy(comm.ExchangeNo[:], "COMEX")
				copy(comm.CommodityNo[:], "GC")
				comm.CommodityType = quote.TAPI_COMMODITY_TYPE_FUTURES
				s := quote.TAPIUINT32(2)
				q.QryContract(&s, &comm)
			}
		}
	}
	q.OnRspQryContract = func(sessionID quote.TAPIUINT32, errorCode quote.TAPIINT32, isLast quote.TAPIYNFLAG, info *quote.TapAPIQuoteContractInfo) {
		if errorCode != 0 {
			fmt.Println("查合约错误: ", errorCode)
		} else if info != nil {
			fmt.Println(string(info.Contract.ContractNo1[:]))
			if isLast == quote.APIYNFLAG_YES {
				comm := quote.TapAPICommodity{}
				copy(comm.ExchangeNo[:], "CFFEX")
				copy(comm.CommodityNo[:], "IF")
				comm.CommodityType = quote.TAPI_COMMODITY_TYPE_FUTURES

				c := quote.TapAPIContract{
					CallOrPutFlag1: quote.TAPI_CALLPUT_FLAG_NONE,
					CallOrPutFlag2: quote.TAPI_CALLPUT_FLAG_NONE,
				}
				c.Commodity = comm
				copy(c.ContractNo1[:], "2312")

				s := quote.TAPIUINT32(3)
				res := q.SubscribeQuote(&s, &c)
				fmt.Println("订阅行情: ", res)
			}
		}
	}
	q.OnRspSubscribeQuote = func(sessionID quote.TAPIUINT32, errorCode quote.TAPIINT32, isLast quote.TAPIYNFLAG, info *quote.TapAPIQuoteWhole) {
		if errorCode != 0 {
			fmt.Println("订阅行情响应: ", errorCode)
		} else {
			// 1,集合竞价;2,集合竞价撮合;3,连续交易;4,交易暂停;5,闭市
			fmt.Printf("交易状态: %+v\n", *info)
		}
	}
	q.OnRtnQuote = func(info *quote.TapAPIQuoteWhole) {
		fmt.Printf("%+v\n", *info)
	}
	q.OnDisconnect = func(reasonCode quote.TAPIINT32) { fmt.Println("行情断开: ", reasonCode) }

	login := quote.TapAPIQuoteLoginAuth{
		ISModifyPassword: quote.APIYNFLAG_NO,
		ISDDA:            quote.APIYNFLAG_NO,
	}

	// r := q.SetHostAddress("61.163.243.173", quote.TAPIUINT16(7171)) // 北斗星-行情
	// copy(login.UserNo[:], "ES")

	// r := q.SetHostAddress("123.161.206.213", quote.TAPIUINT16(8383)) // 北斗星-交易
	// copy(login.UserNo[:], "Q576288330")

	r := q.SetHostAddress("106.37.101.162", quote.TAPIUINT16(16868)) // 启明星
	copy(login.UserNo[:], "0020000879")

	// r := q.SetHostAddress("118.25.179.96", quote.TAPIUINT16(55012)) // (北)极星国际

	fmt.Println("配置前置: ", r)

	copy(login.Password[:], "123456")
	r = q.Login(&login)
	fmt.Println("登录: ", r)

	select {}
}

func TestTradeCTP(tst *testing.T) {
	t, err := trade.NewTradeExt("simnow_client_test", "67EA896065459BECDFDB924B29CB7DF1946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC5211AF9FEE541DDE41BCBAB68D525B0D111A0884D847D57163FF7F329FA574E7946CED32E26C1EAC946CED32E26C1EAC733827B0CE853869ABD9B8F170E14F8847D3EA0BF4E191F5D97B3DFE4CCB1F01842DD2B3EA2F4B20CAD19B8347719B7E20EA1FA7A3D1BFEFF22290F4B5C43E6C520ED5A40EC1D50ACDF342F46A92CCF87AEE6D73542C42EC17818349C7DEDAB0E4DB16977714F873D505029E27B3D57EB92D5BEDA0A710197EB67F94BB1892B30F58A3F211D9C3B3839BE2D73FD08DD776B9188654853DDA57675EBB7D6FBBFC")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	// 180.168.146.187:10130
	err = t.ReqConnect("180.168.146.187", 10130)
	if err != nil {
		fmt.Println("connect:", err)
	}

	err = t.ReqLogin("008107", "1")
	if err != nil {
		fmt.Println(err)
		os.Exit(-2)
	}
}

func TestTrade(tst *testing.T) {
	accountNo := "Q24918700"
	// commodityType := trade.TAPI_COMMODITY_TYPE_FUTURES
	// exchangeID := "CBOT"
	// commodityNo := "C"
	// contractNo := "2312"
	// price := float64(484.0)

	// exchangeID := "BMD"
	// commodityNo := "CPO"
	// contractNo := "2312"
	// price := float64(3695)

	t, err := trade.NewTradeExt("", "67EA896065459BECDFDB924B29CB7DF1946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC946CED32E26C1EAC5211AF9FEE541DDE41BCBAB68D525B0D111A0884D847D57163FF7F329FA574E7946CED32E26C1EAC946CED32E26C1EAC733827B0CE853869ABD9B8F170E14F8847D3EA0BF4E191F5D97B3DFE4CCB1F01842DD2B3EA2F4B20CAD19B8347719B7E20EA1FA7A3D1BFEFF22290F4B5C43E6C520ED5A40EC1D50ACDF342F46A92CCF87AEE6D73542C42EC17818349C7DEDAB0E4DB16977714F873D505029E27B3D57EB92D5BEDA0A710197EB67F94BB1892B30F58A3F211D9C3B3839BE2D73FD08DD776B9188654853DDA57675EBB7D6FBBFC")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	t.OnTrade = func(info *trade.TapAPIFillInfo) {
		fmt.Printf("OnTrade: %+v\n", *info)
	}
	t.OnPosition = func(info *trade.TapAPIPositionSummary) {
		fmt.Printf("OnPosition: %+v\n", *info)
	}

	err = t.ReqConnect("123.161.206.213", 8383)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	err = t.ReqLogin(accountNo, "654321")
	if err != nil {
		fmt.Println(err)
		os.Exit(-2)
	}
	// defer t.Disconnect()
	defer t.ReqLogout()

	for _, v := range t.AccountID {
		fmt.Println("帐号: ", v)
	}
	fmt.Println("查持仓 ...")
	ps := t.ReqQryPosition(t.AccountID[0])
	for _, p := range ps {
		if p.MatchSide == trade.TAPI_SIDE_BUY {
			fmt.Println(p.Key(), ":", p.PositionQty.Uint32())
		} else {
			fmt.Println(p.Key(), ": -", p.PositionQty.Uint32())
		}
	}
	fmt.Println("持仓完成")

	// for _, v := range t.Instrument {
	// 	if v.CommodityNo.String() == commodityNo && v.ContractNo1.String() == contractNo {
	// 		session, id, err := t.ReqOrderInsert(exchangeID, commodityNo, contractNo, orderType, price, qty, side, commodityType)
	// 		if err != nil {
	// 			fmt.Println(err)
	// 			os.Exit(-3)
	// 		}
	// 		fmt.Println("委托: ", session, id)
	// 		break
	// 	}
	// }

	// select {}
}
